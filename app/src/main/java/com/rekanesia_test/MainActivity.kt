package com.rekanesia_test

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import androidx.core.text.color
import androidx.core.widget.doOnTextChanged
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.rekanesia.chip.ChipOption
import com.rekanesia_test.databinding.ActivityMainBinding
import com.rekanesia_test.example.showcase.ListShowcaseActivity
import com.rekanesia.options.Opt

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var isPrimary:Boolean = true
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        title = ""

        binding.btnShowcase.setOnClickListener {
            startActivity(Intent(this, ListShowcaseActivity::class.java))
        }


        Dexter.withContext(this)
            .withPermissions(
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener{
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {

                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }
            })
            .onSameThread()
            .check()

        binding.tfLeft.text = (10000000).toString()

        val chips = mutableListOf<ChipOption>()
        for (i in 1.. 10) {
            chips.add(
                ChipOption(
                    i,
                    "Chip $i",
                    false,
                    childChips = arrayListOf(
                        ChipOption(
                            i,
                            "Chip $i",
                            false
                        )
                    )
                )
            )
        }
        binding.chipGroup.addAllChip(chips)

        binding.chipGroup.setOnChipSelectedListener {
            Toast.makeText(this, it?.name, Toast.LENGTH_SHORT).show()
        }

        binding.chipGroupStatic.setOnMultipleChipsSelectedListener {
            Toast.makeText(this, it?.toString(), Toast.LENGTH_SHORT).show()
        }

        binding.chipGroupStatic.setOnChipSelectedIndexed { index, chipOption ->
            Log.d("ChipGroup", "Index: $index \n$chipOption")
        }

        val options = mutableListOf<Opt>()
        for (i in 1 until 10) {
            options.add(
                Opt(
                    id = i,
                    label = "Option $i",
                    selected = false,
                    requiredTextField = true,
                    prefix = if (i % 2 == 0) "Rp " else "",
                    suffix = if (i % 2 != 0) " Kg" else ""
                )
            )
        }
        binding.options.replaceItems(options)

        binding.options.onSelectedTextFieldChange {
            Log.d("Option", it)
        }

        binding.options.isEnabledClick = false

        binding.options.onOptionSelected { option ->

        }

        binding.bannerWithLoading.showProgressBar = true

        binding.tfInput.input.doOnTextChanged { text, start, before, count ->
            binding.tfInput.isError = text.toString().length < 10
        }

        binding.apply {
            tfRight.input.doOnTextChanged { text, _, _, _ ->
                tfRight.isError = text.toString().length < 10
            }

            tfLeft.input.doOnTextChanged { text, start, before, count ->
                tfLeft.isError = text.toString().length < 10
            }

            textArea.input.doOnTextChanged { text, _, _, _ ->
                textArea.isError = text.toString().length < 10
            }

            choose.setOnClickListener {
                Log.d("TESTT","ada")

            }

            rekanToolbar.setOnIconEndClickListener {
                Toast.makeText(this@MainActivity, "Clicked", Toast.LENGTH_SHORT).show()
                binding.options.isEnabledClick = true
            }

            searchBox.querySearch = "123123"
            searchBox.setOnIconEndClickListener {
                searchBox.setVisibilityIconEnd(false)
                Toast.makeText(this@MainActivity, "Clicked", Toast.LENGTH_SHORT).show()
            }

            tfCurrencyInput.addTextChangedListener { s->
                Log.d("TESTT","$s")
            }

            tfCurrencyInput.text = "100000"


            npTest.setListener{
                Log.d("TESTT","0")
            }

            npTest.setCount(100)

            tfPasswordText.setOnDrawableClickListener{
                Log.d("TESTT","sdsdsdsdsds")
            }

            tfDisable.stateEnable = false
            choose.setEnable = true

            tileKomoditas.setValueFontWeight(com.rekanesia.FontWeightDataTile.SemiBold)
            tileKomoditas.setLabelFontWeight(com.rekanesia.FontWeightDataTile.Bold)

            tileKomoditas.setOnClickListener {
                Toast.makeText(this@MainActivity, "Clicked", Toast.LENGTH_SHORT).show()
            }

            tileKomoditas.setValueIcon(com.rekanesia.R.drawable.ic_rekanesia_info, null)

            choose2.setIconStart(R.drawable.ic_edit_profile)

            val spannableString = SpannableStringBuilder("KOIN: ")
                .bold {
                    color(ContextCompat.getColor(this@MainActivity, R.color.purple_500)) {
                        append("+ 20.000 Poin")
                    }
                }

            choose2.setSpannableText(spannableString)
        }
    }

    private val resultPhoto = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK && result.data != null) {
            Toast.makeText(this, "Sukses", Toast.LENGTH_SHORT).show()
        }
    }
}