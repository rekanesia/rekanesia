package com.rekanesia_test.example.showcase.choose

import android.graphics.Bitmap
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia_test.databinding.ActivityChooseShowcaseBinding

class ChooseActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChooseShowcaseBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChooseShowcaseBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            chooseNormal.setOnClickListener {

            }
            chooseCustom1.setOnClickListener {

            }

            chooseCustom2.setOnClickListener {

            }

            chooseNormalIpubers.setOnClickListener {

            }

            chooseCustom1Ipubers.setOnClickListener {

            }

            chooseCustom2Ipubers.setOnClickListener {

            }
        }
    }

    private fun setIconProgramatically(@DrawableRes icon: Int) {
        binding.apply {
            chooseNormal.setIconEnd(icon)
            chooseNormal.setIconStart(icon)
        }
    }

    private fun setIconBitmapProgramatically(bitmap: Bitmap) {
        binding.apply {
            chooseNormal.setIconStart(bitmap)
            chooseNormal.setIconEnd(bitmap)
        }
    }

    private fun setterChoose() {
        binding.apply {
            // Use this function if you want to make a highlighted text
            chooseNormal.setSpannableText(null)

            // Use this property to set text normally
            chooseNormal.text = ""

            // Go to the Choose Class if you want to see more
        }
    }
}