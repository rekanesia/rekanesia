package com.rekanesia_test.example.showcase.options

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia.R
import com.rekanesia_test.databinding.ActivityOptionsBinding
import com.rekanesia.options.Opt
import com.rekanesia.options.TopRightContent
import com.rekanesia.options.TypeListView

class OptionsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOptionsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOptionsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        generateOptions()
        handleOnSelectOptions()

    }

    private fun handleOnSelectOptions() {
        binding.apply {
            multipleOptionMultiView.onMultipleOptionsSelected { typeListViews ->
                Log.d("OptionsActivity", "handleOnSelectOptions: $typeListViews")
            }
            singleOptionMultiView.onOptionSelected { typeView ->
                Log.d("OptionsActivity", "handleOnSelectOptions: $typeView")
            }
            options1.onOptionSelected { option ->
                // Do Something
            }

            options2.onOptionSelected { option ->
                // Do Something
            }

            options3.onMultipleOptionsSelected { options ->
                // Do Something
            }

            options4.onOptionSelected { option ->
                // Do Something
            }

            options4.onSelectedTextFieldChange { text ->
                // Do Something
            }
        }
    }

    private fun getSelectedOptions() {
        binding.apply {
            val option1 = options1.getSelectedItem()
            val option2 = options2.getSelectedItem()
            val option3 = options3.getSelectedItems()
            val option4 = options4.getSelectedItem()
        }
    }

    private fun generateOptions() {
        //multiple view
        val dataOption = listOf(
            TypeListView.ViewTypeGeneral(
                id = 1,
                label = "Option 1",
                selected = false,
            ),
            TypeListView.ViewTypeWithFourSideInformation(
                id = 2,
                topLeftValue = "Option 2",
                topRightValue = TopRightContent.Text("Top Right Text"),
                bottomRightValue = "bottom Left Text",
                bottomLeftValue = "Rp. 1.000.000.000",
                selected = false,
            ),
            TypeListView.ViewTypeWithFourSideInformation(
                id = 3,
                topLeftValue = "Option 3",
                topRightValue = TopRightContent.DrawableResource(com.rekanesia_test.R.drawable.bank_logo),
                bottomRightValue = "bottom Left Text",
                bottomLeftValue = "Rp. 1.000.000.000",
                selected = false,
            )
        )

        binding.programmaticallyOptionMultiView.replaceItems(dataOption)


        val options = listOf<Opt>(
            Opt(
                id = 1,
                label = "Option 1",
                selected = false,
                providedId = "#11"
            ),
            Opt(
                id = 2,
                label = "Option 2",
                selected = false,
                providedId = "#12"
            ),
            Opt(
                id = 3,
                label = "Option 3",
                selected = false,
                providedId = "#13"
            ),
        )
        binding.options2.replaceItems(options)
        binding.options3.replaceItems(options)
        binding.options2Ipubers.replaceItems(options)
        binding.options3Ipubers.replaceItems(options)
    }
}