package com.rekanesia_test.example.showcase.number_picker

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia_test.databinding.ActivityNumberPickerBinding

class NumberPickerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNumberPickerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNumberPickerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.numberPicker.debounceTime = 2000
        binding.numberPicker.setListener{ count->
            Log.d("TESTT",count.toString())
        }

        binding.numberPickerIpubers.setListener {

        }

        binding.numberPickerDisabled.setListener {

        }

        binding.numberPickerError.setListener { number ->
            binding.numberPickerError.isError = number > 5
        }
    }
}