package com.rekanesia_test.example.showcase.checkbox_selector

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia.checkbox_selector.CheckboxSelector
import com.rekanesia.helper.GeneralUtils.showKeyboardAndFocus
import com.rekanesia.helper.TextFieldHelper.hideKeyboard
import com.rekanesia_test.databinding.ActivityCheckboxSelectorBinding

class CheckBoxSelectorActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCheckboxSelectorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckboxSelectorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val list = listOf(
            CheckboxSelector(
                id = "1",
                customId = "epicuri",
                text = "Semua Produk",
                checked = true
            ),
            CheckboxSelector(
                id = "2",
                customId = "UN40",
                text = "Urea N46%",
                checked = false
            ),
            CheckboxSelector(
                id = "3",
                customId = "epicu123",
                text = "NPK Phonska",
                checked = false
            ),
        )
        binding.csView.submitList(list)
        binding.csView.setOnSingleCheckedListener { item ->
            Log.d("CheckboxSelectorActivity", "setOnSingleCheckedListener: $item")
        }

        binding.csViewRekan.submitList(list)
        binding.csViewRekan.setOnSingleCheckedListener { item ->
            Log.d("CheckboxSelectorActivity", "setOnSingleCheckedListener: $item")
        }

        binding.csViewMultiple.submitList(list)
        binding.csViewMultiple.isEnabledClick = false
        binding.csViewMultiple.setOnMultipleCheckedListener { item ->
            Log.d("CheckboxSelectorActivity", "setOnMultipleCheckedListener: $item")
        }

        binding.csViewMultipleRekan.submitList(list)
        binding.csViewMultipleRekan.setOnMultipleCheckedListener { item ->
            Log.d("CheckboxSelectorActivity", "setOnMultipleCheckedListener: $item")
        }

        binding.btnSubmit.setOnClickListener {
            val checkedItems = binding.csView.getCheckedItems()
            val checkedItemsRekan = binding.csViewRekan.getCheckedItems()
            val checkedItemsMultiple = binding.csViewMultiple.getCheckedItems()
            val checkedItemsMultipleRekan = binding.csViewMultipleRekan.getCheckedItems()
            Log.d("CheckboxSelectorActivity", "checkedItems: $checkedItems")
            Log.d("CheckboxSelectorActivity", "checkedItemsRekan: $checkedItemsRekan")
            Log.d("CheckboxSelectorActivity", "checkedItemsMultiple: $checkedItemsMultiple")
            Log.d("CheckboxSelectorActivity", "checkedItemsMultipleRekan: $checkedItemsMultipleRekan")
        }

        val listCheckbox = mutableListOf(
            CheckboxSelector(
                text = "Salah Input Produk",
            ),
            CheckboxSelector(
                text = "Ingin Mengubah Alamat",
            ),
            CheckboxSelector(
                text = "Alasan Lain",
                requiredTextField = true,
            ),
        )
        binding.csViewTextField.submitList(listCheckbox)
        binding.csViewTextField.setOnSingleCheckedListener { item ->
            if (item.text == "Alasan Lain") {
                binding.csViewTextField.showKeyboardAndFocus(window)
            } else {
                hideKeyboard(binding.csViewTextField)
            }
        }
        binding.csViewTextField.setOnTextFieldChangeListener { text ->
            Log.d("CheckboxSelectorActivity", "setOnTextFieldChangeListener: $text")
        }
    }
}