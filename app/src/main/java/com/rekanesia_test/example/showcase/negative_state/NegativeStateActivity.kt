package com.rekanesia_test.example.showcase.negative_state

import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.rekanesia.R
import com.rekanesia_test.databinding.ActivityNegativeStateBinding

class NegativeStateActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNegativeStateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNegativeStateBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val blackColor = ContextCompat.getColor(this, R.color.M800)
        val spannableText = "Hello World".highlightSubstring("World", blackColor, true)
        binding.ns1.setDescription(spannableText)
    }

    private fun String.highlightSubstring(targetText: String, color: Int, bold: Boolean = false): SpannableStringBuilder {
        return try {
            val startIndex = this.indexOf(targetText)
            val endIndex = startIndex + targetText.length

            val spannableStringBuilder = SpannableStringBuilder(this)
            spannableStringBuilder.setSpan(ForegroundColorSpan(color), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            if (bold) {
                spannableStringBuilder.setSpan(StyleSpan(Typeface.BOLD), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }

            spannableStringBuilder
        } catch (e: Exception) {
            e.printStackTrace()
            SpannableStringBuilder(this)
        }
    }
}