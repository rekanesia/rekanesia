package com.rekanesia_test.example.showcase

data class Showcase(
    val type: ShowcaseEnum,
    val title: String,
    val name: String
)

enum class ShowcaseEnum {
    Input,
    LeftInput,
    RightInput,
    Choose,
    SearchBox,
    TextArea,
    Options,
    Chips,
    Badge,
    Banner,
    BannerNotifier,
    DataTile,
    ImagePopup,
    NegativeState,
    ListTile,
    NumberPicker,
    SelectionInput,
    Shadow,
    Toolbar,
    Navigation,
    CheckboxSelector
}

val showcases = mutableListOf(
    Showcase(
        ShowcaseEnum.Input,
        "Input",
        "Contoh dan use case component Text Field InputText"
    ),
    Showcase(
        ShowcaseEnum.LeftInput,
        "Left Input",
        "Contoh dan use case component Text Field LeftInputText"
    ),
    Showcase(
        ShowcaseEnum.RightInput,
        "Right Input",
        "Contoh dan use case component Text Field RightInputText"
    ),
    Showcase(
        ShowcaseEnum.Choose,
        "Choose",
        "Contoh dan use case component Text Field Choose"
    ),
    Showcase(
        ShowcaseEnum.SearchBox,
        "Search Box",
        "Contoh dan use case component Text Field Search Box"
    ),
    Showcase(
        ShowcaseEnum.TextArea,
        "Text Area",
        "Contoh dan use case component Text Field Text Area"
    ),
    Showcase(
        ShowcaseEnum.Options,
        "Options",
        "Contoh dan use case component Options Group"
    ),
    Showcase(
        ShowcaseEnum.Chips,
        "Chips",
        "Contoh dan use case component Chips"
    ),
    Showcase(
        ShowcaseEnum.Badge,
        "Badge",
        "Contoh dan use case component Badge"
    ),
    Showcase(
        ShowcaseEnum.Banner,
        "Banner",
        "Contoh dan use case component Banner"
    ),
    /*Showcase(
        ShowcaseEnum.BannerNotifier,
        "Banner Notifier",
        "Contoh dan use case component Banner Notifier"
    ),*/
    Showcase(
        ShowcaseEnum.DataTile,
        "Data Tile",
        "Contoh dan use case component Data Tile"
    ),
    Showcase(
        ShowcaseEnum.ImagePopup,
        "Image Popup",
        "Contoh dan use case component Image Pop Up"
    ),
    Showcase(
        ShowcaseEnum.NegativeState,
        "Negative State",
        "Contoh dan use case component Negative State"
    ),
    Showcase(
        ShowcaseEnum.ListTile,
        "List Tile",
        "Contoh dan use case component List Tile"
    ),
    Showcase(
        ShowcaseEnum.NumberPicker,
        "Number Picker",
        "Contoh dan use case component Number Picker"
    ),
    Showcase(
        ShowcaseEnum.Shadow,
        "Shadow",
        "Contoh dan use case component Shadow Layout"
    ),
    Showcase(
        ShowcaseEnum.Toolbar,
        "Toolbar",
        "Contoh dan use case component Shadow Layout"
    ),
    Showcase(
        ShowcaseEnum.SelectionInput,
        "Selection Input",
        "Contoh dan use case component Selection Input"
    ),
    Showcase(
        ShowcaseEnum.Navigation,
        "Navigation Component",
        "percobaan input saat state Navigation"
    ),
    Showcase(
        ShowcaseEnum.CheckboxSelector,
        "Checkbox Selector",
        "Contoh dan use case component Checkbox Selector"
    )
)