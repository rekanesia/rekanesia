package com.rekanesia_test.example.showcase

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.rekanesia_test.databinding.ActivityListShowcaseBinding
import com.rekanesia_test.example.navigation.NavigationActivityPercobaan
import com.rekanesia_test.example.showcase.ShowcaseEnum.*
import com.rekanesia_test.example.showcase.banner.BannerActivity
import com.rekanesia_test.example.showcase.checkbox_selector.CheckBoxSelectorActivity
import com.rekanesia_test.example.showcase.chip.ChipActivity
import com.rekanesia_test.example.showcase.choose.ChooseActivity
import com.rekanesia_test.example.showcase.data_tile.DataTileActivity
import com.rekanesia_test.example.showcase.input.ExampleInputActivity
import com.rekanesia_test.example.showcase.list_tile.ListTileActivity
import com.rekanesia_test.example.showcase.negative_state.NegativeStateActivity
import com.rekanesia_test.example.showcase.number_picker.NumberPickerActivity
import com.rekanesia_test.example.showcase.options.OptionsActivity
import com.rekanesia_test.example.showcase.searchbox.SearchBoxActivity
import com.rekanesia_test.example.showcase.selection_input.SelectionInputActivity
import com.rekanesia_test.example.showcase.text_area.TextAreaActivity

class ListShowcaseActivity : AppCompatActivity() {

    private lateinit var binding: ActivityListShowcaseBinding
    private val showcaseAdapter: ShowcaseAdapter by lazy { ShowcaseAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListShowcaseBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarRekan.toolbar)

        setupAdapter()

        generateListShowcase()

        onViewClick()
    }

    private fun onViewClick() {
        showcaseAdapter.setOnItemClick { showcase ->
            when (showcase.type) {
                Input -> startActivity(Intent(this, ExampleInputActivity::class.java))
                LeftInput -> Unit
                RightInput -> Unit
                Choose -> startActivity(Intent(this, com.rekanesia_test.example.showcase.choose.ChooseActivity::class.java))
                SearchBox -> startActivity(Intent(this, SearchBoxActivity::class.java))
                TextArea -> startActivity(Intent(this, com.rekanesia_test.example.showcase.text_area.TextAreaActivity::class.java))
                Options -> startActivity(Intent(this, OptionsActivity::class.java))
                Chips -> startActivity(Intent(this, com.rekanesia_test.example.showcase.chip.ChipActivity::class.java))
                Badge -> Unit
                Banner -> startActivity(Intent(this, BannerActivity::class.java))
                BannerNotifier -> Unit
                DataTile -> startActivity(Intent(this, DataTileActivity::class.java))
                ListTile -> startActivity(Intent(this, ListTileActivity::class.java))
                NumberPicker -> startActivity(Intent(this, NumberPickerActivity::class.java))
                SelectionInput -> startActivity(Intent(this, SelectionInputActivity::class.java))
                NegativeState -> startActivity(Intent(this, NegativeStateActivity::class.java))
                Navigation -> startActivity(Intent(this, NavigationActivityPercobaan::class.java))
                CheckboxSelector -> startActivity(Intent(this, CheckBoxSelectorActivity::class.java))
                else -> Unit
            }
        }
    }

    private fun setupAdapter() {
        binding.rvShowcase.apply {
            adapter = showcaseAdapter
            layoutManager = LinearLayoutManager(this@ListShowcaseActivity)
            setHasFixedSize(true)
        }
    }

    private fun generateListShowcase() {
        showcaseAdapter.submitList(showcases)
    }
}