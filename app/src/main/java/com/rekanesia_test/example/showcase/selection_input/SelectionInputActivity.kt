package com.rekanesia_test.example.showcase.selection_input

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia.FontWeightDataTile
import com.rekanesia.R
import com.rekanesia_test.databinding.ActivitySelectionInputBinding
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.selection_input.SelectionInputChildModel
import com.rekanesia.selection_input.SelectionInputChildTextFieldModel
import com.rekanesia.selection_input.SelectionInputModel
import com.rekanesia.selection_input.SelectionInputStyle
import com.rekanesia.selection_input.TextFieldType

class SelectionInputActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySelectionInputBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectionInputBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setRekanItems()


        setRekanChildItems()

        setRekanChildTextField()

        setIpubersChildItems()

        setSiTestIpubersItems()


    }

    private fun setSiTestIpubersItems() {
        val items = mutableListOf<SelectionInputModel>()
        val style = SelectionInputStyle(
            titleStartFontWeight = FontWeightDataTile.Bold,
            showBody = true,
            addBackground = true
        )
        items.add(
            SelectionInputModel(
                0,
                titleStart = "Tani Jaya Lestari (Saya)",
                bodyStart = "Perbon, Tuban",
                isSelected = true,
                providedId = "RT01",
                style = style
            ),
        )
        items.add(
            SelectionInputModel(
                1,
                titleStart = "Elang Naga Jaya",
                bodyStart = "Jenu, Tuban",
                isSelected = false,
                providedId = "RT02",
                style = style
            ),
        )
        items.add(
            SelectionInputModel(
                2,
                titleStart = "Tani Makmur",
                bodyStart = "Jenu, Tuban",
                isSelected = false,
                providedId = "RT03",
                style = style
            ),
        )
        binding.test.setItems(items)
        binding.test.setOnSelectionChanged { item ->
            Log.d("SelectionInput", item.toString())
        }
    }

    private fun setRekanItems() {
        val items = mutableListOf<SelectionInputModel>()
        val style = SelectionInputStyle(
            titleStartFontWeight = FontWeightDataTile.Bold,
            titleEndFontWeight = FontWeightDataTile.Bold,
            titleEndColor = R.color.B500,
            showBody = true
        )
        items.add(
            SelectionInputModel(
                id = 0,
                titleStart = "Title 0",
                titleEnd = "End 0",
                bodyStart = "Body Start 0",
                bodyEnd = "Body End 0",
                iconTitleStart = R.drawable.ic_calendar,
                style = style
            )
        )
        repeat(3) {
            items.add(
                SelectionInputModel(
                    id = Math.random().toInt(),
                    titleStart = "Title ${it+1}",
                    titleEnd = "End ${it+1} ",
                    style = style
                )
            )
        }
        binding.selectionInput.setItems(items)
        binding.selectionInput.setOnSelectionChanged { selection ->
            Log.d("SelectionInput", selection.toString())
        }
    }

    private fun setIpubersItems() {
        val itemsIpubers = mutableListOf<SelectionInputModel>()
        repeat(3) {
            itemsIpubers.add(
                SelectionInputModel(
                    id = it,
                    titleStart = "Title $it",
                    titleEnd = "End $it",
                    bodyStart = "Body Start $it",
                    bodyEnd = "Body end $it"
                )
            )
        }
        binding.selectionInputIpubers.setItems(itemsIpubers)
        binding.selectionInputIpubersChild.setOnSelectionChanged { selection ->
            Log.d("SelectionInput", selection.toString())
        }
    }

    private fun setRekanChildItems() {
        val items = mutableListOf<SelectionInputModel>()
        val itemsChild = mutableListOf(
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "3x",
                titleEnd = "Rp. 150.000"
            ),
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "Subtitle",
                titleEnd = "Subtitle 2"
            ),
        )
        val itemsChild2 = mutableListOf(
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "3x",
                titleEnd = "Rp. 150.000",
                bodyStart = "Bunga 1%",
                bodyEnd = "Rp. 200.000"
            ),
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "Subtitle",
                titleEnd = "Subtitle 2",
                bodyStart = "Biaya layanan 0.5%",
                bodyEnd = "Biaya layanan 0.5%"
            )
        )
        repeat(3) {
            items.add(
                SelectionInputModel(
                    id = it,
                    titleStart = "Title $it",
                    titleEnd = "End $it",
                )
            )
        }
        items[0].childs = itemsChild
        items[1].childs = itemsChild2

        binding.selectionInputChild.setItems(items)
        binding.selectionInputChild.setOnSelectionChanged { selection ->
            Log.d("SelectionInput", selection.toString())
        }
    }

    private fun setIpubersChildItems() {
        val items = mutableListOf<SelectionInputModel>()
        val itemsChild = mutableListOf(
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "3x",
                titleEnd = "Rp. 150.000"
            ),
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "Subtitle",
                titleEnd = "Subtitle 2"
            ),
        )
        val itemsChild2 = mutableListOf(
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "3x",
                titleEnd = "Rp. 150.000",
                bodyStart = "Bunga 1%",
                bodyEnd = "Rp. 200.000"
            ),
            SelectionInputChildModel(
                id = Math.random().toInt(),
                titleStart = "Subtitle",
                titleEnd = "Subtitle 2",
                bodyStart = "Biaya layanan 0.5%",
                bodyEnd = "Biaya layanan 0.5%"
            )
        )
        val itemChild3 = mutableListOf(
            SelectionInputChildTextFieldModel(
                id = Math.random().toInt(),
                hint = "Placeholder",
                inputType = InputTypeRekanesia.Currency,
                prefix = "Rp ",
                suffix = " /Ton"
            ),
        )
        repeat(3) {
            items.add(
                SelectionInputModel(
                    id = it,
                    titleStart = "Title $it",
                    titleEnd = "End $it",
                )
            )
        }
        items[0].childs = itemsChild
        items[1].childs = itemsChild2
        items[2].childsTextField = itemChild3

        binding.selectionInputIpubersChild.setItems(items)
        binding.selectionInputIpubersChild.setOnSelectionChanged { selection ->
            Log.d("SelectionInput", selection.toString())
        }
    }

    private fun setRekanChildTextField() {
        val items = mutableListOf<SelectionInputModel>()
        val itemsChild = mutableListOf(
            SelectionInputChildTextFieldModel(
                id = Math.random().toInt(),
                hint = "Placeholder",
                suffix = " Buah"
            ),
            SelectionInputChildTextFieldModel(
                id = Math.random().toInt(),
                hint = "Placeholder",
                prefix = "Buah ",
                suffix = " /Kg"
            )
        )
        val itemsChild2 = mutableListOf(
            SelectionInputChildTextFieldModel(
                id = Math.random().toInt(),
                hint = "Placeholder",
                textFieldType = TextFieldType.TextArea
            )
        )
        val itemsChild3 = mutableListOf(
            SelectionInputChildTextFieldModel(
                id = Math.random().toInt(),
                hint = "Placeholder",
                textFieldType = TextFieldType.Choose,
                iconEnd = R.drawable.ic_chevron_right,
                iconStart = R.drawable.ic_calendar
            )
        )
        repeat(3) {
            items.add(
                SelectionInputModel(
                    id = it,
                    titleStart = "Title $it",
                    titleEnd = "End $it",
                )
            )
        }
        items[0].childsTextField = itemsChild
        items[1].childsTextField = itemsChild2
        items[2].childsTextField = itemsChild3

        binding.selectionInputChildTextField.setItems(items)
        binding.selectionInputChildTextField.setOnSelectionChanged { selection ->
            Log.d("SelectionInput", selection.toString())
        }
    }

}