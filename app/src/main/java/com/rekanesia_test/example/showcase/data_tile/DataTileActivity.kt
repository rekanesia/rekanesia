package com.rekanesia_test.example.showcase.data_tile

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia.FontWeightDataTile
import com.rekanesia.R
import com.rekanesia_test.databinding.ActivityDataTileBinding

class DataTileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDataTileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDataTileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tile1.setOnClickListener {

        }

        handleSetterDataTile()
    }

    private fun handleSetterDataTile() {
        binding.apply {
            tile1.label = "Set Label"
            tile1.value = "Set Value"

            tile2.setValueFontWeight(FontWeightDataTile.Bold)
            tile2.isValueClickable = true
            tile2.setOnValueClick {
                Log.d("DataTile", "Fire")
            }

            tile3.label = "binding.textValue.typeface = mati muda dia kak keknya yaa awlkjwad awlkdjaw"
            tile3.value = null

            tile1.setLabelTextSize(20F)
            tile1.setValueTextSize(18F)

            // Handle Click Event on value
            tile4.setOnValueClick {

            }

            // Please Check DataTile class for further information
        }
    }
}