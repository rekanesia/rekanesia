package com.rekanesia_test.example.showcase.left_input

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rekanesia.R
import com.rekanesia_test.databinding.ActivityLeftInputBinding

class LeftInputActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLeftInputBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeftInputBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}