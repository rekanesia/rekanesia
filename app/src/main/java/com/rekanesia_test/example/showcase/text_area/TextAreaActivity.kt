package com.rekanesia_test.example.showcase.text_area

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doOnTextChanged
import com.rekanesia_test.databinding.ActivityTextAreaBinding

class TextAreaActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTextAreaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTextAreaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            textAreaCustom.input.doOnTextChanged { text, _, _, _ ->
                val textArea = text.toString()
                textAreaCustom.isError = textArea.isNotEmpty()
            }

            textAreaCustomIpubers.input.doAfterTextChanged { text ->
                val textArea = text.toString()
                textAreaCustomIpubers.isError = textArea.isNotEmpty()
            }
        }
    }
}