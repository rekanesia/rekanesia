package com.rekanesia_test.example.showcase.searchbox

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia_test.databinding.ActivitySearchBoxBinding

class SearchBoxActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySearchBoxBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBoxBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {

            searchBox.onQueryChangeListener { text ->
                // Do Something on query change
            }

            searchBoxWithIcon.setOnIconEndClickListener {
                // Do something on icon end clicked
            }
        }
    }
}