package com.rekanesia_test.example.showcase.banner

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia_test.databinding.ActivityBannerBinding

class BannerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBannerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBannerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bannerNotifier.setOnClickListener {

        }
    }
}