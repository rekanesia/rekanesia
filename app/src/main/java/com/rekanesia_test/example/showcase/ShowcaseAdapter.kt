package com.rekanesia_test.example.showcase

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rekanesia_test.databinding.ItemShowcaseBinding

class ShowcaseAdapter: ListAdapter<Showcase, ShowcaseAdapter.ItemViewHolder>(diffCallback) {

    private var onItemClick: ((Showcase) -> Unit)? = null
    fun setOnItemClick(onClick: (Showcase) -> Unit) {
        onItemClick = onClick
    }

    inner class ItemViewHolder(private val binding: ItemShowcaseBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Showcase) {
            binding.apply {
                tvTitle.text = item.title
                tvDescription.text = item.name

                root.setOnClickListener {
                    onItemClick?.invoke(item)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = ItemShowcaseBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Showcase>() {
            override fun areItemsTheSame(oldItem: Showcase, newItem: Showcase): Boolean {
                return oldItem.type == newItem.type
            }

            override fun areContentsTheSame(oldItem: Showcase, newItem: Showcase): Boolean {
                return oldItem == newItem
            }
        }
    }
}