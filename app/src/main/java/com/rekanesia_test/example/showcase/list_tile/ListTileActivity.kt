package com.rekanesia_test.example.showcase.list_tile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia_test.databinding.ActivityListTileBinding

class ListTileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityListTileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListTileBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}