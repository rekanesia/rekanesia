package com.rekanesia_test.example.showcase.input

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia_test.databinding.ActivityExampleInputBinding

class ExampleInputActivity : AppCompatActivity() {
    private lateinit var binding: ActivityExampleInputBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityExampleInputBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarRekan.toolbar)

        caseAddError()

    }

    private fun caseAddError() {
        binding.apply {
            errorInput.addTextChangedListener { text ->
                errorInput.isError = text.isNullOrEmpty()
            }

            errorInputIpubers.addTextChangedListener { text ->
                errorInputIpubers.isError = text.isNullOrEmpty()
            }

            customInput.setOnDrawableClickListener {
                Toast.makeText(this@ExampleInputActivity, "Clicked", Toast.LENGTH_SHORT).show()
            }
        }
    }
}