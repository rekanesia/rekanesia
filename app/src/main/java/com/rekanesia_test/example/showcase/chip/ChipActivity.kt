package com.rekanesia_test.example.showcase.chip

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia.chip.ChipOption
import com.rekanesia_test.databinding.ActivityChipBinding

class ChipActivity : AppCompatActivity() {

    private lateinit var binding: ActivityChipBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChipBinding.inflate(layoutInflater)
        setContentView(binding.root)

        generateChips()

        handleClickEvent()
    }

    private fun handleClickEvent() {
        binding.apply {
            chips.setOnChipSelectedListener { chip ->
                // Do Something
            }

            chips2.setOnMultipleChipsSelectedListener { chip ->
                // Do Something
            }

            chips3.setOnChipSelectedIndexed { i, chipOption ->
                // Do Something
            }
        }
    }

    private fun generateChips() {
        binding.apply {
            val dynamicChips = listOf(
                ChipOption(
                    index = 1,
                    name = "Chip 1",
                    id = "#1"
                ),
                ChipOption(
                    index = 2,
                    name = "Chip 2",
                    id = "#2"
                ),
                ChipOption(
                    index = 3,
                    name = "Chip 3",
                    id = "#3"
                ),
                ChipOption(
                    index = 4,
                    name = "Chip 4",
                    id = "#4"
                ),
            )
            chips2.addAllChip(dynamicChips)

            val dynamicChips2 = listOf(
                ChipOption(
                    index = 1,
                    name = "Chip 1",
                    id = "#1"
                ),
                ChipOption(
                    index = 2,
                    name = "Chip 2",
                    id = "#2",
                    childChips = listOf(
                        "1",
                        "2",
                        "3"
                    )
                ),
                ChipOption(
                    index = 3,
                    name = "Chip 3",
                    id = "#3"
                ),
                ChipOption(
                    index = 4,
                    name = "Chip 4",
                    id = "#4"
                ),
            )
            chips3.addAllChip(dynamicChips2)
        }
    }
}