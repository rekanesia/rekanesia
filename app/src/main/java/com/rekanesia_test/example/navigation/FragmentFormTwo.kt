package com.rekanesia_test.example.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rekanesia_test.databinding.FragmentNavigationDuaBinding
import com.rekanesia_test.databinding.FragmentNavigationSatuBinding

class FragmentFormTwo:Fragment() {
    private var _binding:FragmentNavigationDuaBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNavigationDuaBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(activity != null){

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}