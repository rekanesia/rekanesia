package com.rekanesia_test.example

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.rekanesia_test.databinding.ActivitySecondBinding
import com.rekanesia.options.Opt

class SecondActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.negativeState.setOnActionButtonListener {
            Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show()
        }

        val options = mutableListOf<Opt>()
        for (i in 1..20) {
            options.add(
                Opt(
                    i, "selections", false
                )
            )
        }
        binding.options.replaceItems(options)
        binding.options.setEnabledNestedScroll(true)
    }
}