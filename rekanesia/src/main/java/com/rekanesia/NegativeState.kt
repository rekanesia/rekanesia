package com.rekanesia

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.StyleRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.content.res.getResourceIdOrThrow
import androidx.core.content.res.getStringOrThrow
import androidx.core.view.isVisible
import com.airbnb.paris.extensions.style
import com.rekanesia.databinding.LayoutNegativeStateBinding
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*

class NegativeState : ConstraintLayout {

    private lateinit var binding: LayoutNegativeStateBinding

    private var style = Rekan

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    var title: String = ""
        set(value) {
            field = value
            if (field.isEmpty() || field == "null") field = ""
            binding.tvTitle.text = field
        }
        get() = binding.tvTitle.text.toString()

    var description: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            binding.tvDescription.text = field
            binding.tvDescription.isVisible = !field.isNullOrEmpty()
        }
        get() = binding.tvDescription.text.toString()

    var isShowActionButton = true
        set(value) {
            field = value
            binding.btnAction.isVisible = field
        }

    var textButton: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            binding.btnAction.text = field
        }
        get() = binding.btnAction.text



    fun setOnActionButtonListener(onClick: () -> Unit) {
        binding.btnAction.setOnClickListener { onClick.invoke() }
    }

    fun setStyle(@StyleRes style:Int){
        binding.btnAction.style(style)
    }

    fun setIcon(resId: Int) {
        binding.imgNegativeState.setImageResource(resId)
    }

    fun setIcon(drawable: Drawable) {
        binding.imgNegativeState.setImageDrawable(drawable)
    }

    fun setIcon(bitmap: Bitmap) {
        binding.imgNegativeState.setImageBitmap(bitmap)
    }

    fun setIcon(uri: Uri) {
        binding.imgNegativeState.setImageURI(uri)
    }

    fun setDescription(spannableText: SpannableStringBuilder) {
        binding.tvDescription.setText(spannableText)
    }


    private fun init(attrs: AttributeSet) {
        binding = LayoutNegativeStateBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.NegativeState)

        title = typedArray.getStringOrThrow(R.styleable.NegativeState_rekan_title)
        description = typedArray.getString(R.styleable.NegativeState_rekan_description).orEmpty()
        textButton = typedArray.getString(R.styleable.NegativeState_rekan_textButton).orEmpty()
        isShowActionButton =
            typedArray.getBoolean(R.styleable.NegativeState_rekan_showActionButton, true)
        style = values()[typedArray.getInt(R.styleable.NegativeState_rekan_styles, 1) - 1]
        val icon = typedArray.getResourceIdOrThrow(R.styleable.NegativeState_rekan_icon)

        setIcon(icon)

        setText()

        setButton()

        setStyle()

        typedArray.recycle()
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvTitle.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    tvDescription.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    tvTitle.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    tvDescription.setTextColor(ContextCompat.getColor(context, R.color.D500))
                }
                Ipubers -> {
                    tvTitle.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                    tvDescription.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    tvTitle.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    tvDescription.setTextColor(ContextCompat.getColor(context, R.color.M500))
                }
            }
        }
    }

    private fun setText() {
        binding.apply {
            tvTitle.text = title
            tvDescription.text = description
            tvDescription.isVisible = !(description.isNullOrEmpty() || description == "null")
        }
    }

    private fun setButton() {
        binding.apply {
            btnAction.isVisible = isShowActionButton
            btnAction.text = textButton
        }
    }

    fun setState(type:Int){
        val title:String
        val description:String
        binding.tvDescription.isVisible = true
        when(type){
            empty->{
                binding.imgNegativeState.setImageDrawable(
                    ContextCompat.getDrawable(context,R.drawable.ic_no_product)
                )
                title = "Data Kosong"
                description = ""
            }
            noInternet->{
                binding.imgNegativeState.setImageDrawable(
                    ContextCompat.getDrawable(context,R.drawable.ic_no_internet)
                )
                title = "Tidak Ada Koneksi"
                description = "Anda tidak terhubung ke internet, harap periksa jaringan internet anda"
            }
            failure->{
                binding.imgNegativeState.setImageDrawable(
                    ContextCompat.getDrawable(context,R.drawable.ic_500)
                )
                title = "Terjadi Masalah"
                description = "mohon maaf, anda bisa coba dalam beberapa saat lagi, atau hubungi tim helpdesk rekan"
            }
            else -> throw IllegalStateException("type must be empty,noInternet or failure .. or can be 1,2 or 3")
        }

        binding.tvTitle.text = title
        binding.tvDescription.text = description
    }

    fun setState(bitmap: Bitmap,title:String,description:String?=null){
        binding.imgNegativeState.setImageBitmap(bitmap)
        binding.tvTitle.text = title
        binding.tvDescription.isVisible = !(description.isNullOrEmpty() || description == "null")
        binding.tvDescription.text = description
    }

    fun setState(drawable: Drawable,title:String,description:String?=null){
        binding.imgNegativeState.setImageDrawable(drawable)
        binding.tvTitle.text = title
        binding.tvDescription.isVisible = !(description.isNullOrEmpty() || description == "null")
        binding.tvDescription.text = description
    }

    fun setState(uri: Uri,title:String,description:String?=null){
        binding.imgNegativeState.setImageURI(uri)
        binding.tvTitle.text = title
        binding.tvDescription.isVisible = !(description.isNullOrEmpty() || description == "null")
        binding.tvDescription.text = description
    }

    companion object {
        const val empty = 1
        const val noInternet = 2
        const val failure = 3
    }
}