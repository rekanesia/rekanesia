package com.rekanesia.toolbar

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.DrawableRes
import androidx.core.view.isVisible
import com.google.android.material.appbar.MaterialToolbar
import com.rekanesia.R
import com.rekanesia.databinding.LayoutMaterialToolbarBinding

class MaterialToolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : MaterialToolbar(context, attrs, defStyle) {
    private var binding: LayoutMaterialToolbarBinding

    var label = ""
        set(value) {
            field = value
            binding.tvToolbar.text = value
        }

    private var onBackButtonPressed: (() -> Unit)? = null
    fun onBackButtonPressed(onClick: (() -> Unit)) {
        onBackButtonPressed = onClick
    }

    private var onIconEndClick: (() -> Unit)? = null
    fun setOnIconEndClickListener(onClick: () -> Unit) {
        onIconEndClick = onClick
    }

    init {
        binding = LayoutMaterialToolbarBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.MaterialToolbar, 0, 0)
        val pageTitle = typedArray.getString(R.styleable.MaterialToolbar_rekan_label).orEmpty()
        val icon = typedArray.getResourceId(R.styleable.MaterialToolbar_rekan_iconEnd, -1)
        setIconEnd(icon)
        binding.tvToolbar.text = pageTitle
        binding.imgBack.setOnClickListener {
            onBackButtonPressed?.invoke()
        }
        binding.imgEnd.setOnClickListener {
            onIconEndClick?.invoke()
        }
        typedArray.recycle()
    }

    fun setIconEndVisibility(isVisible: Boolean) {
        binding.imgEnd.isVisible = isVisible
    }

    fun setIconEnd(@DrawableRes icon: Int?) {
        if (icon != -1 && icon != null) {
            binding.imgEnd.isVisible = true
            binding.imgEnd.setImageResource(icon)
        }
    }

    fun setIconEnd(bitmap: Bitmap?) {
        if (bitmap != null) {
            binding.imgEnd.apply {
                isVisible = true
                setImageBitmap(bitmap)
            }
        }
    }

    fun setIconEnd(drawable: Drawable?) {
        if (drawable != null) {
            binding.imgEnd.apply {
                isVisible = true
                setImageDrawable(drawable)
            }
        }
    }

    fun setIconEnd(uri: Uri?) {
        if (uri != null) {
            binding.imgEnd.apply {
                isVisible = true
                setImageURI(uri)
            }
        }
    }
}