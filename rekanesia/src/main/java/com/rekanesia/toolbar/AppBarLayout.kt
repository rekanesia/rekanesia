package com.rekanesia.toolbar

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import com.google.android.material.appbar.AppBarLayout
import com.rekanesia.R
import com.rekanesia.databinding.LayoutAppBarLayoutBinding

class AppBarLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppBarLayout(context, attrs, defStyle) {
    private var binding: LayoutAppBarLayoutBinding

    var label = ""
        set(value) {
            field = value
            binding.pageTitle.text = value
        }

    var toolbar: androidx.appcompat.widget.Toolbar
    var title: TextView

    init {
        binding = LayoutAppBarLayoutBinding.inflate(LayoutInflater.from(context), this)

        toolbar = binding.toolbar
        title = binding.pageTitle
        toolbar.title = ""

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.AppBarLayout, 0, 0)
        val pageTitle = typedArray.getString(R.styleable.AppBarLayout_rekan_label).orEmpty()
        binding.pageTitle.text = pageTitle
        typedArray.recycle()
    }
}