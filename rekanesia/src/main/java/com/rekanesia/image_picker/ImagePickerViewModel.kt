package com.rekanesia.image_picker

import android.content.ContentResolver
import android.content.ContentUris
import android.provider.MediaStore
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rekanesia.helper.sdk29AndUp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class ImagePickerViewModel : ViewModel() {

    private var _loading: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val loading get() = _loading.asStateFlow()

    private var _photos: MutableStateFlow<List<ImagePicker>> = MutableStateFlow(emptyList())
    val photos get() = _photos.asStateFlow()

    fun loadPhotosGallery(contentResolver: ContentResolver) {
        viewModelScope.launch {
            getPhotosFromGallery(contentResolver).collect { images ->
                _photos.value = images
            }
        }
    }

    private fun getPhotosFromGallery(contentResolver: ContentResolver): Flow<List<ImagePicker>> {
        _loading.value = true
        return flow<List<ImagePicker>> {
            val photos = mutableListOf<ImagePicker>()
            val collections = sdk29AndUp {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
            } ?: MediaStore.Images.Media.EXTERNAL_CONTENT_URI

            val projection = arrayOf(
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.WIDTH,
                MediaStore.Images.Media.HEIGHT,
                MediaStore.Images.Media.DATE_ADDED,
                MediaStore.MediaColumns.DATA,
            )

            val galleryCursor = contentResolver.query(
                collections,
                projection,
                null,
                null,
                "${MediaStore.Images.Media.DATE_ADDED} DESC"
            )
            galleryCursor.use { cursor ->
                cursor?.let {
                    val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
                    val displayNameColumn =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                    val widthColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.WIDTH)
                    val heightColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.HEIGHT)

                    while (cursor.moveToNext()) {
                        val id = cursor.getLong(idColumn)
                        val displayName = cursor.getString(displayNameColumn)
                        val width = cursor.getInt(widthColumn)
                        val height = cursor.getInt(heightColumn)
                        val contentUri = ContentUris.withAppendedId(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            id
                        )
                        photos.add(
                            ImagePicker(
                                id,
                                displayName,
                                width,
                                height,
                                contentUri,
                            )
                        )
                    }
                    emit(photos)
                }
            }
            galleryCursor?.close()
            _loading.value = false
        }.flowOn(Dispatchers.IO)
    }
}