package com.rekanesia.image_picker

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.rekanesia.RekanesiaCameraActivity
import com.rekanesia.R
import com.rekanesia.databinding.LayoutImagePickerBinding
import com.rekanesia.helper.ImageHelper.saveToStorages
import kotlinx.coroutines.launch
import java.io.File


class ImagePickerActivity : AppCompatActivity() {

    private lateinit var binding: LayoutImagePickerBinding
    private lateinit var galleryAdapter: GalleryAdapter

    private val viewModel: ImagePickerViewModel by viewModels()

    private var imageFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LayoutImagePickerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        if (!isStoragePermissionGranted()) {
            finish()
            Toast.makeText(
                this,
                "Perizinan camera dan penyimpanan belum diizinkan",
                Toast.LENGTH_SHORT
            ).show()
        }

        initAdapter()

        observeLoading()

        observePhotos()

        onViewClick()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadPhotosGallery(contentResolver)
    }

    private fun onViewClick() {
        binding.apply {
            imgBack.setOnClickListener {
                finish()
            }

            galleryAdapter.setOnItemClickListener { imagePicker, position ->
                imgSelected.setImageURI(imagePicker.contentUri)
                val images = galleryAdapter.currentList.toMutableList()
                val curIndexSelected = galleryAdapter.curIndexSelected
                if (galleryAdapter.curIndexSelected != -1) {
                    images[curIndexSelected].isSelected = false
                }
                images[position].isSelected = true
                galleryAdapter.notifyItemChanged(curIndexSelected)
                imageFile = File(getRealPathFromUri(imagePicker.contentUri) ?: "")
            }

            galleryAdapter.setOnTakePhotoListener {
                val intent = Intent(this@ImagePickerActivity, RekanesiaCameraActivity::class.java)
                cameraLauncher.launch(intent)
            }

            btnNext.setOnClickListener {
                if (pictureResult != null) {
                    val fileName = "foto"
                    val imageFile = pictureResult!!.saveToStorages(this@ImagePickerActivity, fileName, 90)
                    val intent = Intent().apply {
                        putExtra(EXTRA_RESULT_PICTURE, imageFile)
                    }
                    setResult(RESULT_OK, intent)
                } else {
                    val intent = Intent().apply {
                        putExtra(EXTRA_RESULT_PICTURE, imageFile)
                    }
                    setResult(RESULT_OK, intent)
                }
                pictureResult = null
                finish()
            }
        }
    }

    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                pictureResult?.let { bitmap ->
                    binding.imgSelected.setImageBitmap(bitmap)
                }
            }
        }

    private fun observePhotos() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.photos.collect { images ->
                    val photosGallery = mutableListOf<ImagePicker>()
                    photosGallery.add(
                        ImagePicker(
                            id = 90,
                            name = "",
                            width = 0,
                            height = 0,
                            contentUri = Uri.parse(""),
                            takePhoto = R.drawable.ic_take_photo
                        )
                    )
                    photosGallery.addAll(images)
                    galleryAdapter.submitList(photosGallery)
                }
            }
        }
    }

    private fun observeLoading() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.loading.collect { isLoading ->
                    binding.progressBar.isVisible = isLoading
                }
            }
        }
    }

    private fun isStoragePermissionGranted(): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
    }

    private fun initAdapter() {
        binding.rvGallery.apply {
            galleryAdapter = GalleryAdapter()
            adapter = galleryAdapter
            layoutManager = GridLayoutManager(this@ImagePickerActivity, 3)

        }
    }

    private fun getRealPathFromUri(contentUri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(contentUri, proj, null, null, null)
        return cursor.use {
            val columnIndex: Int = it?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA) ?: 0
            it?.moveToFirst()
            it?.getString(columnIndex)
        }
    }

    companion object {
        var pictureResult: Bitmap? = null
        const val EXTRA_RESULT_PICTURE = "resultPicture"
    }
}