package com.rekanesia.image_picker

import android.net.Uri

data class ImagePicker(
    val id: Long,
    val name: String,
    val width: Int,
    val height: Int,
    val contentUri: Uri,
    val takePhoto: Int? = null,
    var isSelected: Boolean = false
)
