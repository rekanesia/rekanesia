package com.rekanesia.image_picker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rekanesia.R
import com.rekanesia.databinding.ItemPhotoBinding

class GalleryAdapter : ListAdapter<ImagePicker, GalleryAdapter.ItemViewHolder>(diffCallback) {

    private var onItemClick: ((ImagePicker, Int) -> Unit)? = null
    fun setOnItemClickListener(onClick: (ImagePicker, Int) -> Unit) {
        onItemClick = { imagePicker, position ->
            onClick.invoke(imagePicker, position)
        }
    }

    private var onTakePhotoClick: (() -> Unit)? = null
    fun setOnTakePhotoListener(onClick: () -> Unit) {
        onTakePhotoClick = { onClick.invoke() }
    }

    var curIndexSelected = -1

    inner class ItemViewHolder(val binding: ItemPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ImagePicker) {
            binding.apply {
                if (item.takePhoto != null) {
                    imgPhoto.setImageDrawable(
                        ContextCompat.getDrawable(
                            binding.imgPhoto.context,
                            item.takePhoto
                        )
                    )
                } else {
                    Glide.with(binding.imgPhoto)
                        .load(item.contentUri)
                        .apply(RequestOptions().override(640, 640))
                        .error(R.color.B400)
                        .into(binding.imgPhoto)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        val binding = holder.binding
        holder.bind(item)
        holder.itemView.setOnClickListener {
            if (position == 0) {
                onTakePhotoClick?.invoke()
            } else {
                onItemClick?.invoke(item, position)
            }
            curIndexSelected = position
            binding.imgSelected.isVisible = item.isSelected
        }
        binding.imgSelected.isVisible = item.isSelected

    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ImagePicker>() {
            override fun areItemsTheSame(oldItem: ImagePicker, newItem: ImagePicker): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ImagePicker, newItem: ImagePicker): Boolean {
                return oldItem == newItem
            }
        }
    }
}