package com.rekanesia

import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.rekanesia.databinding.LayoutInputStepperBinding
import com.rekanesia.helper.GeneralUtils.addDelimiter
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import java.lang.reflect.Field


class NumberPicker : LinearLayout {

    private lateinit var binding: LayoutInputStepperBinding
    private lateinit var listener: NumberPickerListener
    private var disableListener: Boolean = false
    private var styles = Rekan

    private val handler = Handler(Looper.getMainLooper())
    private var delayRunnable: Runnable? = null

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    var maxValue = 0
    var multipleCount = 1
    var qty = 0

    /**
     * Debounce time in milliseconds
     * Default : 0 (realtime when click character on keyboard)
     */
    var debounceTime:Long = 0L


    var stateEnabled: Boolean = true
        set(value) {
            field = value
            setEnabledNumberPicker()
        }

    var isError: Boolean = false
        set(value) {
            field = value
            if (field) {
                setErrorStyle()
            } else {
                setStyles()
            }
        }

    fun interface NumberPickerListener {
        fun count(qty: Int)
    }

    fun setListener(listen: NumberPickerListener) {
        this.listener = listen
    }

    private fun init(attrs: AttributeSet) {
        binding = LayoutInputStepperBinding.inflate(LayoutInflater.from(context), this)
        orientation = HORIZONTAL

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.NumberPicker, 0, 0)
        maxValue = typedArray.getInt(R.styleable.NumberPicker_rekan_maxLength, DEFAULT_MAX_VALUE)
        multipleCount = typedArray.getInt(R.styleable.NumberPicker_rekan_multiple, 1)
        styles = StyleRekanesia.values()[typedArray.getInt(R.styleable.NumberPicker_rekan_styles, 1) - 1]
        isError = typedArray.getBoolean(R.styleable.NumberPicker_rekan_isError, false)
        stateEnabled = typedArray.getBoolean(R.styleable.NumberPicker_rekan_enabled, true)

        setEnabledNumberPicker()

        if (isError) {
            setErrorStyle()
        } else {
            setStyles()
        }


        binding.apply {

            decrease.setOnClickListener {
                subtraction()
            }
            increase.setOnClickListener {
                sum()
            }

            etProductAmount.addTextChangedListener(textWatcher)
        }

        typedArray.recycle()
    }

    private val textWatcher = object : TextWatcher {
        var blockCompletion = false
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (blockCompletion) return
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (disableListener) return
            delayRunnable?.let { handler.removeCallbacks(it) }
            binding.apply {
                p0?.let { s ->
                    if (s.length > 1 && s.first() == '0') {
                        etProductAmount.setText(p0.toString().trimStart('0').addDelimiter())
                        etProductAmount.text?.let { text ->
                            etProductAmount.setSelection(text.length)
                        }
                    }
                    if (s.length == 1 && s.first() == '0') {
                        qty = 0
                        etProductAmount.setSelection(1)
                        listener.count(qty)
                    }
                }
            }
        }

        override fun afterTextChanged(p0: Editable?) {
            if (disableListener) return
            delayRunnable?.let { handler.removeCallbacks(it) }
            delayRunnable = Runnable{
                p0?.let { s ->
                    if (s.isNotEmpty()) {
                        val number = s.toString().removeDelimiter().toInt()
                        if (number in 1..maxValue) {
                            qty = number
                            listener.count(number)
                            if (blockCompletion) {
                                blockCompletion = false
                                return@Runnable
                            }
                            blockCompletion = true
                            val value = qty.toString().addDelimiter()
                            binding.etProductAmount.setText(value)
                            binding.etProductAmount.setSelection(value.length)
                        } else if (number > maxValue) {
                            qty = maxValue
                            val value = qty.toString().addDelimiter()
                            binding.etProductAmount.setText(value)
                            binding.etProductAmount.setSelection(value.length)
                            listener.count(maxValue)
                        }
                    } else {
                        qty = 0
                        binding.etProductAmount.setText("0")
                        listener.count(0)
                    }
                }
            }
            // Jalankan setelah 500ms tanpa perubahan baru
            handler.postDelayed(delayRunnable!!, debounceTime)
        }
    }

    private fun setStyles() {
        binding.apply {
            when (styles) {
                Rekan -> {
                    backgroundNumberPicker.setCardBackgroundColor(ContextCompat.getColor(context, R.color.D100))
                    backgroundNumberPicker.strokeColor = ContextCompat.getColor(context, R.color.D400)
                    decrease.rippleColor = ContextCompat.getColorStateList(context, R.color.B150)
                    etProductAmount.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    etProductAmount.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                    increase.rippleColor = ContextCompat.getColorStateList(context, R.color.B150)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        etProductAmount.setTextCursorDrawable(R.drawable.cursor_color_rekan)
                    } else {
                        try {
                            val field: Field = TextView::class.java.getDeclaredField("mCursorDrawableRes")
                            field.isAccessible = true
                            field.set(etProductAmount, R.drawable.cursor_color_rekan)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    etProductAmount.setOnFocusChangeListener { v, hasFocus ->
                        if (hasFocus) {
                            backgroundNumberPicker.setCardBackgroundColor(ContextCompat.getColor(context, R.color.B100))
                            backgroundNumberPicker.strokeColor = ContextCompat.getColor(context, R.color.B400)
                        } else {
                            backgroundNumberPicker.setCardBackgroundColor(ContextCompat.getColor(context, R.color.D100))
                            backgroundNumberPicker.strokeColor = ContextCompat.getColor(context, R.color.D400)
                        }
                    }
                }
                Ipubers -> {
                    backgroundNumberPicker.setCardBackgroundColor(ContextCompat.getColor(context, R.color.M100))
                    backgroundNumberPicker.strokeColor = ContextCompat.getColor(context, R.color.M400)
                    decrease.rippleColor = ContextCompat.getColorStateList(context, R.color.L150)
                    etProductAmount.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    etProductAmount.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        etProductAmount.setTextCursorDrawable(R.drawable.cursor_color_ipubers)
                    } else {
                        try {
                            val field: Field = TextView::class.java.getDeclaredField("mCursorDrawableRes")
                            field.isAccessible = true
                            field.set(etProductAmount, R.drawable.cursor_color_ipubers)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    increase.rippleColor = ContextCompat.getColorStateList(context, R.color.L150)
                    etProductAmount.setOnFocusChangeListener { v, hasFocus ->
                        if (hasFocus) {
                            backgroundNumberPicker.setCardBackgroundColor(ContextCompat.getColor(context, R.color.L100))
                            backgroundNumberPicker.strokeColor = ContextCompat.getColor(context, R.color.L400)
                        } else {
                            backgroundNumberPicker.setCardBackgroundColor(ContextCompat.getColor(context, R.color.D100))
                            backgroundNumberPicker.strokeColor = ContextCompat.getColor(context, R.color.D400)
                        }
                    }
                }
            }
        }
    }

    private fun setErrorStyle() {
        binding.apply {
            backgroundNumberPicker.setCardBackgroundColor(ContextCompat.getColor(context, R.color.R100))
            backgroundNumberPicker.strokeColor = ContextCompat.getColor(context, R.color.R400)
        }
    }

    private fun setEnabledNumberPicker() {
        binding.apply {
            if (stateEnabled) {
                backgroundNumberPicker.alpha = 1f
                imgPlus.alpha = 1F
                imgMinus.alpha = 1F
            } else {
                backgroundNumberPicker.alpha = .7f
                imgPlus.alpha = .7F
                imgMinus.alpha = .7F
            }
            increase.isEnabled = stateEnabled
            decrease.isEnabled = stateEnabled
            etProductAmount.isEnabled = stateEnabled
            etProductAmount.isFocusable = stateEnabled
            etProductAmount.isFocusableInTouchMode = stateEnabled
        }
    }

    private fun subtraction() {
        disableListener = true
        if (qty != 0) qty -= multipleCount
        if (qty < 0) qty = 0
        val value = qty.toString().addDelimiter()
        binding.etProductAmount.setText(value)
        binding.etProductAmount.setSelection(value.length)
        listener.count(qty)
        disableListener = false
    }

    private fun sum() {
        disableListener = true
        qty += multipleCount
        if (qty > maxValue) qty = maxValue
        val value = qty.toString().addDelimiter()
        binding.etProductAmount.setText(value)
        binding.etProductAmount.setSelection(value.length)
        listener.count(qty)
        disableListener = false
    }

    fun setCount(size: Int) {
        disableListener = true
        this.qty = size
        try {
            binding.etProductAmount.setText(size.toString().addDelimiter())
        } catch (e: Exception) {
            throw RuntimeException("listener must be init first before another function", e)
        } finally {
            disableListener = false
        }
    }

    companion object {
        private const val DEFAULT_MAX_VALUE = 999999
    }
}