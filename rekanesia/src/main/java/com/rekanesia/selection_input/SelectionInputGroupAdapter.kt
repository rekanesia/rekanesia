package com.rekanesia.selection_input

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rekanesia.R
import com.rekanesia.databinding.ItemSelectionInputBinding
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.Ipubers
import com.rekanesia.helper.StyleRekanesia.Rekan

class SelectionInputGroupAdapter: ListAdapter<SelectionInputModel, SelectionInputGroupAdapter.ItemViewHolder>(diffCallback) {

    var style = Rekan
    var isClickable = true

    private var onSelectionChanged: ((SelectionInputModel) -> Unit)? = null
    fun setOnSelectionChanged(onSelectionChanged: (SelectionInputModel) -> Unit) {
        this.onSelectionChanged = onSelectionChanged
    }

    inner class ItemViewHolder(private val binding: ItemSelectionInputBinding) : ViewHolder(binding.root) {
        private val childAdapter: SelectionInputGroupChildAdapter by lazy { SelectionInputGroupChildAdapter(style) }
        private val childTextFieldAdapter: SelectionInputGroupChildTextFieldAdapter by lazy { SelectionInputGroupChildTextFieldAdapter(style) }
        fun bind(item: SelectionInputModel) {
            binding.apply {

                setupChildAdapter(item)

                tileTitle.label = item.titleStart
                tileTitle.value = item.titleEnd
                tileBody.label = item.bodyStart
                tileBody.value = item.bodyEnd

                tileTitle.setLabelIcon(item.iconTitleStart, null)

                if (item.isSelected) {
                    rvSelectionInputChild.isVisible = item.childs.isNotEmpty()
                    rvSelectionInputTextFieldChild.isVisible = item.childsTextField.isNotEmpty()
                    if (item.style.showBody) {
                        tileBody.isVisible = true
                    } else {
                        tileBody.isVisible = !item.bodyStart.isNullOrEmpty()
                    }
                    imgCheck.visibility = View.VISIBLE
                } else {
                    rvSelectionInputChild.isVisible = false
                    rvSelectionInputTextFieldChild.isVisible = false
                    if (item.style.showBody) {
                        tileBody.isVisible = true
                    } else {
                        tileBody.isVisible = false
                    }
                    imgCheck.visibility = View.INVISIBLE
                }

                if (adapterPosition == currentList.size - 1) {
                    divider.visibility = View.INVISIBLE
                } else {
                    divider.visibility = View.VISIBLE
                }

                root.setOnClickListener {
                    if (!isClickable) return@setOnClickListener
                    if (!item.isSelected) {
                        val items = currentList
                        items.forEach { it.isSelected = false }
                        notifyItemRangeChanged(0 , items.size)
                        item.isSelected = true
                        notifyItemChanged(adapterPosition)
                        onSelectionChanged?.invoke(item)
                    }
                }

                childAdapter.setOnItemClickListener { child ->
                    val itemSelected = item.copy(childs = listOf(child))
                    onSelectionChanged?.invoke(itemSelected)
                }

                childTextFieldAdapter.setOnInputTextFieldChange { child, text ->
                    val itemSelected = item.copy(childsTextField = listOf(child))
                    onSelectionChanged?.invoke(itemSelected)
                }

                childTextFieldAdapter.setOnChooseClick { child ->
                    val itemSelected = item.copy(childsTextField = listOf(child))
                    onSelectionChanged?.invoke(itemSelected)
                }

                childTextFieldAdapter.setOnTextAreaChange { child, text ->
                    val itemSelected = item.copy(childsTextField = listOf(child))
                    onSelectionChanged?.invoke(itemSelected)
                }

                setStyle(item)
            }
        }

        private fun setupChildAdapter(item: SelectionInputModel) {
            binding.apply {
                val context = root.context
                if (item.childs.isEmpty() && item.childsTextField.isEmpty()) return
                if (item.childs.isNotEmpty()) {
                    rvSelectionInputChild.adapter = childAdapter
                    rvSelectionInputChild.layoutManager = LinearLayoutManager(context)
                    childAdapter.submitList(item.childs)
                }
                if (item.childsTextField.isNotEmpty()) {
                    rvSelectionInputTextFieldChild.adapter = childTextFieldAdapter
                    rvSelectionInputTextFieldChild.layoutManager = LinearLayoutManager(context)
                    childTextFieldAdapter.submitList(item.childsTextField)
                }
            }
        }

        private fun setStyle(item: SelectionInputModel) {
            binding.apply {
                val context = root.context
                val styleDataTile = item.style
                tileTitle.setLabelFontWeight(styleDataTile.titleStartFontWeight)
                tileTitle.setValueFontWeight(styleDataTile.titleEndFontWeight)

                tileBody.setLabelFontWeight(styleDataTile.bodyStartFontWeight)
                tileBody.setValueFontWeight(styleDataTile.bodyEndFontWeight)
                when (style) {
                    Rekan -> {
                        tileTitle.setFont(R.font.noto_sans_medium)
                        tileBody.setFont(R.font.noto_sans_regular)

                        tileBody.setLabelTextColor(R.color.B500)
                        tileBody.setValueTextColor(R.color.B500)

                        rvSelectionInputChild.setBackgroundResource(R.drawable.bg_select)

                        if (item.isSelected) {
                            tileTitle.setFont(R.font.noto_sans_bold)

                            tileTitle.setLabelTextColor(R.color.B500)
                            tileTitle.setValueTextColor(R.color.B500)

                            if (styleDataTile.addBackground) {
                                when (adapterPosition) {
                                    0 -> root.setBackgroundResource(R.drawable.bg_selected_top)
                                    currentList.size - 1 -> root.setBackgroundResource(R.drawable.bg_selected_bot)
                                    else -> root.setBackgroundResource(R.drawable.bg_selected)
                                }
                            } else {
                                root.setBackgroundResource(R.color.B100)
                            }
                        } else {
                            tileTitle.setFont(R.font.noto_sans_medium)

                            tileTitle.setLabelTextColor(R.color.D800)
                            tileTitle.setValueTextColor(R.color.D800)

                            root.setBackgroundResource(android.R.color.transparent)
                        }

                        divider.setBackgroundColor(ContextCompat.getColor(context, R.color.D400))

                        imgCheck.imageTintList = ContextCompat.getColorStateList(context, R.color.B500)
                    }
                    Ipubers -> {
                        tileTitle.setFont(R.font.harmony_medium)
                        tileBody.setFont(R.font.harmony_regular)

                        tileBody.setLabelTextColor(R.color.L500)
                        tileBody.setValueTextColor(R.color.L500)

                        rvSelectionInputChild.setBackgroundResource(R.drawable.bg_select_psp)

                        divider.setBackgroundColor(ContextCompat.getColor(context, R.color.M400))

                        if (item.isSelected) {
                            tileTitle.setFont(R.font.noto_sans_bold)

                            tileTitle.setLabelTextColor(R.color.L500)
                            tileTitle.setValueTextColor(R.color.L500)

                            if (styleDataTile.addBackground) {
                                when (adapterPosition) {
                                    0 -> root.setBackgroundResource(R.drawable.bg_selected_top_psp)
                                    currentList.size - 1 -> root.setBackgroundResource(R.drawable.bg_selected_bot_psp)
                                    else -> root.setBackgroundResource(R.drawable.bg_selected_psp)
                                }
                            } else {
                                root.setBackgroundResource(R.color.L100)
                            }
                        } else {
                            tileTitle.setFont(R.font.noto_sans_medium)

                            tileTitle.setLabelTextColor(R.color.M800)
                            tileTitle.setValueTextColor(R.color.M800)

                            root.setBackgroundResource(android.R.color.transparent)
                        }

                        imgCheck.imageTintList = ContextCompat.getColorStateList(context, R.color.L500)
                    }
                }

                if (styleDataTile.titleStartColor != null) {
                    tileTitle.setLabelTextColor(styleDataTile.titleStartColor!!)
                }

                if (styleDataTile.titleEndColor != null) {
                    tileTitle.setValueTextColor(styleDataTile.titleEndColor!!)
                }

                if (styleDataTile.bodyStartColor != null) {
                    tileBody.setLabelTextColor(styleDataTile.bodyStartColor!!)
                }

                if (styleDataTile.bodyEndColor != null) {
                    tileBody.setValueTextColor(styleDataTile.bodyEndColor!!)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(ItemSelectionInputBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<SelectionInputModel>() {
            override fun areItemsTheSame(
                oldItem: SelectionInputModel,
                newItem: SelectionInputModel,
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: SelectionInputModel,
                newItem: SelectionInputModel,
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

}