package com.rekanesia.selection_input

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rekanesia.databinding.ItemSelectionInputTextFieldChildBinding
import com.rekanesia.helper.GeneralUtils
import com.rekanesia.helper.GeneralUtils.addDelimiter
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import com.rekanesia.selection_input.TextFieldType.*

class SelectionInputGroupChildTextFieldAdapter(
    private val style: StyleRekanesia
) : ListAdapter<SelectionInputChildTextFieldModel, SelectionInputGroupChildTextFieldAdapter.ItemViewHolder>(diffCallback) {

    private var onChooseClick: ((SelectionInputChildTextFieldModel) -> Unit)? = null
    fun setOnChooseClick(listener: (SelectionInputChildTextFieldModel) -> Unit) {
        onChooseClick = listener
    }

    private var onInputTextFieldChange: ((SelectionInputChildTextFieldModel, String) -> Unit)? = null
    fun setOnInputTextFieldChange(listener: (SelectionInputChildTextFieldModel, String) -> Unit) {
        onInputTextFieldChange = listener
    }

    private var onTextAreaChange: ((SelectionInputChildTextFieldModel, String) -> Unit)? = null
    fun setOnTextAreaChange(listener: (SelectionInputChildTextFieldModel, String) -> Unit) {
        onTextAreaChange = listener
    }

    inner class ItemViewHolder(private val binding: ItemSelectionInputTextFieldChildBinding): ViewHolder(binding.root) {
        fun bind(item: SelectionInputChildTextFieldModel) {
            binding.apply {
                when (item.textFieldType) {
                    Input -> {
                        val prefix = item.prefix.orEmpty()
                        val suffix = item.suffix.orEmpty()
                        TextFieldHelper.setInputType(tfInput.input, item.inputType)
                        tfInput.style = style
                        tfInput.isVisible = true
                        tfInputArea.isVisible = false
                        tfChoose.isVisible = false
                        tfInput.input.hint = item.hint
                        tfInput.prefix = prefix
                        tfInput.suffix = suffix

                        var isBlockListener = false
                        if (!item.text.isNullOrEmpty()) {
                            if (item.inputType == InputTypeRekanesia.Currency) {
                                tfInput.input.setText("${prefix}${(item.text ?: "").addDelimiter()}${suffix}")
                            } else {
                                tfInput.input.setText("${prefix}${item.text ?: ""}${suffix}")
                            }
                        }
                        tfInput.input.addTextChangedListener { text ->
                            val newText = text.toString().removeDelimiter()
                            val updatedItem = item.copy(text = newText)
                            if (isBlockListener) {
                                isBlockListener = false
                                return@addTextChangedListener
                            }
                            isBlockListener = true
                            if (text.toString().length < prefix.length) {
                                tfInput.input.setText(prefix)
                                tfInput.input.setSelection(prefix.length)
                                onInputTextFieldChange?.invoke(updatedItem, newText)
                                return@addTextChangedListener
                            }
                            val value = if (item.inputType == InputTypeRekanesia.Currency) {
                                prefix+newText.addDelimiter()+suffix
                            } else {
                                prefix+GeneralUtils.clearTextValue(text.toString(), prefix, suffix)+suffix
                            }
                            tfInput.input.setText(value)
                            tfInput.input.setSelection(value.length - suffix.length)
                            onInputTextFieldChange?.invoke(updatedItem, newText)
                        }
                    }
                    TextArea -> {
                        tfInputArea.style = style
                        tfInput.isVisible = false
                        tfInputArea.isVisible = true
                        tfChoose.isVisible = false
                        tfInputArea.input.hint = item.hint
                        tfInputArea.input.doOnTextChanged { text, _, _, _ ->
                            val updatedItem = item.copy(text = text.toString())
                            onTextAreaChange?.invoke(updatedItem, text.toString())
                        }
                    }
                    Choose -> {
                        tfChoose.style = style
                        tfInput.isVisible = false
                        tfInputArea.isVisible = false
                        tfChoose.isVisible = true
                        tfChoose.text = item.hint
                        tfChoose.setIconEnd(item.iconEnd)
                        tfChoose.setIconStart(item.iconStart)
                        tfChoose.setOnClickListener {
                            onChooseClick?.invoke(item)
                        }
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(ItemSelectionInputTextFieldChildBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<SelectionInputChildTextFieldModel>() {
            override fun areItemsTheSame(
                oldItem: SelectionInputChildTextFieldModel,
                newItem: SelectionInputChildTextFieldModel,
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: SelectionInputChildTextFieldModel,
                newItem: SelectionInputChildTextFieldModel,
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}