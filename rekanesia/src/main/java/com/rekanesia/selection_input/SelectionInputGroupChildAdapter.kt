package com.rekanesia.selection_input

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rekanesia.R
import com.rekanesia.databinding.ItemSelectionInputChildBinding
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*

class SelectionInputGroupChildAdapter(
    private val style: StyleRekanesia
): ListAdapter<SelectionInputChildModel, SelectionInputGroupChildAdapter.ItemViewHolder>(diffCallback) {

    private var onItemClickListener: ((SelectionInputChildModel) -> Unit)? = null
    fun setOnItemClickListener(listener: (SelectionInputChildModel) -> Unit) {
        onItemClickListener = listener
    }

    inner class ItemViewHolder(private val binding: ItemSelectionInputChildBinding): ViewHolder(binding.root) {
        fun bind(item: SelectionInputChildModel) {
            binding.apply {

                setStyle()

                tileTitle.label = item.titleStart
                tileTitle.value = item.titleEnd
                tileBody.label = item.bodyStart
                tileBody.value = item.bodyEnd
                tileTitle.setLabelIcon(item.iconTitleStart, null)
                tileTitle.setValueIcon(null, item.iconTitleEnd)
                tileBody.setLabelIcon(item.iconBodyStart, null)
                tileBody.setValueIcon(null, item.iconBodyEnd)

                tileBody.isVisible = !(item.bodyStart.isNullOrEmpty() || item.bodyEnd.isNullOrEmpty())
                if (adapterPosition == currentList.size - 1) {
                    divider.visibility = View.INVISIBLE
                } else {
                    divider.visibility = View.VISIBLE
                }

                if (item.isSelected) {
                    imgCheck.visibility = View.VISIBLE
                } else {
                    imgCheck.visibility = View.INVISIBLE
                }

                root.setOnClickListener {
                    if (!item.isSelected) {
                        val items = currentList.toMutableList()
                        items.forEach { it.isSelected = false }
                        notifyItemRangeChanged(0, items.size)
                        item.isSelected = !item.isSelected
                        notifyItemChanged(adapterPosition)
                        onItemClickListener?.invoke(item)

                    }
                    onItemClickListener?.invoke(item)
                }
            }
        }

        private fun setStyle() {
            binding.apply {
                val context = root.context
                when (style) {
                    Rekan -> {
                        tileTitle.setFont(R.font.noto_sans_semi_bold)
                        tileBody.setFont(R.font.noto_sans_regular)

                        tileTitle.setLabelTextColor(R.color.D800)
                        tileTitle.setValueTextColor(R.color.B500)
                        tileBody.setValueTextColor(R.color.B500)
                        tileBody.setLabelTextColor(R.color.B500)

                        imgCheck.imageTintList = ContextCompat.getColorStateList(context, R.color.B500)

                        divider.setBackgroundColor(ContextCompat.getColor(context, R.color.D800))
                    }
                    Ipubers -> {
                        tileTitle.setFont(R.font.harmony_medium)
                        tileBody.setFont(R.font.harmony_regular)

                        tileTitle.setLabelTextColor(R.color.M800)
                        tileTitle.setValueTextColor(R.color.L500)
                        tileBody.setValueTextColor(R.color.L500)
                        tileBody.setLabelTextColor(R.color.L500)

                        imgCheck.imageTintList = ContextCompat.getColorStateList(context, R.color.L500)

                        divider.setBackgroundColor(ContextCompat.getColor(context, R.color.M800))
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(ItemSelectionInputChildBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<SelectionInputChildModel>() {
            override fun areItemsTheSame(
                oldItem: SelectionInputChildModel,
                newItem: SelectionInputChildModel,
            ): Boolean {
                return oldItem.id == newItem.id && oldItem.isSelected == newItem.isSelected
            }

            override fun areContentsTheSame(
                oldItem: SelectionInputChildModel,
                newItem: SelectionInputChildModel,
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}