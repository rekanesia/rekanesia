package com.rekanesia.selection_input

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.rekanesia.R
import com.rekanesia.databinding.LayoutSelectionInputGroupBinding
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*

class SelectionInputGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var binding = LayoutSelectionInputGroupBinding.inflate(LayoutInflater.from(context), this)
    val adapter: SelectionInputGroupAdapter by lazy { SelectionInputGroupAdapter() }

    private var style = Rekan
        set(value) {
            field = value
            adapter.style = field
        }

    private var addBackground: Boolean = false

    var isEnabledClick: Boolean = true
        set(value) {
            field = value
            adapter.isClickable = field
        }

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.tvMandatory.isVisible = field
        }

    fun setBackgroundSiGroup(@DrawableRes background: Int) {
        binding.root.setBackgroundResource(background)
    }

    fun setOnSelectionChanged(listener: (SelectionInputModel) -> Unit) {
        adapter.setOnSelectionChanged { item ->
            listener.invoke(item)
        }
    }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.SelectionInputGroup, defStyle, 0)
        val label = typedArray.getString(R.styleable.SelectionInputGroup_rekan_label).orEmpty()
        isMandatory = typedArray.getBoolean(R.styleable.SelectionInputGroup_rekan_mandatory, false)
        isEnabledClick = typedArray.getBoolean(R.styleable.SelectionInputGroup_rekan_enabled_click, true)
        style = StyleRekanesia.values()[typedArray.getInt(R.styleable.SelectionInputGroup_rekan_styles, 1) - 1]
        addBackground = typedArray.getBoolean(R.styleable.SelectionInputGroup_rekan_addBackground, false)

        setLabelAndMandatory(label)

        setupAdapter()

        setStyle()

        typedArray.recycle()
    }

    private fun setLabelAndMandatory(label: String) {
        binding.tvLabel.text = label
        binding.tvLabel.isVisible = label.isNotEmpty()

        binding.tvMandatory.isVisible = isMandatory
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    tvMandatory.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    tvMandatory.setTextColor(ContextCompat.getColor(context, R.color.R500))

                    if (addBackground) {
                        rvSelectionInput.setBackgroundResource(R.drawable.bg_select)
                        return@apply
                    }
                }
                Ipubers -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    tvMandatory.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    tvMandatory.setTextColor(ContextCompat.getColor(context, R.color.R500))

                    if (addBackground) {
                        rvSelectionInput.setBackgroundResource(R.drawable.bg_select_psp)
                        return
                    }

                }
            }
        }
    }

    private fun setupAdapter() {
        binding.rvSelectionInput.adapter = adapter
        binding.rvSelectionInput.layoutManager = LinearLayoutManager(context)
        adapter.isClickable = isEnabledClick
    }

    fun setItems(items: List<SelectionInputModel>) {
        val copiedItems = items.map { it.copy(style = it.style.copy(addBackground = addBackground)) }
        adapter.submitList(copiedItems)
    }

    fun addItem(item: SelectionInputModel) {
        val items = adapter.currentList.toMutableList()
        items.add(item)
        adapter.submitList(items)
    }
}