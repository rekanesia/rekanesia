package com.rekanesia.selection_input

import androidx.annotation.DrawableRes
import com.rekanesia.FontWeightDataTile
import com.rekanesia.R
import com.rekanesia.helper.InputTypeRekanesia

data class SelectionInputModel(
    var id: Int,
    var titleStart: String,
    var titleEnd: String? = null,
    var bodyStart: String? = null,
    var bodyEnd: String? = null,
    @DrawableRes var iconTitleStart: Int? = null,
    @DrawableRes var iconTitleEnd: Int? = null,
    @DrawableRes var iconBodyStart: Int? = null,
    @DrawableRes var iconBodyEnd: Int? = null,
    var isSelected: Boolean = false,
    var childs: List<SelectionInputChildModel> = emptyList(),
    var childsTextField: List<SelectionInputChildTextFieldModel> = emptyList(),
    var providedId: String? = null,
    var style: SelectionInputStyle = SelectionInputStyle()
)

data class SelectionInputChildModel(
    var id: Int,
    var titleStart: String,
    var titleEnd: String? = null,
    var bodyStart: String? = null,
    var bodyEnd: String? = null,
    @DrawableRes val iconTitleStart: Int? = null,
    @DrawableRes val iconTitleEnd: Int? = null,
    @DrawableRes val iconBodyStart: Int? = null,
    @DrawableRes val iconBodyEnd: Int? = null,
    var isSelected: Boolean = false,
    var providedId: String? = null,
    var style: SelectionInputStyle = SelectionInputStyle()
)

data class SelectionInputChildTextFieldModel(
    var id: Int,
    var hint: String = "",
    var text: String? = null,
    var prefix: String? = null,
    var suffix: String? = null,
    @DrawableRes var iconStart: Int? = null,
    @DrawableRes var iconEnd: Int? = null,
    var inputType: InputTypeRekanesia = InputTypeRekanesia.Text,
    var textFieldType: TextFieldType = TextFieldType.Input,
    var providedId: String? = null
)

enum class TextFieldType {
    Input, TextArea, Choose
}

data class SelectionInputStyle(
    var titleStartFontWeight: FontWeightDataTile = FontWeightDataTile.Medium,
    var titleEndFontWeight: FontWeightDataTile = FontWeightDataTile.Medium,
    var bodyStartFontWeight: FontWeightDataTile = FontWeightDataTile.Regular,
    var bodyEndFontWeight: FontWeightDataTile = FontWeightDataTile.Regular,
    var titleStartColor: Int? = null,
    var titleEndColor: Int? = null,
    var bodyStartColor: Int? = null,
    var bodyEndColor: Int? = null,
    var showBody: Boolean = false,
    var addBackground: Boolean = false
)