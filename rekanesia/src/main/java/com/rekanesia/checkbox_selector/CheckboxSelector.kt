package com.rekanesia.checkbox_selector

import java.util.UUID

data class CheckboxSelector(
    var id: String = UUID.randomUUID().toString(),
    var customId: String? = null,
    var text: String,
    var checked: Boolean = false,
    val requiredTextField: Boolean = false,
    var textTextField: String = "",
    var prefix: String = "",
    var suffix: String = "",
)
