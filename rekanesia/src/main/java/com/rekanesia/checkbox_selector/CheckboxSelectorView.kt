package com.rekanesia.checkbox_selector

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.rekanesia.R
import com.rekanesia.databinding.LayoutCheckboxSelectorBinding
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia

class CheckboxSelectorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var binding = LayoutCheckboxSelectorBinding.inflate(LayoutInflater.from(context), this)
    lateinit var adapterCheckboxSelector: CheckboxSelectorAdapter

    private var style = StyleRekanesia.Rekan

    var isEnabledClick: Boolean = true
        set(value) {
            field = value
            if (::adapterCheckboxSelector.isInitialized) {
                adapterCheckboxSelector.isClickable = field
            }
            binding.bgDisabled.isVisible = !isEnabledClick
        }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CheckboxSelectorView, defStyle, defStyle)
        style = StyleRekanesia.entries.toTypedArray()[typedArray.getInt(R.styleable.CheckboxSelectorView_rekan_styles, 1) - 1]
        val isMultipleSelector = typedArray.getBoolean(R.styleable.CheckboxSelectorView_rekan_isMultipleChoice, false)
        isEnabledClick = typedArray.getBoolean(R.styleable.CheckboxSelectorView_rekan_enabled, true)
        val inputType = InputTypeRekanesia.entries[typedArray.getInt(
            R.styleable.CheckboxSelectorView_rekan_setInputType,
            InputTypeRekanesia.Currency.ordinal
        ) - 1]

        binding.bgDisabled.isVisible = !isEnabledClick

        setStyle()

        setupAdapter(isMultipleSelector, inputType)

        typedArray.recycle()
    }

    private fun setupAdapter(isMultipleSelector: Boolean, inputType: InputTypeRekanesia) {
        adapterCheckboxSelector = CheckboxSelectorAdapter(inputType, isMultipleSelector)
        adapterCheckboxSelector.style = style
        adapterCheckboxSelector.isClickable = isEnabledClick
        binding.rvCheckboxSelector.adapter = adapterCheckboxSelector
        binding.rvCheckboxSelector.layoutManager = LinearLayoutManager(context)
    }

    private fun setStyle() {
        when (style) {
            StyleRekanesia.Rekan -> {
                binding.rvCheckboxSelector.setBackgroundResource(R.drawable.bg_rounded_8_outlined_d400)
                binding.bgDisabled.setBackgroundResource(R.drawable.bg_d300_rounded_8_outlined_d400)
            }
            StyleRekanesia.Ipubers -> {
                binding.rvCheckboxSelector.setBackgroundResource(R.drawable.bg_rounded_8_outlined_m400)
                binding.bgDisabled.setBackgroundResource(R.drawable.bg_m300_rounded_8_outlined_m400)
            }
        }
    }

    fun setOnSingleCheckedListener(listener: (CheckboxSelector) -> Unit) {
        adapterCheckboxSelector.setOnSingleCheckedListener { listener.invoke(it) }
    }

    fun setOnMultipleCheckedListener(listener: (List<CheckboxSelector>) -> Unit) {
        adapterCheckboxSelector.setOnMultipleCheckedListener { listener.invoke(it) }
    }

    fun setOnTextFieldChangeListener(listener: (String) -> Unit) {
        adapterCheckboxSelector.setOnTextFieldChange { listener.invoke(it) }
    }

    fun submitList(list: List<CheckboxSelector>) {
        adapterCheckboxSelector.submitList(list)
    }

    fun getCheckedItems(): List<CheckboxSelector> {
        val items = adapterCheckboxSelector.currentList
        return items.filter { it.checked }
    }
}