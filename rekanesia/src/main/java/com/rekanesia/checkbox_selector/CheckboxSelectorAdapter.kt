package com.rekanesia.checkbox_selector

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.rekanesia.R
import com.rekanesia.databinding.ItemCheckboxSelectorBinding
import com.rekanesia.helper.GeneralUtils.showKeyboardAndFocus
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import com.rekanesia.helper.TextFieldHelper.showKeyboard

class CheckboxSelectorAdapter(
    private val inputType: InputTypeRekanesia,
    private val multipleSelection: Boolean = true,
) : ListAdapter<CheckboxSelector, CheckboxSelectorAdapter.ItemViewHolder>(diffCallback) {

    var style: StyleRekanesia = StyleRekanesia.Rekan
    var isClickable = true

    private var onSingleCheckedListener: ((CheckboxSelector) -> Unit)? = null
    fun setOnSingleCheckedListener(listener: (CheckboxSelector) -> Unit) {
        onSingleCheckedListener = listener
    }

    private var onMultipleCheckedListener: ((List<CheckboxSelector>) -> Unit)? = null
    fun setOnMultipleCheckedListener(listener: (List<CheckboxSelector>) -> Unit) {
        onMultipleCheckedListener = listener
    }

    private var onTextFieldChange: ((String) -> Unit)? = null
    fun setOnTextFieldChange(listener: (String) -> Unit) {
        onTextFieldChange = listener
    }

    inner class ItemViewHolder(private val binding: ItemCheckboxSelectorBinding) : ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: CheckboxSelector) {
            binding.apply {

                setStyle()

                handlingTextField(item)

                checkBox.text = item.text
                checkBox.isChecked = item.checked

                line.isVisible = adapterPosition != currentList.size - 1

                checkBox.isEnabled = isClickable

                checkBox.setOnClickListener {
                    if (!isClickable) return@setOnClickListener
                    if (multipleSelection) {
                        val items = currentList
                        item.checked = !item.checked
                        notifyDataSetChanged()

                        val checkedItems = items.filter { it.checked }
                        onMultipleCheckedListener?.invoke(checkedItems)
                        onSingleCheckedListener?.invoke(item)
                        return@setOnClickListener
                    }

                    if (item.checked) {
                        checkBox.isChecked = item.checked
                        item.checked = item.checked
                        notifyDataSetChanged()
                        onSingleCheckedListener?.invoke(item)
                        return@setOnClickListener
                    }

                    val items = currentList
                    items.forEach { it.checked = false }
                    item.checked = true
                    notifyDataSetChanged()
                    onSingleCheckedListener?.invoke(item)
                }
            }
        }

        @SuppressLint("SetTextI18n")
        private fun handlingTextField(item: CheckboxSelector) {
            binding.apply {
                TextFieldHelper.setInputType(tfCheckbox.input, inputType)
                tfCheckbox.input.imeOptions = EditorInfo.IME_ACTION_DONE
                tfCheckbox.prefix = item.prefix
                tfCheckbox.suffix = item.suffix
                tfCheckbox.isVisible = item.requiredTextField && item.checked
                tfCheckbox.text = item.textTextField

                if (item.checked) {
                    if (item.requiredTextField) {
                        tfCheckbox.input.requestFocus()
                        tfCheckbox.input.doOnTextChanged { text, _, _, _ ->
                            if ((item.prefix.isNotEmpty() || item.suffix.isNotEmpty()) && inputType == InputTypeRekanesia.Currency) {
                                onTextFieldChange?.invoke(text.toString().removeDelimiter())
                            } else {
                                onTextFieldChange?.invoke(text.toString())
                            }
                            if (item.prefix.isNotEmpty() && (inputType == InputTypeRekanesia.Currency || inputType == InputTypeRekanesia.Number)) {
                                tfCheckbox.input.setText(item.prefix)
                            }
                            if (item.suffix.isNotEmpty() && (inputType == InputTypeRekanesia.Currency || inputType == InputTypeRekanesia.Number)) {
                                tfCheckbox.input.setText("0" + item.suffix)
                            } else if (item.suffix.isNotEmpty()) {
                                tfCheckbox.input.setText(item.suffix)
                            }
                            item.textTextField = text.toString()
                        }
                    }
                }
            }
        }

        private fun setStyle() {
            binding.apply {
                val context = itemView.context
                tfCheckbox.style = style
                when (style) {
                    StyleRekanesia.Rekan -> {
                        checkBox.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                        checkBox.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    }
                    StyleRekanesia.Ipubers -> {
                        checkBox.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                        checkBox.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = ItemCheckboxSelectorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<CheckboxSelector>() {
            override fun areItemsTheSame(
                oldItem: CheckboxSelector,
                newItem: CheckboxSelector
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: CheckboxSelector,
                newItem: CheckboxSelector
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}