package com.rekanesia

import android.content.Context
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.rekanesia.databinding.LayoutListTileBinding
import com.rekanesia.helper.StyleRekanesia.Ipubers
import com.rekanesia.helper.StyleRekanesia.Rekan
import com.rekanesia.helper.StyleRekanesia.values

class ListTile(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    private var binding: LayoutListTileBinding

    private var style = Rekan
    private var fontWeight = 2

    var label: String? = null
        set(value) {
            field = value
            binding.textLabel.text = field
        }

    var showDivier = true
        set(value) {
            field = value
            binding.divider.visibility = if (!value) View.GONE else View.VISIBLE
        }

    fun setOnClickListener(onClick: (() -> Unit)) {
        binding.layoutMain.setOnClickListener { onClick.invoke() }
    }

    fun setIconStart(@DrawableRes iconStart: Int?) {
        if (iconStart != null) {
            binding.imageSettingIcon.setImageResource(iconStart)
        }
        binding.imageSettingIcon.isVisible = iconStart != null
    }

    fun setIconEnd(@DrawableRes iconEnd: Int) {
        binding.imgEnd.setImageResource(iconEnd)
    }

    fun setSpannableText(text: SpannableStringBuilder?) {
        val mText = text
        if (mText.isNullOrEmpty() || mText.toString() == "null") {
            binding.textLabel.text = ""
        } else {
            binding.textLabel.text = mText
        }
    }

    fun setTextColor(color: Int) {
        binding.textLabel.setTextColor(color)
    }

    init {
        binding = LayoutListTileBinding.inflate(LayoutInflater.from(context), this)

        orientation = VERTICAL
        gravity = Gravity.CENTER

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ListTile, 0, 0)
        label = typedArray.getString(R.styleable.ListTile_rekan_label)
        showDivier = typedArray.getBoolean(R.styleable.ListTile_rekan_showDivider, true)
        fontWeight = typedArray.getInt(R.styleable.ListTile_rekan_customFontWeight, 2)
        style = values()[typedArray.getInt(R.styleable.ListTile_rekan_styles, 1) - 1]
        val textColor = typedArray.getColor(
            R.styleable.ListTile_rekan_textColor,
            when (style) {
                Rekan -> ContextCompat.getColor(context, R.color.D800)
                Ipubers -> ContextCompat.getColor(context, R.color.M800)
            }
        )
        val iconStart = typedArray.getResourceId(R.styleable.ListTile_rekan_icon, -1)
        val iconEnd = typedArray.getResourceId(R.styleable.ListTile_rekan_iconEnd, R.drawable.ic_chevron_right)

        binding.textLabel.text = label
        if (!showDivier) {
            binding.divider.visibility = View.GONE
        }

        setStyle()

        setTextColor(textColor)

        setIcon(iconStart, iconEnd)

        typedArray.recycle()
    }

    private fun setIcon(iconStart: Int, iconEnd: Int) {
        binding.imageSettingIcon.isVisible = iconStart != -1
        if (iconStart != -1) {
            binding.imageSettingIcon.setImageDrawable(ContextCompat.getDrawable(context, iconStart))
        }

        binding.imgEnd.setImageDrawable(ContextCompat.getDrawable(context, iconEnd))
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    when (fontWeight) {
                        1 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_light)
                        2 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                        3 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                        4 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                        5 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                        6 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_black)
                    }
                    divider.setBackgroundResource(R.color.D450)
                }
                Ipubers -> {
                    when (fontWeight) {
                        1 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                        2 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                        3 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                        4 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                        5 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                        6 -> textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_black)
                    }
                    divider.setBackgroundResource(R.color.M400)
                }
            }
        }
    }
}