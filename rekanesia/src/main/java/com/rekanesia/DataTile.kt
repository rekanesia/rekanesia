package com.rekanesia

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.rekanesia.FontWeightDataTile.*
import com.rekanesia.databinding.LayoutDataTileBinding

class DataTile(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    private var binding: LayoutDataTileBinding

    private var fontFamily: Int = 1

    var label: String? = null
        set(value) {
            field = value
            binding.textLabel.text = field
            binding.textLabel.isVisible = !field.isNullOrEmpty()
        }

    var value: String? = null
        set(value) {
            field = value
            binding.textValue.text = field
            binding.textValue.isVisible = !field.isNullOrEmpty()
        }

    var maxLines: Int = 3
        set(value) {
            field = value
            binding.textLabel.maxLines = field
            binding.textValue.maxLines = field
        }

    var isLabelClickable: Boolean = false
        set(value) {
            field = value
            if (field) {
                when (fontFamily) {
                    1 -> binding.textValue.setBackgroundResource(R.drawable.ripple_btn_rekan)
                    2 -> binding.textValue.setBackgroundResource(R.drawable.ripple_btn_ipubers)
                }
                binding.textLabel.setOnClickListener {
                    onLabelClick?.invoke()
                }
            }
        }

    var isValueClickable: Boolean = false
        set(value) {
            field = value
            if (field) {
                when (fontFamily) {
                    1 -> binding.textValue.setBackgroundResource(R.drawable.ripple_btn_rekan)
                    2 -> binding.textValue.setBackgroundResource(R.drawable.ripple_btn_ipubers)
                }
                binding.textValue.setOnClickListener {
                    onValueClick?.invoke()
                }
            }
        }

    private var onValueClick: (() -> Unit)? = null
    fun setOnValueClick(onClick: () -> Unit) {
        onValueClick = onClick
    }

    private var onLabelClick: (() -> Unit)? = null
    fun setOnLabelClick(onClick: () -> Unit) {
        onLabelClick = onClick
    }

    fun setValueTextColor(@ColorRes color: Int) {
        binding.textValue.setTextColor(ContextCompat.getColor(context, color))
    }

    fun setLabelTextColor(@ColorRes color: Int) {
        binding.textLabel.setTextColor(ContextCompat.getColor(context, color))
    }

    fun setLabelIcon(
        @DrawableRes iconStart: Int?,
        @DrawableRes iconEnd: Int?
    ) {
        if (iconStart == null && iconEnd == null) {
            binding.textLabel.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                null,
                null,
                null
            )
            return
        }

        if (iconStart != null && iconEnd != null) {
            binding.textLabel.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, iconStart),
                null,
                ContextCompat.getDrawable(context, iconEnd),
                null
            )
            return
        }

        if (iconStart != null) {
            binding.textLabel.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, iconStart),
                null,
                null,
                null
            )
            return
        }

        if (iconEnd != null) {
            binding.textLabel.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                null,
                ContextCompat.getDrawable(context, iconEnd),
                null
            )
            return
        }
    }

    fun setValueIcon(
        @DrawableRes iconStart: Int?,
        @DrawableRes iconEnd: Int?
    ) {
        if (iconStart == null && iconEnd == null) {
            binding.textValue.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                null,
                null,
                null
            )
            return
        }

        if (iconStart != null && iconEnd != null) {
            binding.textValue.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, iconStart),
                null,
                ContextCompat.getDrawable(context, iconEnd),
                null
            )
            return
        }

        if (iconStart != null) {
            binding.textValue.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, iconStart),
                null,
                null,
                null
            )
            binding.textValue.textAlignment = TEXT_ALIGNMENT_TEXT_START
            return
        }

        if (iconEnd != null) {
            binding.textValue.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                null,
                ContextCompat.getDrawable(context, iconEnd),
                null
            )
            binding.textValue.textAlignment = TEXT_ALIGNMENT_TEXT_END
            return
        }
    }

    fun setValueFontWeight(fontWeight: FontWeightDataTile) {
        when (fontWeight) {
            Regular -> {
                when (fontFamily) {
                    1 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    2 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    3 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }
            }
            Medium -> {
                when (fontFamily) {
                    1 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    2 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    3 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }
            }
            SemiBold -> {
                when (fontFamily) {
                    1 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                    2 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    3 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }
            }
            Bold -> {
                when (fontFamily) {
                    1 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    2 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                    3 -> binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                }
            }
        }
    }

    fun setLabelFontWeight(fontWeight: FontWeightDataTile) {
        when (fontWeight) {
            Regular -> {
                when (fontFamily) {
                    1 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    2 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    3 ->  binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }
            }
            Medium -> {
                when (fontFamily) {
                    1 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    2 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    3 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }
            }
            SemiBold -> {
                when (fontFamily) {
                    1 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                    2 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    3 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }
            }
            Bold -> {
                when (fontFamily) {
                    1 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    2 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                    3 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                }
            }
        }
    }

    fun setFont(fontFamily: Int) {
        this.fontFamily = fontFamily
        binding.textLabel.typeface = ResourcesCompat.getFont(context, this.fontFamily)
        binding.textValue.typeface = ResourcesCompat.getFont(context, this.fontFamily)
    }

    fun setLabelTextSize(textSizeInSp: Float) {
        binding.textLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeInSp)
    }

    fun setValueTextSize(textSizeInSp: Float) {
        binding.textValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeInSp)
    }

    init {
        binding = LayoutDataTileBinding.inflate(LayoutInflater.from(context), this)

        orientation = HORIZONTAL

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.DataTile, 0, 0)
        label = typedArray.getString(R.styleable.DataTile_rekan_label)
        isLabelClickable = typedArray.getBoolean(R.styleable.DataTile_rekan_isLabelClickable, false)
        isValueClickable = typedArray.getBoolean(R.styleable.DataTile_rekan_isValueClickable, false)

        val labelIconStart = typedArray.getResourceId(R.styleable.DataTile_rekan_label_iconStart, -1)
        val labelIconEnd = typedArray.getResourceId(R.styleable.DataTile_rekan_label_iconEnd, -1)
        val labelFontWeight = typedArray.getInt(R.styleable.DataTile_rekan_label_FontWeight, 2)
        val labelTextColor = typedArray.getColor(R.styleable.DataTile_rekan_label_textColor, resources.getColor(R.color.D500))
        val labelTextSize = typedArray.getFloat(R.styleable.DataTile_rekan_label_textSize, 14f)

        value = typedArray.getString(R.styleable.DataTile_rekan_value)
        val valueFontWeight = typedArray.getInt(R.styleable.DataTile_rekan_value_FontWeight, 3)
        val valueTextColor = typedArray.getColor(R.styleable.DataTile_rekan_value_textColor, resources.getColor(R.color.D800))
        val valueTextSize = typedArray.getFloat(R.styleable.DataTile_rekan_value_textSize, 14f)
        val valueTextAligment = typedArray.getInt(R.styleable.DataTile_rekan_value_textAlignment, 3)
        val valueIconStart = typedArray.getResourceId(R.styleable.DataTile_rekan_value_iconStart, -1)
        val valueIconEnd = typedArray.getResourceId(R.styleable.DataTile_rekan_value_iconEnd, -1)
        fontFamily = typedArray.getInt(R.styleable.DataTile_rekan_font_family, 1)
        val typeFace = if(fontFamily == 2){
            ResourcesCompat.getFont(context,R.font.wix_madefor)
        }else{
            ResourcesCompat.getFont(context,R.font.noto_sans)
        }
        maxLines = typedArray.getInt(R.styleable.DataTile_rekan_setMaxLines, 3)

        binding.textValue.isVisible = !value.isNullOrEmpty()
        binding.textLabel.isVisible = !label.isNullOrEmpty()

        binding.textLabel.maxLines = maxLines
        binding.textValue.maxLines = maxLines

        binding.textLabel.typeface = typeFace
        binding.textValue.typeface = typeFace
        binding.textLabel.text = label
        when (labelFontWeight) {
            1 -> {
                if(fontFamily == 2){
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }else{
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                }
            }
            2 -> {
                if(fontFamily == 2){
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }else{
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                }
            }
            3 -> {
                if(fontFamily == 2){
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }else{
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                }
            }
            4 -> {
                if(fontFamily == 2){
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }else{
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                }
            }
            5 -> {
                if(fontFamily == 2){
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                }else{
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                }
            }
            6 -> {
                if(fontFamily == 2){
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                }else{
                    binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_black)
                }
            }
        }
        binding.textLabel.setTextColor(labelTextColor)
        binding.textLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, labelTextSize)

        binding.textValue.text = value
        when (valueFontWeight) {
            1 -> {
                if(fontFamily == 2){
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }else{
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }
            }
            2 -> {
                if(fontFamily == 2){
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }else{
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                }
            }
            3 -> {
                if(fontFamily == 2){
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }else{
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                }
            }
            4 -> {
                if(fontFamily == 2){
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }else{
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                }
            }
            5 -> {
                if(fontFamily == 2){
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                }else{
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                }
            }
            6 -> {
                if(fontFamily == 2){
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                }else{
                    binding.textValue.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_black)
                }
            }
        }
        binding.textValue.setTextColor(valueTextColor)
        binding.textValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, valueTextSize)
        binding.textValue.textAlignment = valueTextAligment

        if (isValueClickable) {
            binding.textValue.setOnClickListener {
                onValueClick?.invoke()
            }
        }

        if (isLabelClickable) {
            binding.textLabel.setOnClickListener {
                onLabelClick?.invoke()
            }
        }

        setDataTileIcon(labelIconStart, labelIconEnd, binding.textLabel)

        setDataTileIcon(valueIconStart, valueIconEnd, binding.textValue)

        typedArray.recycle()
    }

    private fun setDataTileIcon(
        iconStart: Int,
        iconEnd: Int,
        textView: TextView,
    ) {
        if (iconStart != -1 && iconEnd != -1) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, iconStart),
                null,
                ContextCompat.getDrawable(context, iconEnd),
                null
            )
            return
        }

        if (iconStart != -1) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, iconStart),
                null,
                null,
                null
            )
            if (textView.id == binding.textValue.id) {
                binding.textValue.textAlignment = TEXT_ALIGNMENT_TEXT_START
            }
            return
        }

        if (iconEnd != -1) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                null,
                null,
                ContextCompat.getDrawable(context, iconEnd),
                null
            )
            if (textView.id == binding.textValue.id) {
                binding.textValue.textAlignment = TEXT_ALIGNMENT_TEXT_END
            }
            return
        }
    }
}

enum class FontWeightDataTile {
    Regular,
    Medium,
    SemiBold,
    Bold
}