package com.rekanesia

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.Uri
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import androidx.core.net.toUri
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import java.io.File

class ImagePopup(context: Context) {

    private lateinit var dialog: Dialog
    private var bitmap: Bitmap? = null
    private var file: File? = null
    private var resId: Int? = null
    private var uri: Uri? = null

    init {
        init(context)
    }

    constructor(context: Context, bitmap: Bitmap) : this(context) {
        this.bitmap = bitmap
        init(context)
    }

    constructor(context: Context, file: File) : this(context) {
        this.file = file
        init(context)
    }

    constructor(context: Context, resId: Int) : this(context) {
        this.resId = resId
        init(context)
    }

    constructor(context: Context, uri: Uri) : this(context) {
        this.uri = uri
        init(context)
    }

    private fun init(context: Context) {
        dialog = Dialog(context)
        val bgDialog = ColorDrawable(Color.TRANSPARENT)
        val insetDrawable = InsetDrawable(bgDialog, 32)
        dialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.layout_image_popup)
            window?.setBackgroundDrawable(insetDrawable)
            window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
        closeImagePreviewDialog()
    }

    fun withAnimPopupTransition(): ImagePopup = apply {
        dialog.window?.attributes?.windowAnimations = R.style.Rekanesia_Dialog
    }

    fun show() {
        val imagePreview = dialog.findViewById<SubsamplingScaleImageView>(R.id.preview_image)

        when {
            bitmap != null -> {
                imagePreview.setImage(ImageSource.bitmap(bitmap!!))
            }
            file != null -> {
                imagePreview.setImage(ImageSource.uri(file!!.toUri()))
            }
            uri != null -> {
                imagePreview.setImage(ImageSource.uri(uri!!))
            }
            resId != null -> {
                imagePreview.setImage(ImageSource.resource(resId!!))
            }
        }
        dialog.show()
    }

    private fun closeImagePreviewDialog() {
        val btnClose = dialog.findViewById<ImageView>(R.id.btn_close)
        btnClose.setOnClickListener {
            dialog.dismiss()
        }
    }

}
