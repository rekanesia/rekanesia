package com.rekanesia.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Build
import android.text.Html
import android.text.InputFilter
import android.text.InputType
import android.text.Spanned
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.rekanesia.helper.InputTypeRekanesia.*


object TextFieldHelper {
    fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    fun setInputType(editText: EditText, inputType: InputTypeRekanesia) {
        when (inputType) {
            Date -> editText.inputType = InputType.TYPE_CLASS_DATETIME or InputType.TYPE_DATETIME_VARIATION_DATE
            DateTime -> editText.inputType = InputType.TYPE_CLASS_DATETIME or InputType.TYPE_DATETIME_VARIATION_NORMAL
            Number -> editText.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_NORMAL
            NumberDecimal -> editText.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            NumberPassword -> editText.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            Phone -> editText.inputType = InputType.TYPE_CLASS_PHONE
            Text -> editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL
            TextCapCharacters -> editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            TextCapSentence -> editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
            TextEmailAddress -> editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            TextLongMessage -> editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE
            TextMultiline -> {
                editText.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
                editText.isSingleLine = false
                editText.imeOptions = EditorInfo.IME_FLAG_NO_ENTER_ACTION
            }
            TextPassword -> editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            TextPersonName -> editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PERSON_NAME
            Currency -> editText.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_NORMAL
            Alphanumeric ->{
                editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
                editText.filters = arrayOf(InputFilter { source, _, _, _, _, _ ->
                    // Filter to allow only alphanumeric characters
                    source.filter { it.isLetterOrDigit() }
                })
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun setOnClickClear(input: EditText,inputType:InputTypeRekanesia = Text,onClickDrawableEnd:(()->Unit) = {}) {
        input.setOnTouchListener { _, motionEvent ->
            try {
                val drawableEnd = 2
                val isButtonClearClicked =
                    motionEvent.rawX >= (input.right - input.compoundDrawables[drawableEnd].bounds.width())
                if (motionEvent.action == MotionEvent.ACTION_UP) {
                    if (isButtonClearClicked) {
                        Log.d("input type set on click","$inputType")
                        when(inputType){
                            NumberPassword,TextPassword->Unit
                            else->input.setText("")
                        }
                        onClickDrawableEnd.invoke()
                        return@setOnTouchListener true
                    }
                }
                false
            } catch (e: Exception) {
                e.printStackTrace()
                false
            }
        }
    }

    fun String.fromHtml(): Spanned? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(this)
        }
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun Context.showKeyboard(editText: View) {
        val ipManager =
            this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        ipManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    fun String.removeDelimiter(): String {
        val regex = Regex("\\D")
        return regex.replace(this, "")
    }
}