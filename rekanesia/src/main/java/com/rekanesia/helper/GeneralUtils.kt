package com.rekanesia.helper

import android.content.res.Resources
import android.view.View
import android.view.Window
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import java.text.DecimalFormat

object GeneralUtils {

    val Int.dp: Int
        get() = (this / Resources.getSystem().displayMetrics.density).toInt()

    //Convert dp to px
    val Int.px: Int
        get() = (this * Resources.getSystem().displayMetrics.density).toInt()

    fun clearTextValue(
        value: String,
        prefix: String,
        suffix: String
    ): String =
        value.replace(prefix, "").replace(suffix, "")

    /**
     * Adds a delimiter to this String representing a number with a specified format.
     * The delimiter will be added to every three digits with the format "#,###".
     *
     * @return the formatted string with delimiter added.
     *
     * Example usage:
     * ```
     * val input = 1000
     * val output = input.addDelimiter()
     * println(output) // Output: 1.000
     * ```
     */
    fun String.addDelimiter(): String {
        return try {
            val formatter = DecimalFormat("#,###")
            formatter.format(this.toInt()).replace(",", ".")
        } catch (e: Exception) {
            this
        }
    }

    fun View.showKeyboardAndFocus(window: Window) {
        WindowCompat.getInsetsController(window, this).show(WindowInsetsCompat.Type.ime())
        requestFocus()
    }
}