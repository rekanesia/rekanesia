package com.rekanesia.helper

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


object ImageHelper {

    private const val DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

    fun Bitmap.saveToStorages(
        context: Context,
        name: String,
        quality: Int = 100,
        compressFormat: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG
    ): File {
        val file = createCustomTempFile(context, name)
        val fileOutput = FileOutputStream(file)
        compress(compressFormat, quality, fileOutput)
        fileOutput.flush()
        fileOutput.close()
        return file
    }

    @Throws(IOException::class)
    fun createCustomTempFile(context: Context, name: String): File {
        val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "${name}_${
                getCurrentDateTime().toTimeFormat("yyyyMMdd_HHmmss")
            }", ".jpg", storageDir
        )
    }

    fun getCurrentDateTime(): String {
        val sdf = SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT, Locale("IND","ID"))
        return sdf.format(Date())
    }

    @SuppressLint("SimpleDateFormat")
    fun String.toTimeFormat(toFormat:String = "dd-MM-yyyy HH:mm"): String {
        val idf = SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT,Locale("IND","ID")).parse(this)
        return SimpleDateFormat(toFormat).format(idf!!)
    }

    fun rotateBitmapImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        val degressTo = when (angle) {
            90F -> 360F
            270F -> 360F
            180F -> 180F
            else -> angle
        }
        matrix.postRotate(degressTo)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }
}
