package com.rekanesia.helper

enum class InputTypeRekanesia {
    Date,
    DateTime,
    Number,
    NumberDecimal,
    NumberPassword,
    Phone,
    Text,
    TextCapCharacters,
    TextCapSentence,
    TextEmailAddress,
    TextLongMessage,
    TextMultiline,
    TextPassword,
    TextPersonName,
    Currency,
    Alphanumeric
}