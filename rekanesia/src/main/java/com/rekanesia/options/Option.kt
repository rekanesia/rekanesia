package com.rekanesia.options

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.annotation.StyleRes
import com.rekanesia.R
import com.rekanesia.helper.OptionType
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.values

class Option @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {
    var label: String? = null
    var checked: Boolean = false
    var requiredTextField: Boolean = false
    var isDisable: Boolean = false
    var prefix: String = ""
    var suffix: String = ""
    var id: String = ""
    var typeOption: OptionType = OptionType.GENERAL
    var topRightTextValue:String = ""
    @DrawableRes var topRightImage: Int = -1
    var topRightImageUrl: String =""
    var topLeftTextValue:String = ""
    var bottomLeftTextValue:String = ""
    var bottomRightTextValue:String = ""
    @StyleRes var topRightTextStyle: Int = -1
    @StyleRes var topLeftTextStyle: Int = -1
    @StyleRes var bottomLeftTextStyle: Int = -1
    @StyleRes var bottomRightTextStyle: Int = -1

    init {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.Option, 0, 0)
            id = typedArray.getString(R.styleable.Option_rekan_id).orEmpty()
            label = typedArray.getString(R.styleable.Option_rekan_label).orEmpty()
            checked = typedArray.getBoolean(R.styleable.Option_rekan_checked, false)
            requiredTextField = typedArray.getBoolean(R.styleable.Option_rekan_requiredTextField, false)
            prefix = typedArray.getString(R.styleable.Option_rekan_prefixSymbol).orEmpty()
            suffix = typedArray.getString(R.styleable.Option_rekan_suffixSymbol).orEmpty()
            typeOption = OptionType.values()[typedArray.getInt(R.styleable.Option_rekan_type_option, 0)]
            topRightTextStyle = typedArray.getResourceId(R.styleable.Option_rekan_opt_top_right_style,R.style.Rekanesia_TextSemiBold_Black)
            topLeftTextStyle = typedArray.getResourceId(R.styleable.Option_rekan_opt_top_left_style,R.style.Rekanesia_TextSemiBold_Black)
            bottomRightTextStyle = typedArray.getResourceId(R.styleable.Option_rekan_opt_bottom_right_style,R.style.Rekanesia_TextRegular)
            bottomLeftTextStyle = typedArray.getResourceId(R.styleable.Option_rekan_opt_bottom_left_style,R.style.Rekanesia_TextRegular)
            topRightTextValue = typedArray.getString(R.styleable.Option_rekan_opt_top_right_value).orEmpty()
            topRightImageUrl = typedArray.getString(R.styleable.Option_rekan_opt_top_right_image_url).orEmpty()
            topRightImage = typedArray.getResourceId(R.styleable.Option_rekan_opt_top_right_image, -1)
            topRightImage = typedArray.getResourceId(R.styleable.Option_rekan_opt_top_right_image, -1)
            isDisable = !typedArray.getBoolean(R.styleable.Option_rekan_enabled, true)
            if(typeOption == OptionType.FourSide){
                val nonNullCount = listOfNotNull(
                    topRightTextValue.takeIf { item-> item.isNotEmpty() },
                    topRightImageUrl.takeIf { item-> item.isNotEmpty() },
                    topRightImage.takeIf { item-> item != -1 }
                ).size

                // Ensure only one is set
                if (nonNullCount != 1) {
                    throw IllegalArgumentException("Only one of topRightTextValue, topRightImageUrl, or topRightImage must be provided in XML.")
                }
            }

            topLeftTextValue = typedArray.getString(R.styleable.Option_rekan_opt_top_left_value).orEmpty()
            bottomRightTextValue = typedArray.getString(R.styleable.Option_rekan_opt_bottom_right_value).orEmpty()
            bottomLeftTextValue = typedArray.getString(R.styleable.Option_rekan_opt_bottom_left_value).orEmpty()
            typedArray.recycle()
        }
    }
}