package com.rekanesia.options

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.rekanesia.R
import com.rekanesia.databinding.LayoutOptionGroupBinding
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*

class OptionGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {
    private val binding: LayoutOptionGroupBinding by lazy {
        LayoutOptionGroupBinding.inflate(LayoutInflater.from(context), this)
    }

    private lateinit var rvAdapter: OptionGroupListAdapter

    private var style = Rekan

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.textMandatory.isVisible = field
        }

    var isEnabledClick: Boolean = true
        set(value) {
            field = value
            if (::rvAdapter.isInitialized) {
                rvAdapter.isClickable = field
            }
        }

    init {

        orientation = VERTICAL
        gravity = Gravity.CENTER

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.OptionGroup, 0, 0)
        val label = typedArray.getString(R.styleable.OptionGroup_rekan_label)
        isMandatory = typedArray.getBoolean(R.styleable.OptionGroup_rekan_mandatory, false)
        isEnabledClick = typedArray.getBoolean(R.styleable.OptionGroup_rekan_enabled_click, true)
        style = values()[typedArray.getInt(R.styleable.OptionGroup_rekan_styles, 1) - 1]
        val isMultipleChoice = typedArray.getBoolean(R.styleable.OptionGroup_rekan_isMultipleChoice, false)
        val inputType = InputTypeRekanesia.values()[typedArray.getInt(
            R.styleable.OptionGroup_rekan_setInputType,
            InputTypeRekanesia.Currency.ordinal
        ) - 1]

        if (label != null) {
            binding.textLabel.text = label
            binding.containerLabel.visibility = View.VISIBLE
        } else {
            binding.containerLabel.visibility = View.GONE
        }

        setStyle()

        binding.textMandatory.visibility = if (isMandatory) View.VISIBLE else View.GONE

        rvAdapter = OptionGroupListAdapter(inputType, isMultipleChoice, style)
        rvAdapter.isClickable = isEnabledClick
        binding.rv.layoutManager = LinearLayoutManager(context)
        binding.rv.adapter = rvAdapter

        typedArray.recycle()
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    textLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    textLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    textMandatory.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    textMandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))
                    textMandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    containerSelect.setBackgroundResource(R.drawable.bg_select)
                }
                Ipubers -> {
                    textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    textLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    textLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

                    textMandatory.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    textMandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))
                    textMandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

                    containerSelect.setBackgroundResource(R.drawable.bg_select_psp)
                }
            }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        val listForRv = mutableListOf<Opt>()

        for (index in 0 until childCount) {
            val child = getChildAt(index)
            if (child is Option) {
                listForRv.add(
                    Opt(
                        index,
                        child.label ?: "",
                        child.checked,
                        child.requiredTextField,
                        child.prefix,
                        child.suffix,
                        child.id
                    )
                )
            }
        }

        rvAdapter.replaceItems(listForRv.toList())
    }

    fun onSelectedTextFieldChange(onTextChange: (String) -> Unit) {
        rvAdapter.onTextFieldChange { onTextChange.invoke(it) }
    }

    fun onOptionSelected(onSelected: (Opt?) -> Unit = {}) {
        rvAdapter.onOptionSelected { onSelected.invoke(it) }
    }

    fun onMultipleOptionsSelected(onSelected: (List<Opt>?) -> Unit) {
        rvAdapter.setOnMultipleOptionsSelected { onSelected.invoke(it) }
    }

    fun getSelectedItem(): Opt? = rvAdapter.getSelectedItem()

    fun getSelectedItems(): List<Opt> = rvAdapter.getMultipleSelectedOptions()

    fun replaceItems(list: List<Opt>) {
        rvAdapter.replaceItems(list)
    }

    fun addItem(item: Opt) {
        rvAdapter.addItem(item)
    }

    fun notifyDataSetChange() {
        rvAdapter.notifyDataSetChanged()
    }

    fun setEnabledNestedScroll(enabledNestedScroll: Boolean) {
        binding.rv.isNestedScrollingEnabled = enabledNestedScroll
    }
}