package com.rekanesia.options

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.rekanesia.R
import com.rekanesia.databinding.ItemOptionBinding
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import com.rekanesia.helper.TextFieldHelper.showKeyboard

data class Opt(
    var id: Int,
    var label: String,
    var selected: Boolean,
    var requiredTextField: Boolean = false,
    var prefix: String = "",
    var suffix: String = "",
    var providedId: String? = null,
    var statusCode:String?=null // kebutuhan dinamis data
)

class OptionGroupListAdapter(
    private val inputType: InputTypeRekanesia,
    private val isMultipleChoice: Boolean = false,
    private val style: StyleRekanesia = Rekan
) : RecyclerView.Adapter<OptionGroupListAdapter.ViewHolder>() {

    private var items = mutableListOf<Opt>()

    var isClickable = true

    private var onTextFieldChange: ((String) -> Unit)? = null
    fun onTextFieldChange(onTextChange: (String) -> Unit) {
        onTextFieldChange = onTextChange
    }

    private var onMultipleOptionsSelected: ((List<Opt>) -> Unit)? = null
    fun setOnMultipleOptionsSelected(onItemSelected: (List<Opt>) -> Unit) {
        onMultipleOptionsSelected = onItemSelected
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemOptionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = this.items[position]
        holder.bind(item, position)
    }

    override fun getItemCount() = items.size

    fun replaceItems(items: List<Opt>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun addItem(item: Opt) {
        this.items.add(item)
        notifyItemChanged(items.size - 1)
    }

    fun getSelectedItem(): Opt? {
        val selectedCount = items.filter { it.selected }.size
        return if (selectedCount != 0) {
            items.first { it.selected }
        } else {
            null
        }
    }

    fun getMultipleSelectedOptions(): List<Opt> {
        return try {
            items.filter { it.selected }
        } catch (e: Exception) {
            emptyList()
        }
    }

    private var optionSelected: ((Opt?) -> Unit)? = null
    fun onOptionSelected(onSelected: (Opt?) -> Unit) {
        optionSelected = onSelected
    }

    inner class ViewHolder(private val binding: ItemOptionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: Opt, position: Int) {
            val context = itemView.context
            binding.apply {
                TextFieldHelper.setInputType(tfOption.input, inputType)
                tfOption.input.imeOptions = EditorInfo.IME_ACTION_DONE
                tfOption.prefix = item.prefix
                tfOption.isVisible = item.requiredTextField && item.selected
                tfOption.suffix = item.suffix

                textValue.text = item.label

                if (item.selected) {
                    imageCheck.visibility = View.VISIBLE
                    if (item.requiredTextField) {
                        context.showKeyboard(root)
                        tfOption.input.requestFocus()
                        tfOption.input.doOnTextChanged { text, _, _, _ ->
                            if ((item.prefix.isNotEmpty() || item.suffix.isNotEmpty()) && inputType == InputTypeRekanesia.Currency) {
                                onTextFieldChange?.invoke(text.toString().removeDelimiter())
                            } else {
                                onTextFieldChange?.invoke(text.toString())
                            }
                        }
                        if (item.prefix.isNotEmpty() && (inputType == InputTypeRekanesia.Currency || inputType == InputTypeRekanesia.Number)) {
                            tfOption.input.setText(item.prefix)
                        }
                        if (item.suffix.isNotEmpty() && (inputType == InputTypeRekanesia.Currency || inputType == InputTypeRekanesia.Number)) {
                            tfOption.input.setText("0" + item.suffix)
                        } else if (item.suffix.isNotEmpty()) {
                            tfOption.input.setText(item.suffix)
                        }
                    }

                    when (style) {
                        Rekan -> {
                            textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_bold)
                            textValue.setTextColor(ContextCompat.getColor(binding.root.context, R.color.B500))

                            imageCheck.backgroundTintList = ContextCompat.getColorStateList(context, R.color.B500)

                            tfOption.style = style
                        }
                        Ipubers -> {
                            textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_bold)
                            textValue.setTextColor(ContextCompat.getColor(binding.root.context, R.color.L500))

                            imageCheck.backgroundTintList = ContextCompat.getColorStateList(context, R.color.L500)

                            tfOption.style = style
                        }
                    }
                } else {
                    imageCheck.visibility = View.INVISIBLE
                    textValue.typeface =
                        ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                    textValue.setTextColor(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.D800
                        )
                    )
                }

                if (position == (itemCount - 1)) {
                    divider.visibility = View.INVISIBLE
                } else {
                    divider.visibility = View.VISIBLE
                }

                root.setOnClickListener {
                    if (isClickable.not()) {
                        return@setOnClickListener
                    }
                    if (isMultipleChoice) {
                        val isSelected = items[adapterPosition].selected
                        items[adapterPosition].selected = !isSelected
                        val selectedOptions = items.filter { it.selected }
                        notifyDataSetChanged()
                        onMultipleOptionsSelected?.invoke(selectedOptions)
                    } else {
                        if (!item.selected) {
                            items.forEach {
                                it.selected = false
                            }
                            items[position].selected = true
                            notifyDataSetChanged()
                            optionSelected?.invoke(item)
                        }
                    }
                }
            }

        }
    }
}