package com.rekanesia.options

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.rekanesia.R
import com.rekanesia.databinding.LayoutOptionGroupBinding
import com.rekanesia.databinding.LayoutOptionGroupMultipleViewBinding
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.OptionType
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*

class OptionGroupMultipleView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {
    private val binding: LayoutOptionGroupMultipleViewBinding by lazy {
        LayoutOptionGroupMultipleViewBinding.inflate(LayoutInflater.from(context), this)
    }

    private lateinit var rvAdapter: OptionGroupDynamicListAdapter

    private var style = Rekan

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.textMandatory.isVisible = field
        }

    var isEnabledClick: Boolean = true
        set(value) {
            field = value
            if (::rvAdapter.isInitialized) {
                rvAdapter.isClickable = field
            }
        }

    init {

        orientation = VERTICAL
        gravity = Gravity.CENTER

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.OptionGroupMultipleView, 0, 0)
        val label = typedArray.getString(R.styleable.OptionGroupMultipleView_rekan_label)
        isMandatory = typedArray.getBoolean(R.styleable.OptionGroupMultipleView_rekan_mandatory, false)
        isEnabledClick = typedArray.getBoolean(R.styleable.OptionGroupMultipleView_rekan_enabled_click, true)
        style = values()[typedArray.getInt(R.styleable.OptionGroupMultipleView_rekan_styles, 1) - 1]
        val isMultipleChoice = typedArray.getBoolean(R.styleable.OptionGroupMultipleView_rekan_isMultipleChoice, false)
        val inputType = InputTypeRekanesia.values()[typedArray.getInt(
            R.styleable.OptionGroupMultipleView_rekan_setInputType,
            InputTypeRekanesia.Currency.ordinal
        ) - 1]

        if (label != null) {
            binding.textLabel.text = label
            binding.containerLabel.visibility = View.VISIBLE
        } else {
            binding.containerLabel.visibility = View.GONE
        }

        setStyle()

        binding.textMandatory.visibility = if (isMandatory) View.VISIBLE else View.GONE

        rvAdapter = OptionGroupDynamicListAdapter(inputType, isMultipleChoice, style)
        rvAdapter.isClickable = isEnabledClick
        binding.rv.layoutManager = LinearLayoutManager(context)
        binding.rv.adapter = rvAdapter

        typedArray.recycle()
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    textLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    textLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    textMandatory.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    textMandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))
                    textMandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    containerSelect.setBackgroundResource(R.drawable.bg_select)
                }
                Ipubers -> {
                    textLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    textLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    textLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

                    textMandatory.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    textMandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))
                    textMandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

                    containerSelect.setBackgroundResource(R.drawable.bg_select_psp)
                }
            }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        val listForRv = mutableListOf<TypeListView>()

        for (index in 0 until childCount) {
            val child = getChildAt(index)
            if (child is Option) {
                when(child.typeOption){
                    OptionType.GENERAL->{
                        listForRv.add(
                            TypeListView.ViewTypeGeneral(
                                index,
                                child.label ?: "",
                                child.checked,
                                child.requiredTextField,
                                child.prefix,
                                child.suffix,
                                child.id,
                                isDisable = child.isDisable
                            )
                        )
                    }

                    OptionType.FourSide->{
                        val topRightContent = when {
                            child.topRightTextValue.isNotEmpty() -> TopRightContent.Text(child.topRightTextValue)
                            child.topRightImage != -1 -> TopRightContent.DrawableResource(child.topRightImage)
                            child.topRightImageUrl.isNotEmpty() -> TopRightContent.Url(child.topRightImageUrl)
                            else -> throw RuntimeException("topRightContent not found, must be text, drawable, or URL")
                        }
                        listForRv.add(
                            TypeListView.ViewTypeWithFourSideInformation(
                                id = index,
                                topLeftValue = child.topLeftTextValue,
                                topLeftStyleText = child.topLeftTextStyle,
                                topRightValue = topRightContent,
                                topRightStyleText = child.topLeftTextStyle,
                                bottomLeftValue = child.bottomLeftTextValue,
                                bottomLeftStyleText = child.bottomLeftTextStyle,
                                bottomRightValue = child.bottomRightTextValue,
                                bottomRightStyleText = child.bottomRightTextStyle,
                                selected = child.checked,
                                providedId = child.id,
                                isDisable = child.isDisable
                            )
                        )
                    }
                }
            }
        }

        rvAdapter.submitList(listForRv.toList())
    }

    fun onSelectedTextFieldChange(onTextChange: (String) -> Unit) {
        rvAdapter.onTextFieldChange { onTextChange.invoke(it) }
    }

    fun onOptionSelected(onSelected: (TypeListView?) -> Unit = {}) {
        rvAdapter.onOptionSelected { onSelected.invoke(it) }
    }

    fun onMultipleOptionsSelected(onSelected: (List<TypeListView>?) -> Unit) {
        rvAdapter.setOnMultipleOptionsSelected { onSelected.invoke(it) }
    }

    fun getSelectedItem(): TypeListView? = rvAdapter.getSelectedOption()

    fun getSelectedItems(): List<TypeListView> = rvAdapter.getMultipleSelectedOptions()

    fun replaceItems(list: List<TypeListView>) {
        rvAdapter.submitList(list)
    }
    fun setEnabledNestedScroll(enabledNestedScroll: Boolean) {
        binding.rv.isNestedScrollingEnabled = enabledNestedScroll
    }
}