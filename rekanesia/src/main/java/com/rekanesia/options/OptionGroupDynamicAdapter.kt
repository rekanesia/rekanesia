package com.rekanesia.options

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.StyleRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.airbnb.paris.extensions.style
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target
import com.rekanesia.R
import com.rekanesia.databinding.ItemOptionBinding
import com.rekanesia.databinding.ItemOptionWithInfoBinding
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import com.rekanesia.helper.TextFieldHelper.showKeyboard

sealed class TopRightContent {
    data class Url(val url: String) : TopRightContent()
    data class DrawableResource(val drawable: Int) : TopRightContent() // Use resource ID
    data class Text(val text: String) : TopRightContent() // Optional, if you want to support plain text
}

sealed class TypeListView{
    abstract var selected: Boolean
    abstract var isDisable:Boolean
    data class ViewTypeGeneral(
        var id: Int,
        var label: String,
        override var selected: Boolean,
        var requiredTextField: Boolean = false,
        var prefix: String = "",
        var suffix: String = "",
        var providedId: String? = null,
        var statusCode:String?=null, // kebutuhan dinamis data
        override var isDisable: Boolean = false
    ):TypeListView()

    data class ViewTypeWithFourSideInformation(
        var id: Int,
        var topLeftValue: String,
        @StyleRes var topLeftStyleText:Int = R.style.Rekanesia_TextSemiBold_Black,
        var topRightValue: TopRightContent,
        @StyleRes var topRightStyleText:Int = R.style.Rekanesia_TextSemiBold_Black,
        var bottomRightValue: String,
        @StyleRes var bottomRightStyleText:Int = R.style.Rekanesia_TextRegular,
        var bottomLeftValue: String,
        @StyleRes var bottomLeftStyleText:Int = R.style.Rekanesia_TextRegular,
        override var selected: Boolean,
        var providedId: String? = null,
        var statusCode:String?=null, // kebutuhan dinamis data
        override var isDisable: Boolean = false
    ):TypeListView()
}

class OptionGroupDynamicListAdapter(
    private val inputType: InputTypeRekanesia,
    private val isMultipleChoice: Boolean = false,
    private val style: StyleRekanesia = Rekan
):ListAdapter<TypeListView, ViewHolder>(listItemDiffCallback){
    var isClickable = true
    private var onTextFieldChange: ((String) -> Unit)? = null
    fun onTextFieldChange(onTextChange: (String) -> Unit) {
        onTextFieldChange = onTextChange
    }

    private var onMultipleOptionsSelected: ((List<TypeListView>) -> Unit)? = null
    fun setOnMultipleOptionsSelected(onItemSelected: (List<TypeListView>) -> Unit) {
        onMultipleOptionsSelected = onItemSelected
    }

    private var optionSelected: ((TypeListView?) -> Unit)? = null
    fun onOptionSelected(onSelected: (TypeListView?) -> Unit) {
        optionSelected = onSelected
    }

    override fun getItemViewType(position: Int): Int {
        return when(getItem(position)){
            is TypeListView.ViewTypeGeneral -> VIEW_TYPE_GENERAL
            is TypeListView.ViewTypeWithFourSideInformation-> VIEW_TYPE_WITH_INFORMATION_FOUR_SIDE
            else -> throw IllegalArgumentException("Invalid view type, must be one of $VIEW_TYPE_GENERAL or $VIEW_TYPE_WITH_INFORMATION_FOUR_SIDE")
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return when(viewType){
            VIEW_TYPE_GENERAL->{
                ViewHolderGeneral(
                    ItemOptionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                )
            }
            VIEW_TYPE_WITH_INFORMATION_FOUR_SIDE->{
                ViewHolderWithFourSideInformation(
                    ItemOptionWithInfoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                )
            }
            else->throw IllegalArgumentException("Invalid view type, must be one of $VIEW_TYPE_GENERAL or $VIEW_TYPE_WITH_INFORMATION_FOUR_SIDE")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)

        when(holder){
            is ViewHolderGeneral->holder.bind(item as TypeListView.ViewTypeGeneral)
            is ViewHolderWithFourSideInformation->holder.bind(item as TypeListView.ViewTypeWithFourSideInformation)
        }
    }

    inner class ViewHolderGeneral(private val binding: ItemOptionBinding):ViewHolder(binding.root){
        fun bind(item: TypeListView.ViewTypeGeneral){
            with(binding){
                val context = itemView.context
                TextFieldHelper.setInputType(tfOption.input, inputType)
                tfOption.prefix = item.prefix
                tfOption.isVisible = item.requiredTextField && item.selected
                tfOption.suffix = item.suffix

                textValue.text = item.label

                if(item.isDisable){
                    disableMode(binding)
                }else{
                    enableMode(binding)
                }


                imageCheck.visibility = if(item.selected) View.VISIBLE else View.INVISIBLE
                if(item.selected){
                    if (item.requiredTextField) {
                        context.showKeyboard(root)
                        tfOption.input.requestFocus()
                        tfOption.input.doOnTextChanged { text, _, _, _ ->
                            if ((item.prefix.isNotEmpty() || item.suffix.isNotEmpty()) && inputType == InputTypeRekanesia.Currency) {
                                onTextFieldChange?.invoke(text.toString().removeDelimiter())
                            } else {
                                onTextFieldChange?.invoke(text.toString())
                            }
                        }
                        if (item.prefix.isNotEmpty() && (inputType == InputTypeRekanesia.Currency || inputType == InputTypeRekanesia.Number)) {
                            tfOption.input.setText(item.prefix)
                        }
                        if (item.suffix.isNotEmpty() && (inputType == InputTypeRekanesia.Currency || inputType == InputTypeRekanesia.Number)) {
                            tfOption.input.setText("0" + item.suffix)
                        } else if (item.suffix.isNotEmpty()) {
                            tfOption.input.setText(item.suffix)
                        }
                    }


                    when (style) {
                        Rekan -> {
                            textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_bold)
                            textValue.setTextColor(ContextCompat.getColor(binding.root.context, R.color.B500))

                            imageCheck.backgroundTintList = ContextCompat.getColorStateList(context, R.color.B500)

                            tfOption.style = style
                        }
                        Ipubers -> {
                            textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_bold)
                            textValue.setTextColor(ContextCompat.getColor(binding.root.context, R.color.L500))

                            imageCheck.backgroundTintList = ContextCompat.getColorStateList(context, R.color.L500)

                            tfOption.style = style
                        }
                    }
                }else{
                    when(style){
                        Rekan->{
                            textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                        }
                        Ipubers->{
                            textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                        }
                    }
                    textValue.setTextColor(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.D800
                        )
                    )
                }

                if (layoutPosition == (itemCount - 1)) {
                    divider.visibility = View.INVISIBLE
                } else {
                    divider.visibility = View.VISIBLE
                }


                itemView.setOnClickListener{
                    if(isClickable.not() || item.isDisable){
                        return@setOnClickListener
                    }

                    if(isMultipleChoice){
                        updateWhenSelected(layoutPosition)
                        onMultipleOptionsSelected?.invoke(getMultipleSelectedOptions())
                    }else{
                        if(!item.selected){
                            updateWhenSelected(layoutPosition)
                            optionSelected?.invoke(item)
                        }
                    }
                }
            }
        }
    }

    private fun disableMode(binding: ItemOptionBinding){
        with(binding){
            when (style) {
                Rekan -> {
                    textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                    tfOption.style = style
                }
                Ipubers -> {
                    textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                    tfOption.style = style
                }
            }
            textValue.setTextColor(ContextCompat.getColor(binding.root.context, R.color.D300))
            disableView.isVisible = true
        }
    }
    private fun disableMode(binding: ItemOptionWithInfoBinding){
        with(binding){
            when (style) {
                Rekan -> {
                    textTopRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_semi_bold)
                    textTopLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_semi_bold)
                    textBottomLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                    textBottomRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                }
                Ipubers -> {
                    textTopRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_semi_bold)
                    textTopLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_semi_bold)
                    textBottomLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                    textBottomRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                }
            }
            textTopRight.setTextColor(ContextCompat.getColor(binding.root.context, R.color.D300))
            textTopLeft.setTextColor(ContextCompat.getColor(binding.root.context, R.color.D300))
            textBottomLeft.setTextColor(ContextCompat.getColor(binding.root.context, R.color.D300))
            textBottomRight.setTextColor(ContextCompat.getColor(binding.root.context, R.color.D300))
            disableView.isVisible = true
        }

    }

    private fun enableMode(binding: ItemOptionBinding){
        with(binding){
            when (style) {
                Rekan -> {
                    textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                    tfOption.style = style
                }
                Ipubers -> {
                    textValue.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                    tfOption.style = style
                }
            }
            textValue.setTextColor(ContextCompat.getColor(binding.root.context, R.color.D800))
            disableView.isVisible = false
        }
    }
    private fun enableMode(binding: ItemOptionWithInfoBinding,item: TypeListView.ViewTypeWithFourSideInformation){
        with(binding){
            textTopRight.style(item.topRightStyleText)
            textTopLeft.style(item.topLeftStyleText)
            textBottomLeft.style(item.bottomLeftStyleText)
            textBottomRight.style(item.bottomRightStyleText)
            when (style) {
                Rekan -> {
                    textTopRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                    textTopLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                    textBottomLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                    textBottomRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.noto_sans_regular)
                }
                Ipubers -> {
                    textTopRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                    textTopLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                    textBottomLeft.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                    textBottomRight.typeface = ResourcesCompat.getFont(binding.root.context, R.font.wix_madefor_regular)
                }
            }
            disableView.isVisible = false
        }
    }

    inner class ViewHolderWithFourSideInformation(
        private val binding: ItemOptionWithInfoBinding
    ):ViewHolder(binding.root){
        fun bind(item: TypeListView.ViewTypeWithFourSideInformation){
            with(binding){
                imageCheck.visibility = if(item.selected) View.VISIBLE else View.INVISIBLE
                textTopLeft.text = item.topLeftValue
                textBottomRight.text = item.bottomRightValue
                textBottomLeft.text = item.bottomLeftValue

                if(item.isDisable){
                    disableMode(binding)
                }else{
                    enableMode(binding,item)
                }

                setComponentRightContent(itemView.context,binding, item.topRightValue)

                if (layoutPosition == currentList.size) {
                    divider.visibility = View.INVISIBLE
                } else {
                    divider.visibility = View.VISIBLE
                }


                itemView.setOnClickListener{
                    if(isClickable.not() || item.isDisable){
                        return@setOnClickListener
                    }

                    if(isMultipleChoice){
                        updateWhenSelected(layoutPosition)
                        onMultipleOptionsSelected?.invoke(getMultipleSelectedOptions())
                    }else{
                        if(!item.selected){
                            updateWhenSelected(layoutPosition)
                            optionSelected?.invoke(item)
                        }
                    }
                }
            }
        }
    }

    private fun setComponentRightContent(context: Context,binding: ItemOptionWithInfoBinding, value: TopRightContent) {
        when(value) {
            is TopRightContent.Url->{
                binding.textTopRight.isVisible = false
                binding.imageTopRight.isVisible = true
                Glide.with(context)
                    .load(value.url)
                    .into(binding.imageTopRight)
            }
            is TopRightContent.DrawableResource->{
                binding.textTopRight.isVisible = false
                binding.imageTopRight.isVisible = true
                binding.imageTopRight.setImageDrawable(ContextCompat.getDrawable(context,value.drawable))
            }
            is TopRightContent.Text->{
                binding.textTopRight.isVisible = true
                binding.imageTopRight.isVisible = false
                binding.textTopRight.text = value.text
            }
        }
    }

    private fun updateWhenSelected(position:Int){
        val item = currentList[position]
        if(item is TypeListView.ViewTypeGeneral || item is TypeListView.ViewTypeWithFourSideInformation){
            if (isMultipleChoice) {
                // Untuk multiple choice, toggle status selected
                item.selected = !item.selected
                notifyItemChanged(position)
            } else {
                // Untuk single choice, cari item lama yang sudah di select dan ubah
                val positionOldSelected = currentList.indexOfFirst { it.selected }
                if (positionOldSelected != -1 && positionOldSelected != position) {
                    val itemOld = currentList[positionOldSelected]
                    itemOld.selected = false
                    notifyItemChanged(positionOldSelected)
                }

                // Atur item baru menjadi selected
                item.selected = true
                notifyItemChanged(position)
            }
        }
    }

    fun getMultipleSelectedOptions(): List<TypeListView> {
        return if(isMultipleChoice){
            val items = currentList.filterIsInstance<TypeListView>()
            items.filter { it.selected }
        }else{
            emptyList()
        }
    }

    fun getSelectedOption():TypeListView?{
        return if(!isMultipleChoice){
            val items = currentList.filterIsInstance<TypeListView>()
            val selected = items.filter { it.selected }
            selected[0]
        }else{
            null
        }
    }
    companion object{
        const val VIEW_TYPE_GENERAL = 0
        const val VIEW_TYPE_WITH_INFORMATION_FOUR_SIDE = 1

        val listItemDiffCallback = object : DiffUtil.ItemCallback<TypeListView>(){
            override fun areItemsTheSame(oldItem: TypeListView, newItem: TypeListView): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: TypeListView, newItem: TypeListView): Boolean {
                return oldItem == newItem
            }
        }
    }
}