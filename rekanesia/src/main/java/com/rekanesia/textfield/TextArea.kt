package com.rekanesia.textfield

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputFilter
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import com.rekanesia.R
import com.rekanesia.databinding.LayoutTextAreaBinding
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper

class TextArea : ConstraintLayout {

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    private lateinit var binding: LayoutTextAreaBinding

    var style = Rekan
        set(value) {
            field = value
            setStyle()
        }

    var text: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            binding.input.setText(field)
        }
        get() = binding.input.text.toString()

    var label: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            binding.tvLabel.text = field
        }

    val input: EditText get() = binding.input

    var errorMessage: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
        }

    var isError = false
        set(value) {
            field = value
            if (hasFocus) {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setHasFocusBackgroundEditText()
                }
            } else {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setNormalBackgroundEditText()
                }
            }
        }

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.mandatory.isVisible = field
        }

    private var hasFocus = false
    private var showCounter = false

    @SuppressLint("SetTextI18n")
    private fun init(attrs: AttributeSet) {
        binding = LayoutTextAreaBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.TextArea, 0, 0)
        isMandatory = typedArray.getBoolean(R.styleable.TextArea_rekan_mandatory, false)
        val maxLength = typedArray.getInt(R.styleable.TextArea_rekan_maxLength, 240)
        val hint = typedArray.getString(R.styleable.TextArea_rekan_hint).orEmpty()
        label = typedArray.getString(R.styleable.TextArea_rekan_label).orEmpty()
        text = typedArray.getString(R.styleable.TextArea_rekan_text).orEmpty()
        showCounter = typedArray.getBoolean(R.styleable.TextArea_rekan_showCounter, true)
        errorMessage = typedArray.getString(R.styleable.TextArea_rekan_errorMessage).orEmpty()
        style = values()[typedArray.getInt(R.styleable.TextArea_rekan_styles, 1) - 1]

        setConstrainConfig()
        setStyle()

        setLabelAndMandatory(label!!, isMandatory)

        setTextAndHint(text, hint)

        setHelperAndCounter(maxLength)

        setMaxLengthText(maxLength)

        setBackgroundOnFocusChange()

        if (showCounter) {
            binding.tvHelper.text = "Sisa $maxLength huruf"
        }

        typedArray.recycle()
    }

    private fun setConstrainConfig() {
        binding.input.id = View.generateViewId()

        // Parent ConstraintLayout
        val parentLayout = binding.root as ConstraintLayout

        // Initialize ConstraintSet
        val constraintSet = ConstraintSet()
        constraintSet.clone(parentLayout)

        // Update constraints for tv_helper
        constraintSet.connect(
            binding.tvHelper.id,        // ID tv_helper
            ConstraintSet.BOTTOM,       // Bottom of tv_helper
            binding.input.id,           // Anchor to input's Bottom
            ConstraintSet.BOTTOM
        )

        // Update constraints for tv_error_message
        constraintSet.connect(
            binding.tvErrorMessage.id,  // ID tv_error_message
            ConstraintSet.TOP,          // Top of tv_error_message
            binding.input.id,           // Anchor to input's Bottom
            ConstraintSet.BOTTOM
        )

        // Apply updated constraints
        constraintSet.applyTo(parentLayout)
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    mandatory.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    mandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))
                    mandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    input.setBackgroundResource(R.drawable.bg_input)
                    input.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    input.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    input.setHintTextColor(ContextCompat.getColor(context, R.color.D500))

                    tvHelper.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))

                    tvErrorMessage.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                }
                Ipubers -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

                    mandatory.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    mandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))
                    mandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

                    input.setBackgroundResource(R.drawable.bg_input_psp)
                    input.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    input.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    input.setHintTextColor(ContextCompat.getColor(context, R.color.M500))

                    tvHelper.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))

                    tvErrorMessage.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }
            }
        }
    }

    private fun setMaxLengthText(maxLength: Int) {
        if (maxLength != 0) {
            binding.input.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
    }

    private fun setLabelAndMandatory(label: String, isMandatory: Boolean) {
        binding.apply {
            if (label.isNotEmpty()) {
                tvLabel.isVisible = true
            } else {
                val params: MarginLayoutParams = binding.input.layoutParams as MarginLayoutParams
                TextFieldHelper.setMargins(
                    binding.input,
                    params.leftMargin,
                    0,
                    params.rightMargin,
                    params.bottomMargin
                )
                tvLabel.isVisible = false
            }
            tvLabel.text = label
            mandatory.isVisible = isMandatory
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setHelperAndCounter(maxCounter: Int) {
        binding.apply {
            tvHelper.isVisible = showCounter
            if (showCounter) {
                tvHelper.text = "Sisa ${maxCounter - (text?.length ?: 0)} huruf"
                input.doOnTextChanged { text, _, _, _ ->
                    val textLength = text?.length ?: 0
                    if (textLength <= maxCounter) {
                        tvHelper.text = "Sisa ${maxCounter - (text?.length ?: 0)} huruf"
                    }
                }
            }
        }
    }

    private fun setTextAndHint(text: String?, hint: String) {
        binding.apply {
            if (!text.isNullOrEmpty()) {
                input.setText(text)
            }
            input.hint = hint
        }
    }

    private fun setBackgroundOnFocusChange() {
        var textLength = 0
        binding.input.doOnTextChanged { text, _, _, _ ->
            textLength = text.toString().length
        }

        binding.input.setOnFocusChangeListener { _, hasFocus ->
            this.hasFocus = hasFocus
            onFocusChange(textLength)
        }
    }

    private fun onFocusChange(textLength: Int) {
        if (hasFocus) {
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setHasFocusBackgroundEditText()
            }
            if (textLength >= 1) {
                input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    ContextCompat.getDrawable(context, R.drawable.ic_close),
                    null,
                )
                TextFieldHelper.setOnClickClear(input)
            } else {
                input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    null,
                )
            }
        } else {
            binding.input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                0,
                0
            )
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setNormalBackgroundEditText()
            }
        }
    }

    private fun setErrorBackgroundEditText() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    input.setBackgroundResource(R.drawable.bg_input_error)
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    input.setBackgroundResource(R.drawable.bg_input_psp_error)
                }
            }
            tvErrorMessage.isVisible = !(errorMessage.isNullOrEmpty() || errorMessage == "null") && isError
            tvErrorMessage.text = errorMessage
            tvHelper.isVisible = showCounter
        }
    }

    private fun setHasFocusBackgroundEditText() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.B400))
                    input.setBackgroundResource(R.drawable.bg_input)
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.L400))
                    input.setBackgroundResource(R.drawable.bg_input_psp)
                }
            }
            tvHelper.isVisible = showCounter
            tvErrorMessage.isVisible = isError
        }
    }

    private fun setNormalBackgroundEditText() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    input.setBackgroundResource(R.drawable.bg_input)
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    input.setBackgroundResource(R.drawable.bg_input_psp)
                }
            }
            tvHelper.isVisible = showCounter
            tvErrorMessage.isVisible = isError
        }
    }
}