package com.rekanesia.textfield

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.content.res.getStringOrThrow
import androidx.core.view.isVisible
import com.rekanesia.R
import com.rekanesia.databinding.LayoutChooseBinding
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper

class Choose : ConstraintLayout {

    private lateinit var binding: LayoutChooseBinding

    var style = Rekan
        set(value) {
            field = value
            setStyle()
        }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    var text: String? = ""
        get() = binding.tvChoose.text.toString()
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            binding.tvChoose.text = field
        }

    var label: String = ""
        set(value) {
            field = value
            binding.tvLabel.text = field
        }

    private var errorMessage: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
        }

    var isError = false
        set(value) {
            field = value
            if (field) {
                setErrorBackgroundEditText()
            } else {
                setNormalBackgroundEditText()
            }
        }

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.mandatory.isVisible = field
        }

    private var helper = ""

    var setEnable:Boolean = true
        set(value){
            field = value
            binding.layoutChoose.isEnabled = value
        }

    fun setOnClickListener(onClick: (() -> Unit)) {
        binding.layoutChoose.setOnClickListener {
            onClick.invoke()
        }
    }

    fun setTextChooseColor(textColor: Int) {
        if (textColor != -1) {
            binding.tvChoose.setTextColor(ContextCompat.getColor(context, textColor))
        }
    }

    fun setSpannableText(text: SpannableStringBuilder?) {
        val mText = text
        if (mText.isNullOrEmpty() || mText.toString() == "null") {
            binding.tvChoose.text = ""
        } else {
            binding.tvChoose.text = mText
        }
    }

    private fun init(attrs: AttributeSet) {
        binding = LayoutChooseBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.Choose, 0, 0)
        isMandatory = typedArray.getBoolean(R.styleable.Choose_rekan_mandatory, false)
        val iconStart = typedArray.getResourceId(R.styleable.Choose_rekan_iconStart, -1)
        val iconEnd = typedArray.getResourceId(R.styleable.Choose_rekan_iconEnd, -1)
        val textInputColor = typedArray.getColor(R.styleable.Choose_rekan_textInputColor, -1)
        val iconStartColor = typedArray.getColor(R.styleable.Choose_rekan_iconStartTint, -1)
        val iconEndColor = typedArray.getColor(R.styleable.Choose_rekan_iconEndTint, -1)
        style = values()[typedArray.getInt(R.styleable.Choose_rekan_styles, 1) - 1]
        label = typedArray.getString(R.styleable.Choose_rekan_label).orEmpty()
        helper = typedArray.getString(R.styleable.Choose_rekan_helper).orEmpty()
        text = typedArray.getStringOrThrow(R.styleable.Choose_rekan_text)
        isError = typedArray.getBoolean(R.styleable.Choose_rekan_isError, false)
        errorMessage = typedArray.getString(R.styleable.Choose_rekan_errorMessage).orEmpty()

        setStyle()

        setTextChoose(text)

        setLabelAndMandatory(label, isMandatory)

        setHelper(helper)

        setTextChooseColor(textInputColor)

        setIcon(iconStart, iconEnd)

        setIconEndTint(iconEndColor)

        setIconStartTint(iconStartColor)

        typedArray.recycle()
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)

                    mandatory.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    mandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    mandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))

                    layoutChoose.setBackgroundResource(R.drawable.bg_input_choose)
                    tvChoose.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    tvChoose.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    tvChoose.setTextColor(ContextCompat.getColor(context, R.color.D500))

                    tvHelper.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    tvHelper.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
                }
                Ipubers -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

                    mandatory.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    mandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)
                    mandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))

                    layoutChoose.setBackgroundResource(R.drawable.bg_input_psp_choose)
                    tvChoose.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    tvChoose.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)
                    tvChoose.setTextColor(ContextCompat.getColor(context, R.color.M500))

                    tvHelper.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    tvHelper.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))
                }
            }


        }
    }

    private fun setTextChoose(text: String?) {
        binding.apply {
            if (!text.isNullOrEmpty()) {
                tvChoose.text = text
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setHelper(helper: String) {
        binding.apply {
            tvHelper.isVisible = helper.isNotEmpty()
            tvHelper.text = helper
        }
    }

    private fun setLabelAndMandatory(label: String, isMandatory: Boolean) {
        binding.apply {
            if (label.isNotEmpty()) {
                tvLabel.isVisible = true
            } else {
                val params: MarginLayoutParams = binding.layoutChoose.layoutParams as MarginLayoutParams
                TextFieldHelper.setMargins(
                    binding.layoutChoose,
                    params.leftMargin,
                    0,
                    params.rightMargin,
                    params.bottomMargin
                )
                tvLabel.isVisible = false
            }
            tvLabel.text = label
            mandatory.isVisible = isMandatory
        }
    }

    private fun setIcon(iconStart: Int, iconEnd: Int) {
        binding.apply {
            if (iconStart != -1) {
                imgChooseStart.setImageDrawable(ContextCompat.getDrawable(context, iconStart))
                imgChooseStart.isVisible = true
            } else {
                imgChooseStart.isVisible = false
            }

            if (iconEnd != -1) {
                imgChooseEnd.setImageDrawable(ContextCompat.getDrawable(context, iconEnd))
                imgChooseEnd.isVisible = true
            } else {
                imgChooseEnd.isVisible = false
            }
        }
    }

    fun setIconStartTint(color: Int) {
        if (color != -1) {
            binding.imgChooseStart.imageTintList = ColorStateList.valueOf(color)
        }
    }

    fun setIconEndTint(color: Int) {
        if (color != -1) {
            binding.imgChooseEnd.imageTintList = ColorStateList.valueOf(color)
        }
    }

    fun setIconStart(@DrawableRes iconStart: Int?) {
        binding.apply {
            if (iconStart != null) {
                imgChooseStart.setImageResource(iconStart)
                imgChooseStart.isVisible = true
            } else {
                imgChooseStart.isVisible = false
            }
        }
    }

    fun setIconStart(bitmap: Bitmap?) {
        binding.apply {
            if (bitmap != null) {
                imgChooseStart.setImageBitmap(bitmap)
                imgChooseStart.isVisible = true
            } else {
                imgChooseStart.isVisible = false
            }
        }
    }

    fun setIconEnd(@DrawableRes iconEnd: Int?) {
        binding.apply {
            if (iconEnd != null) {
                imgChooseEnd.setImageResource(iconEnd)
                imgChooseEnd.isVisible = true
            } else {
                imgChooseEnd.isVisible = false
            }
        }
    }

    fun setIconEnd(bitmap: Bitmap?) {
        binding.apply {
            if (bitmap != null) {
                imgChooseEnd.setImageBitmap(bitmap)
                imgChooseEnd.isVisible = true
            } else {
                imgChooseEnd.isVisible = false
            }
        }
    }

    private fun setErrorBackgroundEditText() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    layoutChoose.setBackgroundResource(R.drawable.bg_input_choose_error)
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    layoutChoose.setBackgroundResource(R.drawable.bg_input_psp_choose_error)
                }
            }
            if ((errorMessage.isNullOrEmpty() || errorMessage == "null") && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = helper
            } else if (!errorMessage.isNullOrEmpty() && helper.isEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage.toString()
            } else if (!errorMessage.isNullOrEmpty() && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage
            }
        }
    }

    private fun setNormalBackgroundEditText() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    layoutChoose.setBackgroundResource(R.drawable.bg_input_choose)
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    layoutChoose.setBackgroundResource(R.drawable.bg_input_psp_choose)
                }
            }
            tvHelper.isVisible = helper.isNotEmpty()
            tvHelper.text = helper
        }
    }
}