package com.rekanesia.textfield

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.doOnTextChanged
import com.rekanesia.R
import com.rekanesia.databinding.LayoutSearchBoxBinding
import com.rekanesia.helper.GeneralUtils.addDelimiter
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import java.util.*
import kotlin.concurrent.schedule

class SearchBox(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    private var binding: LayoutSearchBoxBinding

    private var style = Rekan

    var querySearch: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            if (inputType == InputTypeRekanesia.Currency) {
                binding.input.setText(field?.addDelimiter())
            } else {
                binding.input.setText(field)
            }

        }
        get() = when(inputType){
            InputTypeRekanesia.Currency->{
                binding.input.text.toString().removeDelimiter()
            }
            else->{
                binding.input.text.toString().ifEmpty { null }
            }
        }

    private var onQueryChange: ((String) -> Unit)? = null
    fun onQueryChangeListener(onSearchQueryChange: ((query: String) -> Unit)) {
        onQueryChange = onSearchQueryChange
    }

    val input: EditText get() = binding.input

    private var onIconEndClick: (() -> Unit)? = null
    fun setOnIconEndClickListener(onClick: () -> Unit) {
        onIconEndClick = onClick
    }

    private var iconVisibility = true
    private var iconEnd = -1
    fun setVisibilityIconEnd(isVisible: Boolean) {
        if (iconEnd != -1) {
            iconVisibility = isVisible
            showIconEnd(isVisible)
        }
    }

    private var inputType = InputTypeRekanesia.Number

    private var bgSearchBox: Int? = null
    fun setBackgroundSearchBox(resId: Int){
        bgSearchBox = resId
        binding.input.setBackgroundResource(resId)
    }

    init {
        binding = LayoutSearchBoxBinding.inflate(LayoutInflater.from(context), this)

        orientation = VERTICAL
        gravity = Gravity.CENTER


        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.SearchBox, 0, 0)
        val placeholder = typedArray.getString(R.styleable.SearchBox_rekan_hint)
        val maxLength = typedArray.getInt(R.styleable.SearchBox_rekan_maxLength, 0)
        style = values()[typedArray.getInt(R.styleable.SearchBox_rekan_styles, 1) - 1]
        iconEnd = typedArray.getResourceId(R.styleable.SearchBox_rekan_iconEnd, -1)
        querySearch = typedArray.getString(R.styleable.SearchBox_rekan_text).orEmpty()
        inputType =
            InputTypeRekanesia.values()[typedArray.getInt(
                R.styleable.SearchBox_rekan_setInputType,
                7
            ) - 1]

        val defaultBackground = when (style) {
            Rekan -> R.drawable.bg_input
            Ipubers -> R.drawable.bg_search_box_psp
        }
        bgSearchBox = typedArray.getResourceId(R.styleable.SearchBox_rekan_background, defaultBackground)

        binding.input.hint = placeholder
        binding.input.setText(querySearch)
        typedArray.recycle()

        setStyle()

        TextFieldHelper.setInputType(binding.input, inputType)

        setMaxLengthText(maxLength)

        setIconEnd(querySearch?.length ?: 0)

        binding.input.addTextChangedListener(object : TextWatcher {
            var timer = Timer()

            override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                timer.cancel()
                val sleep = when (s?.length) {
                    1 -> 1000L
                    2, 3 -> 700L
                    4, 5 -> 500L
                    else -> 300L
                }
                timer = Timer()
                timer.schedule(sleep) {
                    Handler(context.mainLooper).post { onQueryChange?.invoke(binding.input.text.toString()) }
                }
            }
        })
    }

    private fun setStyle() {
        binding.apply {
            bgSearchBox?.let { input.setBackgroundResource(bgSearchBox!!) }
            when (style) {
                Rekan -> {
                    input.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    input.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    input.setHintTextColor(ContextCompat.getColor(context, R.color.D500))
                    input.setTextColor(ContextCompat.getColor(context, R.color.D800))
                }
                Ipubers -> {
                    input.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    input.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    input.setHintTextColor(ContextCompat.getColor(context, R.color.M500))
                    input.setTextColor(ContextCompat.getColor(context, R.color.M800))
                }
            }
        }
    }

    private fun setMaxLengthText(maxLength: Int) {
        if (maxLength != 0) {
            binding.input.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setIconEnd(queryTextLength: Int) {
        binding.apply {
            var isFocus = false
            var textLength = queryTextLength
            showIconClear(false)
            if (iconEnd != -1) {
                showIconEnd(iconVisibility)
                input.setOnTouchListener { _, motionEvent ->
                    try {
                        val drawableEnd = 2
                        val isIconEndClicked =
                            motionEvent.rawX >= (input.right - input.compoundDrawables[drawableEnd].bounds.width())
                        if (motionEvent.action == MotionEvent.ACTION_UP) {
                            if (isIconEndClicked) {
                                onIconEndClick?.invoke()
                                return@setOnTouchListener true
                            }
                        }
                        false
                    } catch (e: Exception) {
                        e.printStackTrace()
                        false
                    }
                }
            }

            input.doOnTextChanged { text, _, _, _ ->
                textLength = text?.length ?: 0
                if (iconEnd != -1) {
                    showIconEnd(iconVisibility)
                } else {
                    if (isFocus) {
                        if (textLength >= 1) {
                            showIconClear(true)
                            TextFieldHelper.setOnClickClear(input)
                        } else {
                            showIconClear(false)
                        }
                    } else {
                        showIconClear(false)
                    }
                }
            }

            input.setOnFocusChangeListener { _, hasFocus ->
                isFocus = hasFocus
                if (isFocus) {
                    if (iconEnd != -1) {
                        showIconEnd(iconVisibility)
                    } else {
                        if (textLength >= 1) {
                            showIconClear(true)
                            TextFieldHelper.setOnClickClear(input)
                        }
                    }
                } else {
                    if (iconEnd != -1) {
                        showIconEnd(iconVisibility)
                    } else {
                        showIconClear(false)
                    }
                }
            }
        }
    }

    private fun showIconClear(isVisible: Boolean) {
        binding.apply {
            if (isVisible) {
                input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    ContextCompat.getDrawable(context, R.drawable.ic_search),
                    null,
                    ContextCompat.getDrawable(context, R.drawable.ic_close),
                    null
                )
            } else {
                showIconEnd(false)
            }
        }
    }

    private fun showIconEnd(isVisible: Boolean) {
        if (isVisible) {
            binding.input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, R.drawable.ic_search),
                null,
                ContextCompat.getDrawable(context, iconEnd),
                null
            )
        } else {
            binding.input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, R.drawable.ic_search),
                null,
                null,
                null
            )
        }
    }
}