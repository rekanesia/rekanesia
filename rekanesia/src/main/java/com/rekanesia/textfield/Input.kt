package com.rekanesia.textfield

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.text.InputFilter
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import com.rekanesia.R
import com.rekanesia.databinding.LayoutInputBinding
import com.rekanesia.helper.DelimiterInputWatcher
import com.rekanesia.helper.GeneralUtils.addDelimiter
import com.rekanesia.helper.GeneralUtils.clearTextValue
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import com.rekanesia.helper.TextFieldHelper.setOnClickClear
import java.util.*

@SuppressLint("ClickableViewAccessibility")
class Input : ConstraintLayout {

    private lateinit var binding: LayoutInputBinding

    private var textWatcher: DelimiterInputWatcher? = null
    private var locale: Locale = Locale.getDefault()
    private lateinit var onDrawableClick:OnDrawableClickListener
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    var text: String? = ""
        @SuppressLint("SetTextI18n")
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            if (inputType == InputTypeRekanesia.Currency) {
                if (prefix.isNotEmpty() || suffix.isNotEmpty()) {
                    binding.input.setText("$prefix${field?.addDelimiter()}$suffix")
                } else {
                    binding.input.setText(field?.addDelimiter())
                }
            } else {
                if (prefix.isNotEmpty() || suffix.isNotEmpty()) {
                    binding.input.setText("$prefix$field$suffix")
                } else {
                    binding.input.setText(field)
                }
            }

        }
        get() = when(inputType){
            InputTypeRekanesia.Currency->{
                binding.input.text.toString().removeDelimiter()
            }
            else->{
                clearTextValue(binding.input.text.toString(),prefix,suffix)
            }
        }

    var label: String = ""
        set(value) {
            field = value
            binding.tvLabel.text = field
        }

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.mandatory.isVisible = field
        }

    var stateEnable:Boolean = true
        set(value) {
            field = value
            binding.input.isEnabled = value
            binding.input.isFocusable = value
            binding.input.isFocusableInTouchMode = value
            if(value){
                binding.input.setHintTextColor(ContextCompat.getColor(context,R.color.D500))
            }else{
                binding.input.setHintTextColor(ContextCompat.getColor(context,R.color.D400))
            }
        }

    val input: EditText get() = binding.input

    private var inputType = InputTypeRekanesia.Number

    private var helper = ""
    private var hasFocus = false
    private var useCounter = false
    private var visibility = false

    var style = Rekan
        set(value) {
            field = value
            setStyle()
        }

    private var errorMessage: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
        }

    var isError = false
        set(value) {
            field = value
            if (hasFocus) {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setHasFocusBackgroundEditText()
                }
            } else {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setNormalBackgroundEditText()
                }
            }
        }

    var prefix = ""
    var suffix = ""

    private fun init(attrs: AttributeSet) {
        binding = LayoutInputBinding.inflate(LayoutInflater.from(context), this)
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.Input, 0, 0)
        isMandatory = typedArray.getBoolean(R.styleable.Input_rekan_mandatory, false)
        val placeholder = typedArray.getString(R.styleable.Input_rekan_hint).orEmpty()
        val maxCounter = typedArray.getInt(R.styleable.Input_rekan_maxCounter, 140)
        val maxLength = typedArray.getInt(R.styleable.Input_rekan_maxLength, 0)
        val iconStart = typedArray.getResourceId(R.styleable.Input_rekan_iconStart, -1)
        val iconEnd = typedArray.getResourceId(R.styleable.Input_rekan_iconEnd, -1)
        val textColor = typedArray.getColor(R.styleable.Input_rekan_textInputColor, -1)
        style = values()[typedArray.getInt(R.styleable.Input_rekan_styles, 1) - 1]
        label = typedArray.getString(R.styleable.Input_rekan_label).orEmpty()
        text = typedArray.getString(R.styleable.Input_rekan_textInput).orEmpty()
        helper = typedArray.getString(R.styleable.Input_rekan_helper).orEmpty()
        useCounter = typedArray.getBoolean(R.styleable.Input_rekan_showCounter, false)
        inputType =
            InputTypeRekanesia.values()[typedArray.getInt(
                R.styleable.Input_rekan_setInputType,
                7
            ) - 1]
        isError = typedArray.getBoolean(R.styleable.Input_rekan_isError, false)
        errorMessage = typedArray.getString(R.styleable.Input_rekan_errorMessage).orEmpty()
        prefix = typedArray.getString(R.styleable.Input_rekan_prefixSymbol).orEmpty()
        suffix = typedArray.getString(R.styleable.Input_rekan_suffixSymbol).orEmpty()

        setConstrainConfig()

        setStyle()

        setLabelAndMandatory(label, isMandatory)

        setTextAndHint(text, placeholder)

        TextFieldHelper.setInputType(binding.input, inputType)

        setMaxLengthText(maxLength)

        setHelperAndCounter(helper, maxCounter)

        onFocusChangeAndSetIcon(iconStart, iconEnd)

        setTextInputColor(textColor)

        changeTextListener()

        typedArray.recycle()
    }

    /**
     * untuk mencari ulang id dynamic yang sudah di generate ulang
     */
    private fun setConstrainConfig() {
        binding.input.id = View.generateViewId()

        val parentLayout = binding.root as ConstraintLayout

        val constraintSet = ConstraintSet()
        constraintSet.clone(parentLayout)

        constraintSet.connect(
            binding.tvHelper.id,
            ConstraintSet.TOP,
            binding.input.id,
            ConstraintSet.BOTTOM,
        )

        constraintSet.connect(
            binding.tvCounter.id,        // ID of the target view
            ConstraintSet.TOP,           // Target's side
            binding.input.id,            // ID of the anchor view
            ConstraintSet.BOTTOM         // Anchor's side
        )

        constraintSet.applyTo(parentLayout)
    }

    private fun setStyle() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))

                    mandatory.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    mandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    mandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))

                    tvHelper.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    tvHelper.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))

                    tvCounter.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    tvCounter.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                    tvCounter.setTextColor(ContextCompat.getColor(context, R.color.D500))

                    input.setBackgroundResource(R.drawable.bg_input)
                    input.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    input.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    input.setHintTextColor(ContextCompat.getColor(context, R.color.D500))
                    input.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                }
                Ipubers -> {
                    tvLabel.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    tvLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))

                    mandatory.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    mandatory.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)
                    mandatory.setTextColor(ContextCompat.getColor(context, R.color.R400))

                    tvHelper.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    tvHelper.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))

                    tvCounter.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    tvCounter.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
                    tvCounter.setTextColor(ContextCompat.getColor(context, R.color.M500))

                    input.setBackgroundResource(R.drawable.bg_input_psp)
                    input.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                    input.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    input.setHintTextColor(ContextCompat.getColor(context, R.color.M500))
                    input.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                }
            }
        }
    }

    private fun changeTextListener() {
        when(inputType){
            InputTypeRekanesia.Currency->{
                input.addTextChangedListener { s->
                    textListener?.textChangeListener(s.toString().removeDelimiter())
                }
            }
            else->{
                input.addTextChangedListener { s->
                    textListener?.textChangeListener(clearTextValue(s.toString(),prefix,suffix))
                }
            }
        }
    }

    private fun setTextAndHint(text: String?, hint: String) {
        binding.apply {
            if (!text.isNullOrEmpty()) {
                input.setText(text)
            }
            input.hint = hint
        }
    }

    private fun setLabelAndMandatory(label: String, isMandatory: Boolean) {
        binding.apply {
            if (label.isNotEmpty()) {
                tvLabel.isVisible = true
            } else {
                val params: MarginLayoutParams = binding.input.layoutParams as MarginLayoutParams
                TextFieldHelper.setMargins(
                    binding.input,
                    params.leftMargin,
                    0,
                    params.rightMargin,
                    params.bottomMargin
                )
                tvLabel.isVisible = false
            }
            tvLabel.text = label
            mandatory.isVisible = isMandatory
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setHelperAndCounter(helper: String, maxCounter: Int) {
        binding.apply {
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            if (useCounter) {
                tvCounter.text = "Sisa ${maxCounter - (text?.length ?: 0)} huruf"
                input.doOnTextChanged { text, _, _, _ ->
                    val textLength = text?.length ?: 0
                    if (textLength <= maxCounter) {
                        tvCounter.text = "Sisa ${maxCounter - (text?.length ?: 0)} huruf"
                    }
                }
            }
        }
    }

    private fun setMaxLengthText(maxLength: Int) {
        if (maxLength != 0) {
            binding.input.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
    }

    private fun onFocusChangeAndSetIcon(iconStart: Int, iconEnd: Int) {
        binding.apply {
            var textLength = 0
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconStart != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        null,
                        null,
                    )
                }
            }

            input.doOnTextChanged { text, _, _, _ ->
                textLength = text.toString().length
                onFocusChange(iconStart, iconEnd, textLength, false)
            }

            input.setOnFocusChangeListener { _, hasFocus ->
                this@Input.hasFocus = hasFocus
                onFocusChange(iconStart, iconEnd, textLength, setDelimiter = true)
            }
        }
    }

    fun setTextInputColor(textColor: Int) {
        if (textColor != -1) {
            binding.input.setTextColor(ContextCompat.getColor(context, textColor))
        }
    }

    private fun onFocusChange(iconStart: Int, iconEnd: Int, textLength: Int, setDelimiter: Boolean) {
        if (hasFocus) {
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setHasFocusBackgroundEditText()
            }
            if (setDelimiter) {
                if (inputType == InputTypeRekanesia.Currency) {
                    textWatcher = DelimiterInputWatcher(binding.input, locale = locale, prefix = prefix, suffix = suffix)
                    input.removeTextChangedListener(textWatcher)
                    input.addTextChangedListener(textWatcher)
                }
            }
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                    setOnClickClear(input, onClickDrawableEnd = { onDrawableClick.onDrawableClick() } )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                    setOnClickClear(input, onClickDrawableEnd = { onDrawableClick.onDrawableClick() } )
                }
                iconStart != -1 -> {
                    if (textLength >= 1) {
                        when(inputType){
                            InputTypeRekanesia.TextPassword,InputTypeRekanesia.NumberPassword->{
                                input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    ContextCompat.getDrawable(context, iconStart),
                                    null,
                                    ContextCompat.getDrawable(context, if(visibility) R.drawable.ic_visibility else R.drawable.ic_visibility_off),
                                    null,
                                )
                                setOnClickClear(input, inputType) {
                                    visibility = !visibility
                                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                        ContextCompat.getDrawable(context, iconStart),
                                        null,
                                        ContextCompat.getDrawable(
                                            context,
                                            if (!visibility) R.drawable.ic_visibility else R.drawable.ic_visibility_off
                                        ),
                                        null,
                                    )
                                    when (inputType) {
                                        InputTypeRekanesia.TextPassword -> {
                                            if (!visibility) TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.Text
                                            ) else TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.TextPassword
                                            )
                                        }

                                        InputTypeRekanesia.NumberPassword -> {
                                            if (!visibility) TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.Number
                                            ) else TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.NumberPassword
                                            )
                                        }

                                        else -> Unit
                                    }
                                }
                            }
                            else->{
                                input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    ContextCompat.getDrawable(context, iconStart),
                                    null,
                                    ContextCompat.getDrawable(context, com.rekanesia.R.drawable.ic_close),
                                    null,
                                )
                                setOnClickClear(input)
                            }
                        }
                    } else {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            ContextCompat.getDrawable(context, iconStart),
                            null,
                            null,
                            null,
                        )
                    }
                }
                else -> {
                    if (textLength >= 1) {
                        when(inputType){
                            InputTypeRekanesia.TextPassword,InputTypeRekanesia.NumberPassword->{
                                input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    null,
                                    null,
                                    ContextCompat.getDrawable(context, if(visibility)R.drawable.ic_visibility else R.drawable.ic_visibility_off),
                                    null,
                                )
                                setOnClickClear(input,inputType){
                                    visibility = !visibility
                                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                        null,
                                        null,
                                        ContextCompat.getDrawable(context, if(!visibility)R.drawable.ic_visibility else R.drawable.ic_visibility_off),
                                        null,
                                    )
                                    when(inputType){
                                        InputTypeRekanesia.TextPassword->{
                                            if (!visibility) TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.Text
                                            ) else TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.TextPassword
                                            )
                                        }

                                        InputTypeRekanesia.NumberPassword->{
                                            if(!visibility) TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.Number
                                            ) else TextFieldHelper.setInputType(
                                                input,
                                                InputTypeRekanesia.NumberPassword
                                            )
                                        }

                                        else->Unit
                                    }
                                }
                            }
                            else->{
                                input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    null,
                                    null,
                                    ContextCompat.getDrawable(context, R.drawable.ic_close),
                                    null,
                                )
                                setOnClickClear(input)
                            }
                        }


                    } else {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            null,
                            null,
                        )
                    }
                }
            }
        } else {
            if (inputType == InputTypeRekanesia.Currency) {
                if (textWatcher != null) {
                    input.removeTextChangedListener(textWatcher)
                }
            }
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                    setOnClickClear(input, onClickDrawableEnd = { onDrawableClick.onDrawableClick() } )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                    setOnClickClear(input, onClickDrawableEnd = { onDrawableClick.onDrawableClick() } )
                }
                iconStart != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        null,
                        null,
                    )
                }
                else -> {
                    binding.input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        0,
                        0,
                        0,
                        0
                    )
                }
            }
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setNormalBackgroundEditText()
            }
        }
    }

    private fun setErrorBackgroundEditText() {
        binding.apply {
            if ((errorMessage.isNullOrEmpty() || errorMessage == "null") && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = helper
            } else if (!errorMessage.isNullOrEmpty() && helper.isEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage.toString()
            } else if (!errorMessage.isNullOrEmpty() && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage
            }
            tvCounter.isVisible = false
            when (style) {
                Rekan -> {
                    input.setBackgroundResource(R.drawable.bg_input_error)
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
                    input.setBackgroundResource(R.drawable.bg_input_psp_error)
                }
            }
        }
    }

    private fun setHasFocusBackgroundEditText() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.B400))
                    input.setBackgroundResource(R.drawable.bg_input)
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.L400))
                    input.setBackgroundResource(R.drawable.bg_input_psp)
                }
            }
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            tvHelper.text = helper
        }
    }

    private fun setNormalBackgroundEditText() {
        binding.apply {
            when (style) {
                Rekan -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
                    input.setBackgroundResource(R.drawable.bg_input)
                }
                Ipubers -> {
                    tvHelper.setTextColor(ContextCompat.getColor(context, R.color.M500))
                    tvLabel.setTextColor(ContextCompat.getColor(context, R.color.M800))
                    input.setBackgroundResource(R.drawable.bg_input_psp)
                }
            }
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            tvHelper.text = helper
        }
    }

    fun setOnDrawableClickListener(listener:OnDrawableClickListener){
        this.onDrawableClick = listener
    }

    private var textListener:OnTextListener?=null
    fun addTextChangedListener(listener:OnTextListener){
        textListener = listener
    }

    fun interface OnDrawableClickListener{
        fun onDrawableClick()
    }

    fun interface OnTextListener{
        fun textChangeListener(s: String?)
    }
}