package com.rekanesia.textfield

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputFilter
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.content.res.getStringOrThrow
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import com.rekanesia.R
import com.rekanesia.databinding.LayoutLeftBinding
import com.rekanesia.helper.DelimiterInputWatcher
import com.rekanesia.helper.GeneralUtils
import com.rekanesia.helper.GeneralUtils.addDelimiter
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import com.rekanesia.helper.parseMoneyValue
import java.util.*

class Left : ConstraintLayout {

    private lateinit var binding: LayoutLeftBinding

    private var textWatcher: DelimiterInputWatcher? = null
    private var locale: Locale = Locale.getDefault()

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    var text: String? = ""
        @SuppressLint("SetTextI18n")
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            if (inputType == InputTypeRekanesia.Currency) {
                if (prefix.isNotEmpty() || suffix.isNotEmpty()) {
                    binding.input.setText("$prefix${field?.addDelimiter()}$suffix")
                } else {
                    binding.input.setText(field?.addDelimiter())
                }
            } else {
                if (prefix.isNotEmpty() || suffix.isNotEmpty()) {
                    binding.input.setText("$prefix$field$suffix")
                } else {
                    binding.input.setText(field)
                }
            }

        }
        get() = when(inputType){
            InputTypeRekanesia.Currency->{
                parseMoneyValue(binding.input.text.toString(),",",prefix,suffix).removeDelimiter()
            }
            else->{
                GeneralUtils.clearTextValue(binding.input.text.toString(), prefix, suffix)
            }
        }

    var label: String = ""
        set(value) {
            field = value
            binding.tvLabel.text = field
        }

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.mandatory.isVisible = field
        }

    val input: EditText get() = binding.input

    var isError = false
        set(value) {
            field = value
            if (hasFocus) {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setHasFocusBackgroundEditText()
                }
            } else {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setNormalBackgroundEditText()
                }
            }
        }

    var stateEnable:Boolean = true
        set(value) {
            field = value
            binding.input.isEnabled = value
            if(value){
                binding.input.setHintTextColor(ContextCompat.getColor(context,R.color.D500))
            }else{
                binding.input.setHintTextColor(ContextCompat.getColor(context,R.color.D400))
            }
        }

    var prefix = ""
    var suffix = ""

    fun setTextInputColor(textColor: Int) {
        if (textColor != -1) {
            binding.input.setTextColor(ContextCompat.getColor(context, textColor))
        }
    }

    private var errorMessage: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
        }

    private var inputType = InputTypeRekanesia.Number

    private var helper = ""
    private var hasFocus = false
    private var useCounter = false

    private fun init(attrs: AttributeSet) {
        binding = LayoutLeftBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.Left, 0, 0)
        isMandatory = typedArray.getBoolean(R.styleable.Left_rekan_mandatory, false)
        val placeholder = typedArray.getString(R.styleable.Left_rekan_hint).orEmpty()
        val maxCounter = typedArray.getInt(R.styleable.Left_rekan_maxCounter, 140)
        val maxLength = typedArray.getInt(R.styleable.Left_rekan_maxLength, 0)
        val iconStart = typedArray.getResourceId(R.styleable.Left_rekan_iconStart, -1)
        val iconEnd = typedArray.getResourceId(R.styleable.Left_rekan_iconEnd, -1)
        val textLeft = typedArray.getStringOrThrow(R.styleable.Left_rekan_textLeft)
        val textInputColor = typedArray.getColor(R.styleable.Left_rekan_textInputColor, -1)
        label = typedArray.getString(R.styleable.Left_rekan_label).orEmpty()
        helper = typedArray.getString(R.styleable.Left_rekan_helper).orEmpty()
        text = typedArray.getString(R.styleable.Left_rekan_textInput).orEmpty()
        useCounter = typedArray.getBoolean(R.styleable.Left_rekan_showCounter, false)
        inputType =
            InputTypeRekanesia.values()[typedArray.getInt(
                R.styleable.Left_rekan_setInputType,
                7
            ) - 1]
        isError = typedArray.getBoolean(R.styleable.Left_rekan_isError, false)
        errorMessage = typedArray.getString(R.styleable.Left_rekan_errorMessage).orEmpty()
        prefix = typedArray.getString(R.styleable.Left_rekan_prefixSymbol).orEmpty()
        suffix = typedArray.getString(R.styleable.Left_rekan_suffixSymbol).orEmpty()

        setConstrainConfig()
        TextFieldHelper.setInputType(binding.input, inputType)

        setTextAndHint(text, placeholder, textLeft)

        setLabelAndMandatory(label, isMandatory)

        setHelperAndCounter(helper, maxCounter)

        setMaxLengthText(maxLength)

        onFocusChangeAndSetIcon(iconStart, iconEnd)

        setTextInputColor(textInputColor)

        changeTextListener()
        
        typedArray.recycle()
    }

    private var textListener: Input.OnTextListener?=null
    fun addTextChangedListener(listener: Input.OnTextListener){
        textListener = listener
    }

    /**
     * untuk mencari ulang id dynamic yang sudah di generate ulang
     */
    private fun setConstrainConfig() {
        binding.input.id = View.generateViewId()

        val parentLayout = binding.root as ConstraintLayout

        val constraintSet = ConstraintSet()
        constraintSet.clone(parentLayout)
        with(constraintSet){
            connect(binding.layoutLeft.id, ConstraintSet.END, input.id, ConstraintSet.START) // Start to End of input
            connect(binding.layoutLeft.id, ConstraintSet.TOP, input.id, ConstraintSet.TOP)  // Top to Top of input
            connect(binding.layoutLeft.id, ConstraintSet.BOTTOM, input.id, ConstraintSet.BOTTOM) // Bottom to Bottom of input

            connect(binding.tvHelper.id, ConstraintSet.TOP,binding.input.id, ConstraintSet.BOTTOM) // layout_constraintTop_toBottomOf
            connect(binding.tvCounter.id, ConstraintSet.TOP,binding.input.id, ConstraintSet.BOTTOM) // layout_constraintTop_toBottomOf

            applyTo(parentLayout)
        }
    }

    private fun changeTextListener() {
        when(inputType){
            InputTypeRekanesia.Currency->{
                input.addTextChangedListener { s->
                    textListener?.textChangeListener(parseMoneyValue(s.toString(),",",prefix,suffix))
                }
            }
            else->{
                input.addTextChangedListener { s->
                    textListener?.textChangeListener(
                        GeneralUtils.clearTextValue(
                            s.toString(),
                            prefix,
                            suffix
                        )
                    )
                }
            }
        }
    }

    private fun setTextAndHint(text: String?, hint: String, textLeft: String) {
        binding.apply {
            if (!text.isNullOrEmpty()) {
                input.setText(text)
            }
            input.hint = hint
            tvLeft.text = textLeft
        }
    }

    private fun setLabelAndMandatory(label: String, isMandatory: Boolean) {
        binding.apply {
            if (label.isNotEmpty()) {
                tvLabel.isVisible = true
            } else {
                val params: MarginLayoutParams = binding.input.layoutParams as MarginLayoutParams
                TextFieldHelper.setMargins(
                    binding.input,
                    params.leftMargin,
                    0,
                    params.rightMargin,
                    params.bottomMargin
                )
                tvLabel.isVisible = false
            }
            tvLabel.text = label
            mandatory.isVisible = isMandatory
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setHelperAndCounter(helper: String, maxCounter: Int) {
        binding.apply {
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            if (useCounter) {
                input.doOnTextChanged { text, _, _, _ ->
                    tvCounter.text = "Sisa ${maxCounter - (text?.length ?: 0)} huruf"
                }
            }
        }
    }

    private fun setMaxLengthText(maxLength: Int) {
        if (maxLength != 0) {
            binding.input.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
    }

    private fun onFocusChangeAndSetIcon(iconStart: Int, iconEnd: Int) {
        binding.apply {
            var textLength = 0
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconStart != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        null,
                        null,
                    )
                }
            }

            input.doOnTextChanged { text, _, _, _ ->
                textLength = text.toString().length
                onFocusChange(iconStart, iconEnd, textLength, false)
            }

            input.setOnFocusChangeListener { _, hasFocus ->
                this@Left.hasFocus = hasFocus
                onFocusChange(iconStart, iconEnd, textLength, true)
            }
        }
    }

    private fun onFocusChange(iconStart: Int, iconEnd: Int, textLength: Int, setDelimiter: Boolean) {
        if (hasFocus) {
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setHasFocusBackgroundEditText()
            }
            if (setDelimiter) {
                if (inputType == InputTypeRekanesia.Currency) {
                    textWatcher = DelimiterInputWatcher(binding.input, locale = locale, prefix = prefix, suffix = suffix)
                    input.removeTextChangedListener(textWatcher)
                    input.addTextChangedListener(textWatcher)
                }
            }
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconStart != -1 -> {
                    if (textLength >= 1) {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            ContextCompat.getDrawable(context, iconStart),
                            null,
                            ContextCompat.getDrawable(context, R.drawable.ic_close),
                            null,
                        )
                        TextFieldHelper.setOnClickClear(input)
                    } else {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            ContextCompat.getDrawable(context, iconStart),
                            null,
                            null,
                            null,
                        )
                    }
                }
                else -> {
                    if (textLength >= 1) {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            ContextCompat.getDrawable(context, R.drawable.ic_close),
                            null,
                        )
                        TextFieldHelper.setOnClickClear(input)
                    } else {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            null,
                            null,
                        )
                    }
                }
            }
        } else {
            if (inputType == InputTypeRekanesia.Currency) {
                if (textWatcher != null) {
                    input.removeTextChangedListener(textWatcher)
                }
            }
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconStart != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        null,
                        null,
                    )
                }
                else -> {
                    binding.input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        0,
                        0,
                        0,
                        0
                    )
                }
            }
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setNormalBackgroundEditText()
            }
        }
    }

    private fun setErrorBackgroundEditText() {
        binding.apply {
            tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
            tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
            tvLeft.setTextColor(ContextCompat.getColor(context, R.color.R500))
            input.setBackgroundResource(R.drawable.bg_input_left_input_error)
            layoutLeft.setBackgroundResource(R.drawable.bg_input_left_layout_left_error)
            if ((errorMessage.isNullOrEmpty() || errorMessage == "null") && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = helper
            } else if (!errorMessage.isNullOrEmpty() && helper.isEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage.toString()
            } else if (!errorMessage.isNullOrEmpty() && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage
            }
            tvCounter.isVisible = false
        }
    }

    private fun setHasFocusBackgroundEditText() {
        binding.apply {
            tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
            tvLabel.setTextColor(ContextCompat.getColor(context, R.color.B400))
            tvLeft.setTextColor(ContextCompat.getColor(context, R.color.B400))
            input.setBackgroundResource(R.drawable.bg_input_left_input)
            layoutLeft.setBackgroundResource(R.drawable.bg_input_left_layout_left_active)
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            tvHelper.text = helper
        }
    }

    private fun setNormalBackgroundEditText() {
        binding.apply {
            tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
            tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
            tvLeft.setTextColor(ContextCompat.getColor(context, R.color.D800))
            input.setBackgroundResource(R.drawable.bg_input_left_input)
            layoutLeft.setBackgroundResource(R.drawable.bg_input_left_layout_left)
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            tvHelper.text = helper
        }
    }
}