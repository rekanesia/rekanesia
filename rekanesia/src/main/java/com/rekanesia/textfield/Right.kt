package com.rekanesia.textfield

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputFilter
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.content.res.getStringOrThrow
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import com.rekanesia.R
import com.rekanesia.databinding.LayoutRightBinding
import com.rekanesia.helper.DelimiterInputWatcher
import com.rekanesia.helper.GeneralUtils
import com.rekanesia.helper.GeneralUtils.addDelimiter
import com.rekanesia.helper.InputTypeRekanesia
import com.rekanesia.helper.TextFieldHelper
import com.rekanesia.helper.TextFieldHelper.removeDelimiter
import com.rekanesia.helper.parseMoneyValue
import java.util.*

class Right : ConstraintLayout {

    private lateinit var binding: LayoutRightBinding

    private var textWatcher: DelimiterInputWatcher? = null
    private var locale: Locale = Locale.getDefault()

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    var text: String? = ""
        @SuppressLint("SetTextI18n")
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            if (inputType == InputTypeRekanesia.Currency) {
                if (prefix.isNotEmpty() || suffix.isNotEmpty()) {
                    binding.input.setText("$prefix${field?.addDelimiter()}$suffix")
                } else {
                    binding.input.setText(field?.addDelimiter())
                }
            } else {
                if (prefix.isNotEmpty() || suffix.isNotEmpty()) {
                    binding.input.setText("$prefix$field$suffix")
                } else {
                    binding.input.setText(field)
                }
            }
        }
        get() = when(inputType){
            InputTypeRekanesia.Currency->{
                parseMoneyValue(binding.input.text.toString(),",",prefix,suffix).removeDelimiter()
            }
            else->{
                GeneralUtils.clearTextValue(binding.input.text.toString(), prefix, suffix)
            }
        }

    var textRight: String = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            binding.tvRight.text = field
        }
        get() = binding.tvRight.text.toString()

    var label: String = ""
        set(value) {
            field = value
            binding.tvLabel.text = field
        }

    var isMandatory: Boolean = false
        set(value) {
            field = value
            binding.mandatory.isVisible = field
        }

    val input: EditText get() = binding.input

    private var errorMessage: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
        }

    var isError = false
        set(value) {
            field = value
            if (hasFocus) {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setHasFocusBackgroundEditText()
                }
            } else {
                if (field) {
                    setErrorBackgroundEditText()
                } else {
                    setNormalBackgroundEditText()
                }
            }
        }

    var stateEnable:Boolean = true
        set(value) {
            field = value
            binding.input.isEnabled = value
            if(value){
                binding.input.setHintTextColor(ContextCompat.getColor(context,R.color.D500))
            }else{
                binding.input.setHintTextColor(ContextCompat.getColor(context,R.color.D400))
            }
        }

    var prefix = ""
    var suffix = ""

    private var inputType = InputTypeRekanesia.Number

    private var helper = ""
    private var hasFocus = false
    private var useCounter = false

    private fun init(attrs: AttributeSet) {
        binding = LayoutRightBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.Right, 0, 0)
        isMandatory = typedArray.getBoolean(R.styleable.Right_rekan_mandatory, false)
        val placeholder = typedArray.getString(R.styleable.Right_rekan_hint).orEmpty()
        val maxCounter = typedArray.getInt(R.styleable.Right_rekan_maxCounter, 140)
        val maxLength = typedArray.getInt(R.styleable.Right_rekan_maxLength, 0)
        val iconStart = typedArray.getResourceId(R.styleable.Right_rekan_iconStart, -1)
        val iconEnd = typedArray.getResourceId(R.styleable.Right_rekan_iconEnd, -1)
        textRight = typedArray.getStringOrThrow(R.styleable.Right_rekan_textRight)
        val iconChooseRight = typedArray.getResourceId(R.styleable.Right_rekan_iconChooseRight, -1)
        val textInputColor = typedArray.getColor(R.styleable.Right_rekan_textInputColor, -1)
        label = typedArray.getString(R.styleable.Right_rekan_label).orEmpty()
        helper = typedArray.getString(R.styleable.Right_rekan_helper).orEmpty()
        text = typedArray.getString(R.styleable.Right_rekan_textInput).orEmpty()
        useCounter = typedArray.getBoolean(R.styleable.Right_rekan_showCounter, false)
        inputType =
            InputTypeRekanesia.values()[typedArray.getInt(
                R.styleable.Right_rekan_setInputType,
                7
            ) - 1]
        isError = typedArray.getBoolean(R.styleable.Right_rekan_isError, false)
        errorMessage = typedArray.getString(R.styleable.Right_rekan_errorMessage).orEmpty()
        prefix = typedArray.getString(R.styleable.Right_rekan_prefixSymbol).orEmpty()
        suffix = typedArray.getString(R.styleable.Right_rekan_suffixSymbol).orEmpty()

        setConstrainConfig()

        TextFieldHelper.setInputType(binding.input, inputType)

        setTextAndHint(text, placeholder, textRight)

        setLabelAndMandatory(label, isMandatory)

        setHelperAndCounter(helper, maxCounter)

        setMaxLengthText(maxLength)

        onFocusChangeAndSetIcon(iconStart, iconEnd, iconChooseRight)

        setTextInputColor(textInputColor)

        changeTextListener()

        typedArray.recycle()
    }

    /**
     * untuk mencari ulang id dynamic yang sudah di generate ulang
     */
    private fun setConstrainConfig() {
        binding.input.id = View.generateViewId()

        val parentLayout = binding.root as ConstraintLayout

        val constraintSet = ConstraintSet()
        constraintSet.clone(parentLayout)
        with(constraintSet){
            connect(binding.layoutRight.id, ConstraintSet.START, input.id, ConstraintSet.END) // Start to End of input
            connect(binding.layoutRight.id, ConstraintSet.TOP, input.id, ConstraintSet.TOP)  // Top to Top of input
            connect(binding.layoutRight.id, ConstraintSet.BOTTOM, input.id, ConstraintSet.BOTTOM) // Bottom to Bottom of input

            connect(binding.tvHelper.id,ConstraintSet.TOP,binding.input.id, ConstraintSet.BOTTOM) // layout_constraintTop_toBottomOf
            connect(binding.tvCounter.id,ConstraintSet.TOP,binding.input.id, ConstraintSet.BOTTOM) // layout_constraintTop_toBottomOf

            applyTo(parentLayout)
        }
    }

    private var textListener: Input.OnTextListener?=null
    fun addTextChangedListener(listener: Input.OnTextListener){
        textListener = listener
    }
    private fun changeTextListener() {
        when(inputType){
            InputTypeRekanesia.Currency->{
                input.addTextChangedListener { s->
                    textListener?.textChangeListener(parseMoneyValue(s.toString(),",",prefix,suffix))
                }
            }
            else->{
                input.addTextChangedListener { s->
                    textListener?.textChangeListener(
                        GeneralUtils.clearTextValue(
                            s.toString(),
                            prefix,
                            suffix
                        )
                    )
                }
            }
        }
    }

    private fun setTextAndHint(text: String?, hint: String, textRight: String) {
        binding.apply {
            if (!text.isNullOrEmpty()) {
                input.setText(text)
            }
            input.hint = hint
            tvRight.text = textRight
        }
    }

    private fun setLabelAndMandatory(label: String, isMandatory: Boolean) {
        binding.apply {
            if (label.isNotEmpty()) {
                tvLabel.isVisible = true
            } else {
                val params: MarginLayoutParams = binding.input.layoutParams as MarginLayoutParams
                TextFieldHelper.setMargins(
                    binding.input,
                    params.leftMargin,
                    0,
                    params.rightMargin,
                    params.bottomMargin
                )
                tvLabel.isVisible = false
            }
            tvLabel.text = label
            mandatory.isVisible = isMandatory
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setHelperAndCounter(helper: String, maxCounter: Int) {
        binding.apply {
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            if (useCounter) {
                input.doOnTextChanged { text, _, _, _ ->
                    tvCounter.text = "Sisa ${maxCounter - (text?.length ?: 0)} huruf"
                }
            }
        }
    }

    private fun setMaxLengthText(maxLength: Int) {
        if (maxLength != 0) {
            binding.input.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
    }

    private fun onFocusChangeAndSetIcon(iconStart: Int, iconEnd: Int, iconChooseRight: Int) {
        binding.apply {
            var textLength = 0
            if (iconChooseRight != -1) {
                imgRight.setImageDrawable(ContextCompat.getDrawable(context, iconChooseRight))
                imgRight.isVisible = true
            }
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconStart != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        null,
                        null,
                    )
                }
            }

            input.doOnTextChanged { text, _, _, _ ->
                textLength = text.toString().length
                onFocusChange(iconStart, iconEnd, textLength, false)
            }

            input.setOnFocusChangeListener { _, hasFocus ->
                this@Right.hasFocus = hasFocus
                onFocusChange(iconStart, iconEnd, textLength, true)
            }
        }
    }

    fun setTextInputColor(textColor: Int) {
        if (textColor != -1) {
            binding.input.setTextColor(ContextCompat.getColor(context, textColor))
        }
    }

    private fun onFocusChange(
        iconStart: Int,
        iconEnd: Int,
        textLength: Int,
        setDelimiter: Boolean
    ) {
        if (hasFocus) {
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setHasFocusBackgroundEditText()
            }
            if (setDelimiter) {
                if (inputType == InputTypeRekanesia.Currency) {
                    textWatcher = DelimiterInputWatcher(binding.input, locale = locale, prefix = prefix, suffix = suffix)
                    input.removeTextChangedListener(textWatcher)
                    input.addTextChangedListener(textWatcher)
                }
            }
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconStart != -1 -> {
                    if (textLength >= 1) {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            ContextCompat.getDrawable(context, iconStart),
                            null,
                            ContextCompat.getDrawable(context, R.drawable.ic_close),
                            null,
                        )
                        TextFieldHelper.setOnClickClear(input)
                    } else {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            ContextCompat.getDrawable(context, iconStart),
                            null,
                            null,
                            null,
                        )
                    }
                }
                else -> {
                    if (textLength >= 1) {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            ContextCompat.getDrawable(context, R.drawable.ic_close),
                            null,
                        )
                        TextFieldHelper.setOnClickClear(input)
                    } else {
                        input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            null,
                            null,
                        )
                    }
                }
            }
        } else {
            if (inputType == InputTypeRekanesia.Currency) {
                if (textWatcher != null) {
                    input.removeTextChangedListener(textWatcher)
                }
            }
            when {
                iconStart != -1 && iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconEnd != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null,
                        null,
                        ContextCompat.getDrawable(context, iconEnd),
                        null,
                    )
                }
                iconStart != -1 -> {
                    input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        ContextCompat.getDrawable(context, iconStart),
                        null,
                        null,
                        null,
                    )
                }
                else -> {
                    binding.input.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        0,
                        0,
                        0,
                        0
                    )
                }
            }
            if (isError) {
                setErrorBackgroundEditText()
            } else {
                setNormalBackgroundEditText()
            }
        }
    }

    private fun setErrorBackgroundEditText() {
        binding.apply {
            tvHelper.setTextColor(ContextCompat.getColor(context, R.color.R500))
            tvLabel.setTextColor(ContextCompat.getColor(context, R.color.R500))
            tvRight.setTextColor(ContextCompat.getColor(context, R.color.R500))
            input.setBackgroundResource(R.drawable.bg_input_right_input_error)
            layoutRight.setBackgroundResource(R.drawable.bg_input_right_layout_right_error)
            if ((errorMessage.isNullOrEmpty() || errorMessage == "null") && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = helper
            } else if (!errorMessage.isNullOrEmpty() && helper.isEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage.toString()
            } else if (!errorMessage.isNullOrEmpty() && helper.isNotEmpty()) {
                tvHelper.isVisible = true
                tvHelper.text = errorMessage
            }
            tvCounter.isVisible = false
        }
    }

    private fun setHasFocusBackgroundEditText() {
        binding.apply {
            tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
            tvLabel.setTextColor(ContextCompat.getColor(context, R.color.B400))
            tvRight.setTextColor(ContextCompat.getColor(context, R.color.B400))
            input.setBackgroundResource(R.drawable.bg_input_right_input)
            layoutRight.setBackgroundResource(R.drawable.bg_input_right_layout_right_active)
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            tvHelper.text = helper
        }
    }

    private fun setNormalBackgroundEditText() {
        binding.apply {
            tvHelper.setTextColor(ContextCompat.getColor(context, R.color.D500))
            tvLabel.setTextColor(ContextCompat.getColor(context, R.color.D800))
            tvRight.setTextColor(ContextCompat.getColor(context, R.color.D800))
            input.setBackgroundResource(R.drawable.bg_input_right_input)
            layoutRight.setBackgroundResource(R.drawable.bg_input_right_layout_right)
            tvHelper.isVisible = helper.isNotEmpty()
            tvCounter.isVisible = useCounter
            tvHelper.text = helper
        }
    }
}