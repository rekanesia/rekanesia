package com.rekanesia

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.Barrier
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.rekanesia.databinding.LayoutBannerBinding
import com.rekanesia.helper.GeneralUtils.px
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*

class Banner : ConstraintLayout {

    private lateinit var binding: LayoutBannerBinding

    var title: String? = ""
        set(value) {
            field = value
            binding.rekanTvTitle.isVisible = !(field.isNullOrEmpty() || field == "null")
            binding.rekanTvTitle.text = field
        }

    var text: String? = ""
        set(value) {
            field = value
            binding.rekanTvText.isVisible = !(field.isNullOrEmpty() || field == "null")
            binding.rekanTvText.text = field
        }

    var textButton: String? = ""
        set(value) {
            field = value
            binding.rekanBtnDetail.isVisible = !(field.isNullOrEmpty() || field == "null")
            binding.rekanBtnDetail.text = field
            if (field.isNullOrEmpty() || field == "null") {
                binding.rekanBtnDetail.isVisible = false
                binding.barrier2.type = Barrier.RIGHT
            } else {
                binding.rekanBtnDetail.isVisible = true
                binding.barrier2.type = Barrier.LEFT
            }
        }

    var icon: Int = -1
        set(value) {
            field = value
            if (field != -1) {
                binding.rekanImgIcon.visibility = View.VISIBLE
                binding.rekanImgIcon.setImageDrawable(ContextCompat.getDrawable(context, icon))
            } else {
                binding.rekanImgIcon.visibility = View.GONE
            }
        }

    var showProgressBar: Boolean = false
        set(value) {
            field = value
            binding.rekanProgressBar.isVisible = field
            binding.rekanBtnDetail.isVisible = !field
        }

    var showBtnDetail: Boolean = false
        set(value) {
            field = value
            binding.rekanBtnDetail.isVisible = false
        }

    var showLineBtn: Boolean = true
        set(value) {
            field = value
            binding.rekanLineButton.isVisible = field
        }

    private var style = Rekan

    private var onClick: (() -> Unit)? = null
    fun setOnClickListener(onClick: () -> Unit) {
        this.onClick = onClick
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet) {
        binding = LayoutBannerBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.Banner, 0, 0)
        title = typedArray.getString(R.styleable.Banner_rekan_title).orEmpty()
        text = typedArray.getString(R.styleable.Banner_rekan_text).orEmpty()
        textButton = typedArray.getString(R.styleable.Banner_rekan_textButton).orEmpty()
        icon = typedArray.getResourceId(R.styleable.Banner_rekan_icon, -1)
        showLineBtn = typedArray.getBoolean(R.styleable.Banner_rekan_showLineButton, true)
        style = values()[typedArray.getInt(R.styleable.Banner_rekan_styles, 1) - 1]
        val textBtnColor = typedArray.getColor(R.styleable.Banner_rekan_textButtonColor, -1)
        val background = typedArray.getResourceId(R.styleable.Banner_rekan_background, R.drawable.bg_banner_gradient_blue_orange)
        val titleFontWeight = typedArray.getInt(R.styleable.Banner_rekan_titleFontWeight, 7)
        val textFontWeight = typedArray.getInt(R.styleable.Banner_rekan_textFontWeight, 2)
        val titleTextSize = typedArray.getFloat(R.styleable.Banner_rekan_titleTextSize, 14F)
        val textTextSize = typedArray.getFloat(R.styleable.Banner_rekan_textTextSize, 14F)
        val titleTextColor = typedArray.getColor(R.styleable.Banner_rekan_titleTextColor, ContextCompat.getColor(context, R.color.D800))
        val textTextColor = typedArray.getColor(R.styleable.Banner_rekan_textTextColor, ContextCompat.getColor(context, R.color.D800))

        setBackground(background)

        setTextButtonColor(textBtnColor)

        setPadding(12.px, 12.px, 12.px, 12.px)

        binding.apply {
            rekanTvTitle.isVisible = !(title.isNullOrEmpty() || title == "null")
            rekanTvTitle.text = title

            rekanTvText.isVisible = !(text.isNullOrEmpty() || text == "null")
            rekanTvText.text = text

            when (style) {
                Rekan -> {
                    when (textFontWeight) {
                        1, 2 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                        3 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                        4 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                        5, 6 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    }

                    when (titleFontWeight) {
                        1, 2 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                        3 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                        4 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                        5, 6 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    }
                }
                Ipubers -> {
                    when (textFontWeight) {
                        1, 2 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                        3 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                        4 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                        5, 6 -> binding.rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                    }

                    when (titleFontWeight) {
                        1, 2 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                        3 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                        4 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                        5, 6 -> binding.rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                    }
                }
            }

            setTextTextSize(textTextSize)

            setTextTextColor(textTextColor)

            setTitleTextSize(titleTextSize)

            setTitleTextColor(titleTextColor)

            if (textButton.isNullOrEmpty() || textButton == "null") {
                binding.rekanBtnDetail.isVisible = false
                binding.barrier2.type = Barrier.LEFT
            } else {
                if (rekanTvTitle.isVisible) {
                    rekanTvText.setPadding(0, 4, 0, 0)
                } else {
                    rekanTvText.setPadding(0, 0, 0, 0)
                }
                binding.rekanBtnDetail.isVisible = true
                binding.barrier2.type = Barrier.RIGHT
            }
            rekanBtnDetail.text = textButton

            rekanLineButton.isVisible = showLineBtn

            if (icon != -1) {
                binding.rekanImgIcon.visibility = View.VISIBLE
                rekanImgIcon.setImageDrawable(ContextCompat.getDrawable(context, icon))
            } else {
                rekanImgIcon.visibility = View.GONE
            }

            root.setOnClickListener {
                onClick?.invoke()
            }
        }

        typedArray.recycle()
    }

    fun setTitleTextColor(color: Int) {
        binding.rekanTvTitle.setTextColor(color)
    }

    fun setTextTextColor(color: Int) {
        binding.rekanTvText.setTextColor(color)
    }

    fun setTitleTextSize(sizeInSp: Float) {
        binding.rekanTvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeInSp)
    }

    fun setTextTextSize(sizeInSp: Float) {
        binding.rekanTvText.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeInSp)
    }

    fun setBackground(@DrawableRes res: Int) {
        binding.root.setBackgroundResource(res)
    }

    fun setTextButtonColor(color: Int) {
        if (color != -1) {
            binding.rekanBtnDetail.setTextColor(color)
        } else {
            binding.rekanBtnDetail.setTextColor(ContextCompat.getColor(context, R.color.B500))
        }
    }
}