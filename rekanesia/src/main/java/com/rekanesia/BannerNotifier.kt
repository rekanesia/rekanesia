package com.rekanesia

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.rekanesia.databinding.LayoutBannerNotifierBinding
import com.rekanesia.helper.GeneralUtils.px

class BannerNotifier : ConstraintLayout {

    private lateinit var binding: LayoutBannerNotifierBinding

    var icon: Int = -1
        set(value) {
            field = value
            if (field != -1) {
                binding.rekanImgIcon.visibility = View.VISIBLE
                binding.rekanImgIcon.setImageDrawable(ContextCompat.getDrawable(context, icon))
            } else {
                binding.rekanImgIcon.visibility = View.GONE
            }
        }

    var title: String? = ""
        set(value) {
            field = value
            binding.rekanTvTitle.text = field
        }

    var text: String? = ""
        set(value) {
            field = value
            binding.rekanTvText.isVisible = !(field.isNullOrEmpty() || field == "null")
            binding.rekanTvText.text = field
        }

    var textButton: String? = ""
        set(value) {
            field = value
            binding.rekanBtnDetail.isVisible = !(field.isNullOrEmpty() || field == "null")
            binding.rekanBtnDetail.text = field
        }

    private var onClick: (() -> Unit)? = null
    fun setOnClickListener(onClick: () -> Unit) {
        this.onClick = onClick
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet) {
        binding = LayoutBannerNotifierBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.BannerNotifier, 0, 0)
        title = typedArray.getString(R.styleable.BannerNotifier_rekan_title).orEmpty()
        text = typedArray.getString(R.styleable.BannerNotifier_rekan_text).orEmpty()
        textButton = typedArray.getString(R.styleable.BannerNotifier_rekan_textButton).orEmpty()
        icon = typedArray.getResourceId(R.styleable.BannerNotifier_rekan_icon, -1)
        val background = typedArray.getResourceId(R.styleable.BannerNotifier_rekan_background, R.drawable.bg_banner_gradient_blue_orange)
        val fontFamily = typedArray.getInt(R.styleable.BannerNotifier_rekan_font_family, 1)

        setBackground(background)

        binding.apply {
            when (fontFamily) {
                1 -> {
                    rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
                    rekanBtnDetail.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
                }
                2 -> {
                    rekanTvTitle.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                    rekanTvText.typeface = ResourcesCompat.getFont(context, R.font.harmony_regular)
                    rekanBtnDetail.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                }
            }
            rekanTvTitle.isVisible = !(title.isNullOrEmpty() || title == "null")
            rekanTvTitle.text = title

            rekanTvText.isVisible = !(text.isNullOrEmpty() || text == "null")
            rekanTvText.text = text

            if (textButton.isNullOrEmpty()) {
                layoutCta.visibility = View.GONE
                setPadding(12.px, 16.px, 12.px, 16.px)
            } else {
                layoutCta.visibility = View.VISIBLE
                setPadding(12.px, 16.px, 12.px, 0.px)
            }

            if (icon != -1) {
                binding.rekanImgIcon.visibility = View.VISIBLE
                rekanImgIcon.setImageDrawable(ContextCompat.getDrawable(context, icon))
            } else {
                rekanImgIcon.visibility = View.GONE
            }

            root.setOnClickListener {
                onClick?.invoke()
            }

            layoutCta.setOnClickListener {
                onClick?.invoke()
            }
        }

        typedArray.recycle()
    }

    fun setBackground(@DrawableRes res: Int) {
        binding.root.setBackgroundResource(res)
    }
}