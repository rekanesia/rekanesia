package com.rekanesia

import android.os.Build
import android.os.Bundle
import android.view.WindowInsets
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.lifecycleScope
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.CameraView
import com.otaliastudios.cameraview.PictureResult
import com.otaliastudios.cameraview.controls.Facing
import com.otaliastudios.cameraview.controls.Flash
import com.otaliastudios.cameraview.gesture.Gesture
import com.otaliastudios.cameraview.gesture.GestureAction
import com.rekanesia.databinding.ActivityCameraRekanesiaBinding
import com.rekanesia.helper.ImageHelper.rotateBitmapImage
import com.rekanesia.image_picker.ImagePickerActivity
import kotlinx.coroutines.launch

class RekanesiaCameraActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCameraRekanesiaBinding

    private lateinit var camera: CameraView

    private var orientation = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCameraRekanesiaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            ViewCompat.getWindowInsetsController(window.decorView)
                ?.hide(WindowInsetsCompat.Type.systemBars())
        }

        configCamera()

        onViewClick()
    }

    private val cameraListener = object : CameraListener() {

        override fun onPictureTaken(result: PictureResult) {
            super.onPictureTaken(result)
            result.toBitmap { bitmap ->
                bitmap?.let {
                    lifecycleScope.launch {
                        val rotatedBitmap = rotateBitmapImage(bitmap, orientation.toFloat())
                        ImagePickerActivity.pictureResult = rotatedBitmap
                        setResult(RESULT_OK)
                        finish()
                    }
                }
            }
        }

        override fun onOrientationChanged(orientation: Int) {
            super.onOrientationChanged(orientation)
            this@RekanesiaCameraActivity.orientation = orientation
        }
    }


    private fun onViewClick() {
        binding.apply {
            rekanBtnFlashCamera.setOnClickListener {
                toggleFlash()
            }

            rekanBtnTakePic.setOnClickListener {
                camera.takePicture()
            }

            rekanBtnSwitchCamera.setOnClickListener {
                toggleCamera()
            }
        }
    }

    private fun toggleCamera() {
        if (camera.isTakingPicture) return
        when (camera.toggleFacing()) {
            Facing.BACK -> Toast.makeText(this, "Mengganti ke kamera depan", Toast.LENGTH_SHORT)
                .show()
            Facing.FRONT -> Toast.makeText(this, "Mengganti ke kamera belakang", Toast.LENGTH_SHORT)
                .show()
            else -> Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show()
        }
    }

    private fun toggleFlash() {
        if (camera.flash == Flash.OFF) {
            camera.flash = Flash.TORCH
        } else {
            camera.flash = Flash.OFF
        }
    }

    private fun configCamera() {
        camera = binding.rekanSurfaceCamera
        camera.apply {
            addCameraListener(cameraListener)
            setLifecycleOwner(this@RekanesiaCameraActivity)
            mapGesture(Gesture.TAP, GestureAction.AUTO_FOCUS)
        }
    }
}