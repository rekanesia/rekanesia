package com.rekanesia

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rekanesia.databinding.LayoutAppAlertDialogBinding

enum class DialogType {
    BottomSheet, Default
}

class AppAlertDialog(
    val dialogType: DialogType = DialogType.Default,
    val context: Context,
    val image: Int,
    val title: String,
    val description: String,
    val primaryButtonText: String,
    val onPrimaryButtonClick: ((AppAlertDialog) -> Unit),
    val secondaryButtonText: String? = null,
    val onSecondaryButtonClick: ((AppAlertDialog) -> Unit)? = null,
    val fragmentManager: FragmentManager? = null
) {

    private lateinit var alertDialog: AlertDialog
    private lateinit var bottomSheet: BottomDialog

    fun show() {
        val dialogView = LayoutAppAlertDialogBinding.inflate(LayoutInflater.from(context))

        dialogView.ivAlert.setImageResource(image)
        dialogView.tvTitle.text = title
        dialogView.tvDesc.text = description
        dialogView.buttonPrimary.text = primaryButtonText
        dialogView.buttonPrimary.setOnClickListener { onPrimaryButtonClick.invoke(this) }
        if (secondaryButtonText != null) {
            dialogView.buttonSecondary.text = secondaryButtonText
            dialogView.buttonSecondary.setOnClickListener {
                onSecondaryButtonClick?.invoke(this)
            }

            val btnSecondaryMargin = dialogView.buttonSecondary.layoutParams as ViewGroup.MarginLayoutParams
            btnSecondaryMargin.setMargins(0, 0, 10, 0)
            val btnPrimaryMargin = dialogView.buttonPrimary.layoutParams as ViewGroup.MarginLayoutParams
            btnPrimaryMargin.setMargins(10, 0, 0, 0)
        } else {
            dialogView.buttonsContainer.weightSum = 1f
            dialogView.buttonSecondary.visibility = View.GONE
        }

        if (dialogType == DialogType.Default) {
            val dialogBuilder = AlertDialog.Builder(context)
            dialogBuilder.setView(dialogView.root)
            alertDialog = dialogBuilder.create()
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()
        } else {
            bottomSheet = BottomDialog()
            if (fragmentManager != null) {
                bottomSheet.ad = this
                bottomSheet.image = image
                bottomSheet.title = title
                bottomSheet.description = description
                bottomSheet.primaryButtonText = primaryButtonText
                bottomSheet.secondaryButtonText = secondaryButtonText
                bottomSheet.onPrimaryButtonClick = onPrimaryButtonClick
                bottomSheet.onSecondaryButtonClick = onSecondaryButtonClick
                bottomSheet.show(fragmentManager, "TAG BOTTOM")
            } else {
                throw Exception("Fragment manager must be not null if you want to use bottom sheet alert dialog")
            }
        }
    }

    fun dismiss() {
        if (dialogType == DialogType.Default) {
            alertDialog.dismiss()
        } else {
            bottomSheet.dismiss()
        }
    }
}

class BottomDialog: BottomSheetDialogFragment() {
    var ad: AppAlertDialog? = null
    var image: Int? = null
    var title: String? = null
    var description: String? = null
    var primaryButtonText: String? = null
    var secondaryButtonText: String? = null
    var onPrimaryButtonClick: ((AppAlertDialog) -> Unit)? = null
    var onSecondaryButtonClick: ((AppAlertDialog) -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val dialogView = LayoutAppAlertDialogBinding.inflate(inflater, container, false)

        dialogView.ivAlert.setImageResource(image!!)
        dialogView.tvTitle.text = title
        dialogView.tvDesc.text = description
        dialogView.buttonPrimary.text = primaryButtonText
        dialogView.buttonPrimary.setOnClickListener { onPrimaryButtonClick!!.invoke(ad!!) }
        if (secondaryButtonText != null) {
            dialogView.buttonSecondary.text = secondaryButtonText
            dialogView.buttonSecondary.setOnClickListener {
                onSecondaryButtonClick?.invoke(ad!!)
            }

            val btnSecondaryMargin = dialogView.buttonSecondary.layoutParams as ViewGroup.MarginLayoutParams
            btnSecondaryMargin.setMargins(0, 0, 10, 0)
            val btnPrimaryMargin = dialogView.buttonPrimary.layoutParams as ViewGroup.MarginLayoutParams
            btnPrimaryMargin.setMargins(10, 0, 0, 0)
        } else {
            dialogView.buttonsContainer.weightSum = 1f
            dialogView.buttonSecondary.visibility = View.GONE
        }

        return dialogView.root
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        dialog.cancel()
    }
}