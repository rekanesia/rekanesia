package com.rekanesia

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.rekanesia.databinding.LayoutBadgeBinding

class Badge @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {
    private var binding: LayoutBadgeBinding

    enum class Theme {
        Primary,
        Secondary,
        Success,
        Danger,
        Warning,
        Info,
        Light,
        Dark
    }

    var text = ""
        set(value) {
            field = value
            binding.textLabel.text = field
        }
    var theme: Theme = Theme.Primary
        set(value) {
            field = value

            val style = when (field) {
                Theme.Primary -> 1
                Theme.Secondary -> 2
                Theme.Success -> 3
                Theme.Danger -> 4
                Theme.Warning -> 5
                Theme.Info -> 5
                Theme.Light -> 7
                Theme.Dark -> 8
            }
            setStyle(style)
        }

    init {
        binding = LayoutBadgeBinding.inflate(LayoutInflater.from(context), this)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.Badge, 0, 0)

        val style = typedArray.getInt(R.styleable.Badge_rekan_badgeTheme, 0)
        val label = typedArray.getString(R.styleable.Badge_rekan_label).orEmpty()
        val fontWeight = typedArray.getInt(R.styleable.Badge_rekan_customFontWeight, 5)
        val textSize = typedArray.getFloat(R.styleable.Badge_rekan_textSize, 16f)
        val paddingVertical = typedArray.getDimensionPixelSize(R.styleable.Badge_rekan_paddingVertical, -0)
        val paddingHorizontal = typedArray.getDimensionPixelSize(R.styleable.Badge_rekan_paddingHorizontal, -0)

        when (fontWeight) {
            1 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_light)
            2 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_regular)
            3 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
            4 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_semi_bold)
            5 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
            6 -> binding.textLabel.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_black)
        }

        val paddingLeft = if (paddingHorizontal != -0) paddingHorizontal else 8
        val paddingRight = if (paddingHorizontal != -0) paddingHorizontal else 8
        val paddingTop = if (paddingVertical != -0) paddingVertical else 6
        val paddingBottom = if (paddingVertical != -0) paddingVertical else 6
        binding.badgeLayout.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom)

        binding.textLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
        binding.textLabel.text = label
        binding.textLabel.setTextColor(ContextCompat.getColor(context, R.color.white))
        setStyle(style)

        typedArray.recycle()
    }

    private fun setStyle(style: Int) {
        binding.textLabel.setTextColor(ContextCompat.getColor(context, R.color.white))

        when (style) {
            1 -> binding.badgeLayout.background = ContextCompat.getDrawable(context, R.drawable.bg_badge_primary)
            2 -> {
                binding.badgeLayout.background = ContextCompat.getDrawable(context, R.drawable.bg_badge_secondary)
                binding.textLabel.setTextColor(ContextCompat.getColor(context, R.color.B500))
            }
            3 -> {
                binding.badgeLayout.background = ContextCompat.getDrawable(context, R.drawable.bg_badge_success)
                binding.textLabel.setTextColor(ContextCompat.getColor(context, R.color.D700))
            }
            4 -> {
                binding.badgeLayout.background = ContextCompat.getDrawable(context, R.drawable.bg_badge_danger)
                binding.textLabel.setTextColor(ContextCompat.getColor(context, R.color.R600))
            }
            5 -> binding.badgeLayout.background = ContextCompat.getDrawable(context, R.drawable.bg_badge_warning)
            9 -> {
                binding.badgeLayout.background = ContextCompat.getDrawable(context, R.drawable.bg_badge_red)
                binding.textLabel.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
            else -> binding.badgeLayout.background = ContextCompat.getDrawable(context, R.drawable.bg_badge_primary)
        }
    }
}