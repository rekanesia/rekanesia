package com.rekanesia

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import com.rekanesia.databinding.LayoutButtonBinding

class Button constructor(
    context: Context,
    attrs: AttributeSet
) : LinearLayout(context,attrs) {

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.Button, 0, 0)
        renderComponent(typedArray){ typeArray->
            val style = typeArray.getInt(R.styleable.Button_rekan_style, 1)
            button = when (style) {
                1 -> binding.buttonPrimary
                2 -> binding.buttonSecondary
                3 -> binding.buttonOutline
                4 -> binding.buttonText
                else -> binding.buttonPrimary
            }
        }
    }

    private lateinit var binding: LayoutButtonBinding
    lateinit var button: MaterialButton

    var text: String? = ""
        set(value) {
            field = value
            if (field.isNullOrEmpty() || field == "null") field = ""
            button.text = field
        }
        get() = button.text.toString()

    var buttonEnabled = true
        set(value) {
            field = value
            button.isEnabled = buttonEnabled
        }



    fun setOnClickListener(onClick: (() -> Unit)) {
        binding.buttonPrimary.setOnClickListener { onClick.invoke() }
        binding.buttonSecondary.setOnClickListener { onClick.invoke() }
        binding.buttonOutline.setOnClickListener { onClick.invoke() }
        binding.buttonText.setOnClickListener { onClick.invoke() }
    }

    fun setTextColor(textColor: Int) {
        if (textColor != -1) {
            button.setTextColor(textColor)
        }
    }

    fun setIconTint(iconTint: Int) {
        if (iconTint != -1) {
            button.iconTint = ColorStateList.valueOf(iconTint)
        }
    }

    val attributeTyped = context.obtainStyledAttributes(attrs, R.styleable.Button, 0, 0)

    private fun renderComponent(typedArray:TypedArray,setButton:(TypedArray)->Unit) {
        if(!this::binding.isInitialized){
            binding = LayoutButtonBinding.inflate(LayoutInflater.from(context), this)
        }

        binding.buttonPrimary.visibility = View.GONE
        binding.buttonSecondary.visibility = View.GONE
        binding.buttonOutline.visibility = View.GONE
        binding.buttonText.visibility = View.GONE

        val icon = typedArray.getResourceId(R.styleable.Button_rekan_icon, -1)
        val textSize = typedArray.getFloat(R.styleable.Button_rekan_textSize, 16f)
        val iconGravity = typedArray.getInt(R.styleable.Button_rekan_iconGravity, -1)
        val iconTint = typedArray.getInt(R.styleable.Button_rekan_iconTint, -1)
        val textColor = typedArray.getColor(R.styleable.Button_rekan_textColor, -1)
        setButton.invoke(typedArray)

        text = typedArray.getString(R.styleable.Button_rekan_text).orEmpty()
        buttonEnabled = typedArray.getBoolean(R.styleable.Button_rekan_enabled, true)

        button.isEnabled = buttonEnabled

        button.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)

        setText()

        setIconGravity(icon, iconGravity)

        setTextColor(textColor)

        setIconTint(iconTint)

        button.visibility = View.VISIBLE

        if(!this::binding.isInitialized){
            typedArray.recycle()
        }
    }

    private fun setText() {
        button.text = text
    }

    private fun setIconGravity(icon: Int, iconGravity: Int) {
        if (icon != -1) {
            button.icon = ContextCompat.getDrawable(context, icon)
            button.iconGravity = when (iconGravity) {
                1 -> MaterialButton.ICON_GRAVITY_START
                2 -> MaterialButton.ICON_GRAVITY_TEXT_START
                3 -> MaterialButton.ICON_GRAVITY_END
                4 -> MaterialButton.ICON_GRAVITY_TEXT_END
                5 -> MaterialButton.ICON_GRAVITY_TOP
                6 -> MaterialButton.ICON_GRAVITY_TEXT_TOP
                else -> MaterialButton.ICON_GRAVITY_TEXT_START
            }
        }
    }


    /**
     * untuk fun setType
     * ulang lagi proses render button
     */
    fun setType(type: ButtonType, typeArray: TypedArray){
        renderComponent(typeArray){ _ ->
            button = when (type) {
                ButtonType.PRIMARY -> binding.buttonPrimary
                ButtonType.SECONDARY -> binding.buttonSecondary
                ButtonType.OUTLINE -> binding.buttonOutline
                ButtonType.TEXT_ONLY -> binding.buttonText
            }
        }
    }

    enum class ButtonType{
        PRIMARY,
        SECONDARY,
        OUTLINE,
        TEXT_ONLY
    }
}