package com.rekanesia.chip

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.rekanesia.R
import com.rekanesia.databinding.LayoutChipGroupBinding
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.Rekan

class RekanesiaChipGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {

    private var binding: LayoutChipGroupBinding

    private var isMultipleChoice = false
    private var activeIndicatorSize: Float
    lateinit var chipAdapter: RekanesiaChipAdapter
    private var style = Rekan

    init {
        binding = LayoutChipGroupBinding.inflate(LayoutInflater.from(context), this)
        orientation = VERTICAL

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.RekanesiaChipGroup, 0, 0)
        isMultipleChoice = typedArray.getBoolean(R.styleable.RekanesiaChipGroup_rekan_isMultipleChoice, false)
        activeIndicatorSize = typedArray.getFloat(R.styleable.RekanesiaChipGroup_rekan_activeIndicatorSize, 16f)
        style = StyleRekanesia.values()[typedArray.getInt(R.styleable.RekanesiaChipGroup_rekan_styles, 1) - 1]
        val textColor = typedArray.getColor(R.styleable.RekanesiaChipGroup_rekan_textColor, ContextCompat.getColor(context, R.color.B500))
        setupAdapter()
        chipAdapter.setTextColor(textColor)

        typedArray.recycle()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        val chips = mutableListOf<ChipOption>()
        for (index in 0  until childCount) {
            val child = getChildAt(index)
            if (child is RekanesiaChip) {
                chips.add(
                    ChipOption(
                        index,
                        child.label ?: "",
                        child.isChecked,
                        child.id
                    )
                )
            }
        }
        chipAdapter.submitList(chips)
    }


    private fun setupAdapter() {
        chipAdapter = RekanesiaChipAdapter(isMultipleChoice, activeIndicatorSize, style)
        binding.rvChip.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = chipAdapter
        }
    }

    fun setOnChipSelectedListener(onSelected: (ChipOption?) -> Unit = {}) {
        chipAdapter.setOnChipSelectedListener {
            Thread.sleep(200)
            binding.rvChip.smoothScrollToPosition(0)
            onSelected.invoke(it)
        }
    }

    fun setOnMultipleChipsSelectedListener(onSelected: (List<ChipOption>?) -> Unit) {
        chipAdapter.setOnMultipleChipSelectedListener {
            Thread.sleep(200)
            binding.rvChip.smoothScrollToPosition(0)
            onSelected.invoke(it)
        }
    }

    fun setOnChipSelectedIndexed(onSelected: (Int, ChipOption) -> Unit) {
        chipAdapter.setOnChipSelectedIndexed { index, chipOption ->
            Thread.sleep(200)
            binding.rvChip.smoothScrollToPosition(0)
            onSelected.invoke(index, chipOption)
        }
    }

    fun addChip(chip: ChipOption) {
        chipAdapter.addItem(chip)
    }

    fun addAllChip(chips: List<ChipOption>) {
        chipAdapter.submitList(chips)
    }

    fun getSelectedChip(): ChipOption? = chipAdapter.getSelectedChip()

    fun getMultipleSelectedChips(): List<ChipOption>? = chipAdapter.getMultipleSelectedChips()

}