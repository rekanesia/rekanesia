package com.rekanesia.chip

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rekanesia.R
import com.rekanesia.databinding.ItemChipBinding
import com.rekanesia.helper.StyleRekanesia
import com.rekanesia.helper.StyleRekanesia.*
import com.rekanesia.helper.TextFieldHelper.fromHtml

class RekanesiaChipAdapter(
    private val isMultipleChoice: Boolean = false,
    private val activeIndicatorSize: Float,
    private val style: StyleRekanesia = Rekan
) : ListAdapter<ChipOption, RekanesiaChipAdapter.ItemViewHolder>(diffCallback) {

    private var textColor: Int = when (style) {
        Rekan -> R.color.B500
        Ipubers -> R.color.L500
    }

    private var onChipSelected: ((ChipOption) -> Unit)? = null
    fun setOnChipSelectedListener(onItemSelected: (ChipOption) -> Unit) {
        onChipSelected = onItemSelected
    }

    private var onMultipleChipSelected: ((List<ChipOption>) -> Unit)? = null
    fun setOnMultipleChipSelectedListener(onItemsSelected: (List<ChipOption>) -> Unit) {
        onMultipleChipSelected = onItemsSelected
    }

    private var onChipSelectedIndexed: ((Int, ChipOption) -> Unit)? = null
    fun setOnChipSelectedIndexed(onItemSelected: (Int, ChipOption) -> Unit) {
        onChipSelectedIndexed = onItemSelected
    }

    fun getSelectedChip(): ChipOption? {
        return try {
            val chips = currentList.toMutableList()
            chips.first { it.selected }
        } catch (e: Exception) {
            null
        }
    }

    fun getMultipleSelectedChips(): List<ChipOption>? {
        return try {
            val chips = currentList.toMutableList()
            chips.filter { it.selected }
        } catch (e: Exception) {
            emptyList()
        }
    }

    fun addItem(item: ChipOption) {
        val curList = currentList.toMutableList()
        curList.add(item)
        submitList(curList)
    }

    fun setTextColor(color: Int) {
        textColor = color
    }

    inner class ItemViewHolder(private var binding: ItemChipBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("ResourceAsColor")
        fun bind(item: ChipOption) {
            val context = itemView.context
            binding.apply {
                tvChip.text = item.name
                tvBulletSymbol.setTextSize(TypedValue.COMPLEX_UNIT_SP, activeIndicatorSize)
                imgChevron.isVisible = item.childChips.isNullOrEmpty().not()

                onSelectedChip(context, item.selected, tvBulletSymbol, tvChip, imgChevron, textColor)

                root.setOnClickListener {
                    if (isMultipleChoice) {
                        val items = currentList
                        val isSelected = items[adapterPosition].selected
                        items[adapterPosition].selected = !isSelected
                        val selectedChips = items.filter { it.selected }
                        notifyDataSetChanged()
                        onMultipleChipSelected?.invoke(selectedChips)
                    } else {
                        val items = currentList
                        if (!item.selected) {
                            items.forEach { chipOption ->
                                chipOption.selected = false
                            }
                            items[adapterPosition].selected = true
                            notifyDataSetChanged()
                        }
                        val selectedChip = items.first { it.selected }
                        onChipSelected?.invoke(selectedChip)
                        onChipSelectedIndexed?.invoke(layoutPosition, selectedChip)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding = ItemChipBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    private fun onSelectedChip(
        context: Context,
        isSelected: Boolean,
        symbolSelected: TextView,
        textChip: TextView,
        imageChevron: ImageView,
        textColor: Int
    ) {
        when (style) {
            Rekan -> {
                if (isSelected) {
                    symbolSelected.isVisible = true
                    symbolSelected.text = context.getString(R.string.bullet_symbol).fromHtml()
                    symbolSelected.setTextColor(textColor)
                    textChip.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    symbolSelected.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_bold)
                    textChip.setTextColor(textColor)
                    imageChevron.imageTintList = ColorStateList.valueOf(textColor)
                } else {
                    symbolSelected.isVisible = false
                    textChip.typeface = ResourcesCompat.getFont(context, R.font.noto_sans_medium)
                    textChip.setTextColor(ContextCompat.getColor(context, R.color.D500))
                }
            }
            Ipubers -> {
                if (isSelected) {
                    symbolSelected.isVisible = true
                    symbolSelected.text = context.getString(R.string.bullet_symbol).fromHtml()
                    symbolSelected.setTextColor(textColor)
                    textChip.typeface = ResourcesCompat.getFont(context, R.font.harmony_bold)
                    textChip.setTextColor(textColor)
                    imageChevron.imageTintList = ColorStateList.valueOf(textColor)
                } else {
                    symbolSelected.isVisible = false
                    textChip.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    symbolSelected.typeface = ResourcesCompat.getFont(context, R.font.harmony_medium)
                    textChip.setTextColor(ContextCompat.getColor(context, R.color.M500))
                }
            }
        }

    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ChipOption>() {
            override fun areItemsTheSame(oldItem: ChipOption, newItem: ChipOption): Boolean {
                return oldItem.index == newItem.index
            }

            override fun areContentsTheSame(oldItem: ChipOption, newItem: ChipOption): Boolean {
                return oldItem == newItem
            }
        }
    }
}

data class ChipOption(
    var index: Int,
    var name: String,
    var selected: Boolean = false,
    var params:String?=null, // kebutuhan dinamis data
    var id: String? = null,
    val childChips:List<Any?>? = null
)