package com.rekanesia.chip

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.rekanesia.R

class RekanesiaChip @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
): LinearLayout(context, attrs, defStyle) {

    var label: String? = null
    var isChecked: Boolean = false
    var id: String? = null

    init {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.RekanesiaChip, 0, 0)
            label = typedArray.getString(R.styleable.RekanesiaChip_rekan_label).orEmpty()
            isChecked = typedArray.getBoolean(R.styleable.RekanesiaChip_rekan_checked, false)
            id = typedArray.getString(R.styleable.RekanesiaChip_rekan_id)
            typedArray.recycle()
        }
    }
}